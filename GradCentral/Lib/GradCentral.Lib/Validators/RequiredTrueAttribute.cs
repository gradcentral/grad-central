﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace GC.Lib.Validators
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field, AllowMultiple = false)]
    public sealed class RequiredTrueAttribute : ValidationAttribute
    {
        // Internal field to hold the mask value.
        readonly bool _accepted;

        public bool Accepted
        {
            get { return _accepted; }
        }

        public RequiredTrueAttribute(bool accepted)
        {
            this._accepted = accepted;
        }

        public RequiredTrueAttribute()
        {
            this._accepted = true;
        }


        public override bool IsValid(object value)
        {
            bool isAccepted = (bool)value;
            return (isAccepted == true);
        }

        public override string FormatErrorMessage(string name)
        {
            return String.Format(CultureInfo.CurrentCulture,   ErrorMessageString, name, this.Accepted);
        }
    }
}
