﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace GC.Lib.Utility
{
    public class CallerContext
    {
        public static string[] LocalIps = { "::1", "127.0.0.1" };

        public static string CallerIp
        {
            get
            {
                OperationContext context = OperationContext.Current;
                if (context == null)
                    return "::1";

                MessageProperties messageProperties = context.IncomingMessageProperties;
                RemoteEndpointMessageProperty endpointProperty = messageProperties[RemoteEndpointMessageProperty.Name] as RemoteEndpointMessageProperty;
                return endpointProperty.Address;
            }

        }
    }
}
