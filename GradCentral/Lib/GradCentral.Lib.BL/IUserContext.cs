﻿using System.Collections.Generic;

namespace GC.Lib.BL
{
    public interface IUserContext<TPrivilegeTypeEnum>
    {
        #region User Details

        long Id { get; }

        string UserName { get; }

        string DisplayName { get; }

        List<TPrivilegeTypeEnum> AllowedPrivileges { get; }

        bool IsSystemAdmin { get; }


        #endregion
    }
}
