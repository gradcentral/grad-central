﻿namespace GC.Lib.BL
{
    public enum AuditEventType
    {
        Insert,
        Modify,
        Delete
    }
}
