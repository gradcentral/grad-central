﻿using System;

namespace GC.Lib.BL
{
    public class GenericSecurityException :
     Exception
    {
        public GenericSecurityException(string message) :
            base(message)
        {

        }
    }
}
