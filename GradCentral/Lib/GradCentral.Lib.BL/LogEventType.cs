﻿namespace GC.Lib.BL
{
    public enum LogEventType
    {
        Exception,
        Information,
        CriticalException,
        Warning,
        Error
    }
}
