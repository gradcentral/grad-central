﻿namespace GC.Lib.BL
{
    public interface IAuditedEntity
    {
        string Describe();
    }
}
