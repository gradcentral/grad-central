﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.CompanyData;
using GradCentral.BL.Provider.Company;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Company;
using GradCentral.Models.QualificationData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class CompanyController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.Company.ICompanyProvider CompanyProvider { get; set; }

        public CompanyController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            CompanyProvider = new CompanyProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public CompanyViewModel SaveCompany(CompanyViewModel model)
        {
            try
             {
                if (!ModelState.IsValid)
                {
                    //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                    var errors = ModelState.Values.SelectMany(v => v.Errors);
                }
                Company newCompany = new Company()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Street = model.Street,
                    Number = model.Number,
                    Recruiters = model.Recruiters,
                    Zip = model.Zip,
                    Town = model.Town,
                    Province = model.Province,
                    City = model.City,
                    Email = model.Email

                };

                Company SavedCompany = CompanyProvider.SaveCompany(newCompany);

                CompanyViewModel viewModelCompany = new CompanyViewModel()
                {
                    Id = SavedCompany.Id,
                    Name = SavedCompany.Name,
                    Street = SavedCompany.Street,
                    Number = SavedCompany.Number,
                    Recruiters = SavedCompany.Recruiters,
                    Zip = SavedCompany.Zip,
                    Town = SavedCompany.Town,
                    Province = SavedCompany.Province,
                    City = SavedCompany.City,
                    Email = SavedCompany.Email
                };
                
                return viewModelCompany;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public List<CompanyViewModel> CompanyList()
        {
            List<Company> CompanyList = CompanyProvider.GetCompanys().OrderBy(b => b.Name).ToList();
            
            var model = CompanyList.Select(Companys => new CompanyViewModel()
            {
                Id = Companys.Id,
                Name = Companys.Name,
                Street = Companys.Street,
                Number = Companys.Number,
                Recruiters = Companys.Recruiters,
                Zip = Companys.Zip,
                Town = Companys.Town,
                Province = Companys.Province,
                City = Companys.City,
                Email = Companys.Email
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<CompanyGridModel> CompanyGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = CompanyProvider.GetCompanys().Select(Companys => new CompanyGridModel()
                {
                    Id = Companys.Id,
                    Name = Companys.Name,
                    Street = Companys.Street,
                    Number = Companys.Number,
                    Recruiters = Companys.Recruiters,
                    Zip = Companys.Zip,
                    Town = Companys.Town,
                    Province = Companys.Province,
                    City = Companys.City,
                    Email = Companys.Email
            });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Street.ToString().Contains(model.Searchfor) 
                        || r.Id.ToString().Contains(model.Searchfor) || r.Number.ToString().Contains(model.Searchfor)
                             || r.Zip.ToString().Contains(model.Searchfor) || r.Town.ToString().Contains(model.Searchfor)
                             || r.Province.ToString().Contains(model.Searchfor) || r.City.ToString().Contains(model.Searchfor)
                             || r.Email.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "street":
                            filteredQuery = filteredQuery.OrderBy(r => r.Street);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderBy(r => r.City);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "town":
                            filteredQuery = filteredQuery.OrderBy(r => r.Town);
                            break;
                        case "zip":
                            filteredQuery = filteredQuery.OrderBy(r => r.Zip);
                            break;
                        case "number":
                            filteredQuery = filteredQuery.OrderBy(r => r.Number);
                            break;
                        case "province":
                            filteredQuery = filteredQuery.OrderBy(r => r.Province);
                            break;
                        case "email":
                            filteredQuery = filteredQuery.OrderBy(r => r.Email);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "street":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Street);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.City);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "town":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Town);
                            break;
                        case "zip":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Zip);
                            break;
                        case "number":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Number);
                            break;
                        case "province":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Province);
                            break;
                        case "email":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Email);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<CompanyGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public CompanyViewModel CompanyGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                CompanyViewModel model = new CompanyViewModel();

                if (id > 0)
                {
                    model = CompanyProvider.GetCompanys().Where(a => a.Id == id).Select(Comp => new CompanyViewModel()
                    {
                        Name = Comp.Name,
                        Street = Comp.Street,
                        Number = Comp.Number,
                        Recruiters = Comp.Recruiters,
                        Zip = Comp.Zip,
                        Town = Comp.Town,
                        Province = Comp.Province,
                        City = Comp.City,
                        Email = Comp.Email
                    }).Single();

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}