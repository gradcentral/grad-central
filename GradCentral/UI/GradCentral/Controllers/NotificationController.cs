﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.NotificationData;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Faculty;
using GradCentral.BL.Provider.Notification;
using GradCentral.Models.NotificationData;
using GradCentral.BL.Entities.TertiaryData.Faculty;
using GradCentralUI.Models.Job;
using GradCentral.Models.SecurityData;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.JobData;
using GradCentralUI.Models.Notification;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class NotificationController : TCRControllerBase
    {

        #region Constructor

        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private BL.Provider.Notification.INotificationProvider NotificationProvider { get; set; }

        public NotificationController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            NotificationProvider = new NotificationProvider(Context, CurrentUser);
        }


        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        #endregion



        [HttpPost]
        public NotificationViewModel SaveNotification(NotificationViewModel model)
        {

            try
            {
                if (!ModelState.IsValid)
                {

                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Error details"));
                }

                Notification newNotification = new Notification()
                {
                    Id = model.Id,
                    Date = DateTime.Now,
                    ExpiryDate = model.ExpiryDate,
                    DateCreated = model.DateCreated,
                    Read = model.Read,
                    Confirm = model.Confirm,
                    JobId = model.JobId,
                    UserIdentityId = model.UseridentityId,


                };

                Notification SavedNotification = NotificationProvider.SaveNotification(newNotification);



                NotificationViewModel viewModelNotification = new NotificationViewModel()
                {
                    Id = SavedNotification.Id,
                    ExpiryDate = SavedNotification.ExpiryDate,
                    Date = SavedNotification.Date,
                    DateCreated = DateTime.Now,
                    Read = SavedNotification.Read,
                    Message = SavedNotification.Message,
                    Confirm = SavedNotification.Confirm,
                    JobId = SavedNotification.JobId,
                    UseridentityId = CurrentUser.Id,

                };

                return viewModelNotification;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }


        [HttpPost]
        public bool NotifyCandidates(GraduateNotification GraduateNotification)
        {
            return NotificationProvider.SendGradsNotification(GraduateNotification.Job, GraduateNotification.Graduates);
        }

        [HttpGet]
        public List<NotificationViewModel> NotificationList()
        {
            List<Notification> NotificationList = NotificationProvider.GetNotifications().OrderBy(b => b.DateCreated).ToList();

            var model = NotificationList.Select(Notifications => new NotificationViewModel()
            {
                Id = Notifications.Id,
                DateCreated = DateTime.Now,
                Date = Notifications.Date,
                ExpiryDate = Notifications.ExpiryDate,
                Read = Notifications.Read,
                Confirm = Notifications.Confirm,
                Message = Notifications.Message,
                JobId = Notifications.JobId,
                UseridentityId = CurrentUser.Id,

                //Notifications = Notificationes.Notifications,


            }).ToList();

            return model;
        }

        [HttpGet]
        public List<NotificationViewModel> GetUserNotifications()
        {
            var itms = NotificationProvider.GetNotificationsByUser()
                                         .OrderBy(b => b.DateCreated)
                                         .ToList();
            return itms.Select(a => new NotificationViewModel()
            {
                Id = a.Id,
                Date = a.Date,
                DateCreated = DateTime.Now,
                ExpiryDate = a.ExpiryDate,
                Confirm = a.Confirm,
                Read = a.Read,
                JobId = a.JobId,
                Message = a.Message,
                UseridentityId = CurrentUser.Id

            }).ToList();

        }

        [HttpPost]
        public GridResultModel<NotificationGridModel> NotificationGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = NotificationProvider.GetNotifications()
                .Select(Notification => new NotificationGridModel()
                {

                    Id = Notification.Id,
                    DateCreated = DateTime.Now,
                    Date = Notification.Date,
                    ExpiryDate = Notification.ExpiryDate,
                    Read = Notification.Read,
                    Confirm = Notification.Confirm,
                    Message = Notification.Message,
                    JobId = Notification.JobId,
                    UseridentityId = CurrentUser.Id,

                    //Notifications = Notificationes.Notifications,



                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.DateCreated.ToString().Contains(model.Searchfor) || r.Message.Contains(model.Searchfor) || r.ExpiryDate.ToString().Contains(model.Searchfor) || r.Read.ToString().Contains(model.Searchfor) || r.ExpiryDate.ToString().Contains(model.Searchfor) || r.Confirm.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.DateCreated); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "Id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "Name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Date);
                            break;
                        case "NotificationType":
                            filteredQuery = filteredQuery.OrderBy(r => r.DateCreated);
                            break;
                        case "Date":
                            filteredQuery = filteredQuery.OrderBy(r => r.ExpiryDate);
                            break;
                        case "Job":
                            filteredQuery = filteredQuery.OrderBy(r => r.Job.Title);
                            break;
                        case "Expiry Date":
                            filteredQuery = filteredQuery.OrderBy(r => r.ExpiryDate);
                            break;
                        case "Message":
                            filteredQuery = filteredQuery.OrderBy(r => r.Message);
                            break;
                        case "Read":
                            filteredQuery = filteredQuery.OrderBy(r => r.Read);
                            break;
                        case "Confirmed":
                            filteredQuery = filteredQuery.OrderBy(r => r.Confirm);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "Id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "Name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Date);
                            break;
                        case "DateCreated":
                            filteredQuery = filteredQuery.OrderBy(r => r.DateCreated);
                            break;
                        case "Date":
                            filteredQuery = filteredQuery.OrderBy(r => r.ExpiryDate);
                            break;
                        case "Job":
                            filteredQuery = filteredQuery.OrderBy(r => r.Job.Title);
                            break;
                        case "Expiry Date":
                            filteredQuery = filteredQuery.OrderBy(r => r.ExpiryDate);
                            break;
                        case "Message":
                            filteredQuery = filteredQuery.OrderBy(r => r.Message);
                            break;
                        case "Read":
                            filteredQuery = filteredQuery.OrderBy(r => r.Read);
                            break;
                        case "Confirmed":
                            filteredQuery = filteredQuery.OrderBy(r => r.Confirm);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<NotificationGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }


        [HttpGet]
        public NotificationViewModel NotificationGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                NotificationViewModel NotificationViewModel = new NotificationViewModel();

                if (id > 0)
                {
                    NotificationViewModel = NotificationProvider.GetNotifications().Where(a => a.Id == id).Select(Notification => new NotificationViewModel()
                    {
                        Id = Notification.Id,
                        DateCreated = DateTime.Now,
                        Date = Notification.Date,
                        ExpiryDate = Notification.ExpiryDate,
                        Read = Notification.Read,
                        Confirm = Notification.Confirm,
                        Message = Notification.Message,
                        JobId = Notification.JobId,
                        UseridentityId = CurrentUser.Id,


                        //Notifications = Notificationes.Notifications,



                    }).Single();

                }
                return NotificationViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}