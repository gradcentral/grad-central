﻿using GradCentral.BL.Context;
using GradCentral.BL.Entities.DepartmentData;
using GradCentral.BL.Provider.Department;
using GradCentral.Controllers;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Department;
using GradCentralUI.Models.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace GradCentralUI.Controllers
{
    [Authorize]
    [NoCache]
    public class DepartmentController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private IDepartmentProvider DepartmentProvider { get; set; }

        public DepartmentController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            DepartmentProvider = new DepartmentProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public DepartmentViewModel SaveDepartment(DepartmentViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                    var errors = ModelState.Values.SelectMany(v => v.Errors);
                }
                Department newDepartment = new Department()
                {
                    Id = model.Id,
                    Name = model.Name,
                    FacultyId = model.FacultyId
                };

                Department SavedDepartment = DepartmentProvider.SaveDepartment(newDepartment);

                DepartmentViewModel viewModelDepartment = new DepartmentViewModel()
                {
                    Id = SavedDepartment.Id,
                    Name = SavedDepartment.Name,
                    FacultyId = SavedDepartment.FacultyId
                };

                return viewModelDepartment;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public List<DepartmentViewModel> DepartmentList()
        {
            List<Department> DepartmentList = DepartmentProvider.GetDepartments().OrderBy(b => b.Name).ToList();

            var model = DepartmentList.Select(Departments => new DepartmentViewModel()
            {
                Id = Departments.Id,
                Name = Departments.Name,
                FacultyId = Departments.FacultyId,
                Faculty = new GradCentral.Models.Faculty.FacultyViewModel()
                {
                    Id = Departments.Faculty.Id,
                    Name = Departments.Faculty.Name
                }
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<DepartmentGridModel> DepartmentGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = DepartmentProvider.GetDepartments().Select(Departments => new DepartmentGridModel()
            {
                Id = Departments.Id,
                Name = Departments.Name,
                FacultyId = Departments.FacultyId,
                Faculty = new GradCentral.Models.Faculty.FacultyViewModel()
                {
                    Id = Departments.Faculty.Id,
                    Name = Departments.Faculty.Name
                }
            });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Faculty.Name.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "faculty":
                            filteredQuery = filteredQuery.OrderBy(r => r.Faculty.Name);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "faculty":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Faculty.Name);
                            break;
                    }
                    break;
            }
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);
            return new GridResultModel<DepartmentGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public DepartmentViewModel DepartmentGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                DepartmentViewModel model = new DepartmentViewModel();

                if (id > 0)
                {
                    model = DepartmentProvider.GetDepartments().Where(a => a.Id == id).Select(Comp => new DepartmentViewModel()
                    {
                        Name = Comp.Name,
                        FacultyId = Comp.FacultyId,
                        Faculty = new GradCentral.Models.Faculty.FacultyViewModel()
                        {
                            Id = Comp.Faculty.Id,
                            Name = Comp.Faculty.Name
                        }
                    }).Single();

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}