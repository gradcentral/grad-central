﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using GradCentral.Controllers.Filters;
using GradCentral.Models.Enums;
using GC.Lib.Utility;

namespace GradCentral.Controllers
{
    [NoCache]
    public class EnumsController:TCRControllerBase
    {
        [HttpGet]
        public List<CountryModel> Countries()
        {
            using (var context = new DataContext())
            {
                var countries = context.CountrySet.Select(a => new CountryModel()
                {
                    Id = a.ISOCountryId,
                    CountryName = a.CountryName
                }).OrderBy(a => a.CountryName).ToList();

                var rsa = countries.Where(a => a.Id == 710).SingleOrDefault();
                if (rsa != null)
                {
                    countries.Remove(rsa);
                    countries.Insert(0, rsa);
                }
                return countries;
            }
        }

        [HttpGet]
        public List<SecurityTypeModel> SecurityEnum()
        {
            List<SecurityTypeModel> result = new List<SecurityTypeModel>();
            foreach (PrivilegeType t in Enum.GetValues(typeof(PrivilegeType)))
                result.Add(new SecurityTypeModel(t));

            return result;
        }

        [HttpGet]
        public List<EnumModel> StatusTypeEnum()
        {
            return EnumToModel<StatusType>();
        }
        
        [HttpGet]
        public List<EnumModel> UserTypeEnum()
        {
            return EnumToModel<UserType>();
        }

        [HttpGet]
        public List<EnumModel> ProvinceTypeEnum()
        {
            return EnumToModel<ProvinceType>();
        }

        [HttpGet]
        public List<EnumModel>TertiaryTypeEnum()
        {
            return EnumToModel<TertiaryTypes>();
        }

        [HttpGet]
        public List<EnumModel> SemesterTypeEnum()
        {
            return EnumToModel<SemesterTypes>();
        }

        [HttpGet]
        public List<EnumModel> TitleTypeEnum()
        {
            return EnumToModel<TitleType>();
        }

        [HttpGet]
        public List<EnumModel> GenderTypeEnum()
        {
            return EnumToModel<GenderType>();
        }

        [HttpGet]
        public List<EnumModel> RaceTypeEnum()
        {
            return EnumToModel<RaceType>();
        }
        [HttpGet]
        public List<EnumModel> PositionTypeEnum()
        {
            return EnumToModel<PositionTypes>();
        }

        [HttpGet]
        public List<EnumModel> AttachmentFormatTypeEnum()
        {
            return EnumToModel<AttachmentFormat>();
        }

        [HttpGet]                        ///Qualifications
        public List<EnumModel> QualificationTypeEnum()
        {
            return EnumToModel<QualificationType>();
        }

        private List<EnumModel> EnumToModel<T>(bool sort = true)
        {
            List<EnumModel> result = new List<EnumModel>();

            foreach (T eValue in Enum.GetValues(typeof(T)))
            {
                result.Add(new EnumModel() { Description = NameSplitting.SplitCamelCase(eValue), Value = eValue.ToString() });
            }

            if (sort)
            {
                result = result.OrderBy(a => a.Description).ToList();
            }
            return result;
        }
    }
}