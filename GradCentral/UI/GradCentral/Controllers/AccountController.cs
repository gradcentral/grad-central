﻿using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Threading;
using System.Web.Http;
using System.Web.Security;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Email;
using GradCentral.BL.Provider.Security;
using GradCentral.BL.Provider.Settings;
using GradCentral.Controllers.Filters;
using GradCentral.Models.Account;
using GradCentral.Models.SecurityData;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using GradCentralUI.Extentions;
using System.Web;
using System;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class AccountController:TCRControllerBase
    {
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.Security.ISecurityProvider SecurityProvider { get; set; }
        private IEmailProvider EmailProvider { get; set; }
        private IAppSettings AppSettings { get; set; }

        public AccountController()
        {
            Context = new DataContext();
            AppSettings = new AppSettings();
            _DisposeContext = true;
            SecurityProvider = new SecurityProvider(Context, CurrentUser);
            EmailProvider = new EmailProvider(Context,AppSettings);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);
        }

        [HttpPost]
        [AllowAnonymous]
        public CurrentUserModel Login(LoginViewModel model)
        {
            SignOut(0);

            try
            {
                if (!ModelState.IsValid)
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, "Invalid Model"));

                var user = SecurityProvider.UserLogin(model.UserName, model.Password);
                CurrentUser = new CurrentUserModel(user);
                FormsAuthentication.SetAuthCookie(model.UserName, model.RememberMe);
                return CurrentUser;

            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [HttpPost] // This is from System.Web.Http, and not from System.Web.Mvc
        public async Task<HttpResponseMessage> Upload()
        {
            if (!Request.Content.IsMimeMultipartContent())
            {
                this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            }

            var provider = GetMultipartProvider();
            var result = await Request.Content.ReadAsMultipartAsync(provider);

            // On upload, files are given a generic name like "BodyPart_26d6abe1-3ae1-416a-9429-b35f15e6e5d5"
            // so this is how you can get the original file name
            var originalFileName = GetDeserializedFileName(result.FileData.First());

            // uploadedFileInfo object will give you some additional stuff like file length,
            // creation time, directory name, a few filesystem methods etc..
            var uploadedFileInfo = new FileInfo(result.FileData.First().LocalFileName);

            // Remove this line as well as GetFormData method if you're not
            // sending any form data with your upload request
            var fileUploadObj = GetFormData<UploadDataModel>(result);

            // Through the request response you can return an object to the Angular controller
            // You will be able to access this in the .success callback through its data attribute
            // If you want to send something to the .error callback, use the HttpStatusCode.BadRequest instead
            var returnData = "ReturnTest";
            return this.Request.CreateResponse(HttpStatusCode.OK, new { returnData });
        }

        // You could extract these two private methods to a separate utility class since
        // they do not really belong to a controller class but that is up to you
        private MultipartFormDataStreamProvider GetMultipartProvider()
        {
            // IMPORTANT: replace "(tilde)" with the real tilde character
            // (our editor doesn't allow it, so I just wrote "(tilde)" instead)
            var uploadFolder = "(tilde)/App_Data/Tmp/FileUploads"; // you could put this to web.config
            var root = HttpContext.Current.Server.MapPath(uploadFolder);
            Directory.CreateDirectory(root);
            return new MultipartFormDataStreamProvider(root);
        }

        // Extracts Request FormatData as a strongly typed model
        private object GetFormData<T>(MultipartFormDataStreamProvider result)
        {
            if (result.FormData.HasKeys())
            {
                var unescapedFormData = Uri.UnescapeDataString(result.FormData
                    .GetValues(0).FirstOrDefault() ?? String.Empty);
                if (!String.IsNullOrEmpty(unescapedFormData))
                    return JsonConvert.DeserializeObject<T>(unescapedFormData);
            }

            return null;
        }

        private string GetDeserializedFileName(MultipartFileData fileData)
        {
            var fileName = GetFileName(fileData);
            return JsonConvert.DeserializeObject(fileName).ToString();
        }

        public string GetFileName(MultipartFileData fileData)
        {
            return fileData.Headers.ContentDisposition.FileName;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<HttpResponseMessage> AddFile(HttpRequestMessage request)
        {
            try
            {
                if (!Request.Content.IsMimeMultipartContent())
                {
                    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }

                var data = await Request.Content.ParseMultipartAsync();

                if (data.File.ContainsKey("file"))
                {
                    var file = data.File["file"].File;
                    var fileName = data.File["file"].FileName;

                }

                if (data.Fields.ContainsKey("description"))
                {
                    var description = data.Fields["description"].Value;
                }

                return new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("Thank you for uploading your transcript")
                };

                //string root = System.Web.HttpContext.Current.Server.MapPath("~/temp/uploads");
                //var provider = new MultipartFormDataStreamProvider(root);
                //var result = await Request.Content.ReadAsMultipartAsync(provider);

                //// On upload, files are given a generic name like "BodyPart_26d6abe1-3ae1-416a-9429-b35f15e6e5d5"
                //// so this is how you can get the original file name
                //var originalFileName = GetDeserializedFileName(result.FileData.First());

                //var uploadedFileInfo = new FileInfo(result.FileData.First().LocalFileName);
                //string path = result.FileData.First().LocalFileName;

                ////Do whatever you want to do with your file here

                //return this.Request.CreateResponse(HttpStatusCode.OK, originalFileName);
            }
            catch (System.Exception e)
            {

                throw;
            }
        }

        //[HttpPost]
        //public Task<HttpResponseMessage> PostFile(string name, string fileName, byte[] file) {
        //    try
        //    {

        //    }
        //    catch (System.Exception e)
        //    {
        //        return null;
        //    }
        //}

        //private string GetDeserializedFileName(MultipartFileData fileData)
        //{
        //    var fileName = GetFileName(fileData);
        //    return JsonConvert.DeserializeObject(fileName).ToString();
        //}

        //public string GetFileName(MultipartFileData fileData)
        //{
        //    return fileData.Headers.ContentDisposition.FileName;
        //}

        [HttpPost]
        [AllowAnonymous]
        public CurrentUserModel Register(RegisterViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var user = SecurityProvider.SignUp(model.UserName,model.Title,model.FirstName,model.Surname,model.Password,model.Email, model.FromCountry, model.ToCountry, model.Gender);

                    var aCurrentUser = SecurityProvider.UserIdentityToCurrentUser(user);
                    CurrentUser = new CurrentUserModel(aCurrentUser);

                    FormsAuthentication.SetAuthCookie(model.Email, false);

                    return CurrentUser;
                }

                string message = ModelState.Values.SelectMany(x => x.Errors).Select(x => x.ErrorMessage).First();
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, message));
            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [Authorize]
        public CurrentUserModel GetCurrentUser()
        {
            var aUser = SecurityProvider.GetCurrentUser(Thread.CurrentPrincipal.Identity.Name);
            CurrentUser = new CurrentUserModel(aUser);

            return CurrentUser;
        }


        [HttpPost]
        public void SignOut(int? Id)
        {
            FormsAuthentication.SignOut();
            CurrentUser = null;
        }

        [HttpPost]
        public ChangePasswordModel ChangePassword(ChangePasswordModel model)
        {
            try
            {
                var changePassword = SecurityProvider.ChangeMyPassword(model.OldPassword, model.NewPassword);

                var model1 = new ChangePasswordModel()
                {
                    NewPassword = changePassword.PasswordHash
                };

                return (model1);
            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }

        [AllowAnonymous]
        public void SendPassword(UserEditModel model)
        {
            if(model.UserName == null)
                return;
            try
            {
                SecurityProvider.ResetUserPassword(model.UserName, EmailProvider);
            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e.Message));
            }
        }
    }
}