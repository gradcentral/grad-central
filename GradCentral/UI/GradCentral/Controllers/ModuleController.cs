﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.ModuleData;
using GradCentral.BL.Provider.Module;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Module;
using GradCentral.Models.QualificationData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class ModuleController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.Module.IModuleProvider ModuleProvider { get; set; }

        public ModuleController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            ModuleProvider = new ModuleProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public ModuleViewModel SaveModule(ModuleViewModel model)
        {
            try
             {
                if (!ModelState.IsValid)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                    //var errors = ModelState.Values.SelectMany(v => v.Errors);
                }
                Module m = new Module()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Semester = model.SemesterType,
                    QualificationId = model.QualificationId

                };

                Module Module1 = ModuleProvider.SaveModule(m);

                ModuleViewModel Module = new ModuleViewModel()
                {
                    Id = Module1.Id,
                    Name = Module1.Name,
                    SemesterType = Module1.Semester,
                    QualificationId = Module1.QualificationId
                };
                
                return Module;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public List<ModuleViewModel> ModuleList()
        {
            List<Module> moduleList = ModuleProvider.GetModules().OrderBy(b => b.Name).ToList();
            
            var model = moduleList.Select(modules => new ModuleViewModel()
            {
                Id = modules.Id,
                Name = modules.Name,
                SemesterType = modules.Semester,
                QualificationId = modules.QualificationId,

                Qualification = new QualificationViewModel()
                {
                    Id = modules.Qualification.Id,
                    Name = modules.Qualification.Name
                }
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<ModuleGridModel> ModuleGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = ModuleProvider.GetModules().Select(modules => new ModuleGridModel()
                {
                    Id = modules.Id,
                    Name = modules.Name,
                    SemesterTypes = modules.Semester, 
                    QualificationId = modules.QualificationId,

                    Qualification = new QualificationViewModel()
                    {
                        Id = modules.Qualification.Id,
                        Name = modules.Qualification.Name
                    }
                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Semester.ToString().Contains(model.Searchfor) || r.Id.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "semester":
                            filteredQuery = filteredQuery.OrderBy(r => r.Semester);
                            break;
                        case "qualificationName":
                            filteredQuery = filteredQuery.OrderBy(r => r.Qualification.Name);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "semester":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Semester);
                            break;
                        case "qualificationName":
                            filteredQuery = filteredQuery.OrderBy(r => r.Qualification.Name);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<ModuleGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public ModuleViewModel ModuleGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                ModuleViewModel model = new ModuleViewModel();  

                if (id > 0)
                {
                    model = ModuleProvider.GetModules().Where(a => a.Id == id).Select(module => new ModuleViewModel()
                    {
                        Id = module.Id,
                        Name = module.Name,
                        SemesterType = module.Semester,
                        QualificationId = module.QualificationId,

                        Qualification = new QualificationViewModel()
                        {
                            Id = module.Id,
                            Name = module.Name
                        }
                    }).Single();

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}