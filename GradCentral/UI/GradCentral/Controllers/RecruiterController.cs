﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Provider.Recruiter;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentralUI.Models.SecurityData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class RecruiterController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private IRecruiterProvider RecruiterProvider { get; set; }

        public RecruiterController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            RecruiterProvider = new RecruiterProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public RecruiterViewModel SaveRecruiter(RecruiterViewModel model)
        {
            try
             {
                if (!ModelState.IsValid)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                }
                Recruiter newRecruiter = new Recruiter()
                {
                    Id = model.Id,
					Jobs = model.Jobs,
                    CompanyId = model.CompanyId,
                    UserIdentityId = model.UserIdentityId,
                    Events = model.Events,
                    IdType = model.IdType
                };

                Recruiter savedRecruiter = RecruiterProvider.SaveRecruiter(newRecruiter);

                 return  new RecruiterViewModel()
                {
                    Id = savedRecruiter.Id,
                    Jobs = savedRecruiter.Jobs,
                    CompanyId = savedRecruiter.CompanyId,
                    UserIdentityId = savedRecruiter.UserIdentityId,
                    Events = savedRecruiter.Events,
                    IdType = savedRecruiter.IdType
                };
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public List<RecruiterViewModel> RecruiterList()
        {
            List<Recruiter> RecruiterList = RecruiterProvider.GetRecruiters().OrderBy(b => b.UserIdentity.FirstName).ToList();
            
            var model = RecruiterList.Select(recruiters => new RecruiterViewModel()
            {
                Id = recruiters.Id,
                Jobs = recruiters.Jobs,
                CompanyId = recruiters.CompanyId,
                Company = new Models.Company.CompanyViewModel()
                {
                    Id = recruiters.Company.Id,
                    Name = recruiters.Company.Name,
                    Email = recruiters.Company.Email
                },
                UserIdentityId = recruiters.UserIdentityId,
                UserIdentity = new Models.SecurityData.UserEditModel()
                {
                    Id = recruiters.UserIdentity.Id,
                    FirstName = recruiters.UserIdentity.FirstName,
                    Surname = recruiters.UserIdentity.Surname
                },
                Events = recruiters.Events,
                IdType = recruiters.IdType
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<RecruiterGridModel> RecruiterGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = RecruiterProvider.GetRecruiters().Select(recruiters => new RecruiterGridModel()
                {
                    Id = recruiters.Id,
                    Jobs = recruiters.Jobs,
                    UserIdentity = recruiters.UserIdentity,
                    Company = recruiters.Company,
                    CompanyId = recruiters.CompanyId,
                    UserIdentityId = recruiters.UserIdentityId,
                    Events = recruiters.Events,
                    IdType = recruiters.IdType
            });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.UserIdentity.FirstName.Contains(model.Searchfor) || 
                        r.UserIdentity.Surname.ToString().Contains(model.Searchfor) || 
                        r.Id.ToString().Contains(model.Searchfor) ||
                        r.Company.Name.ToString().Contains(model.Searchfor) ||
                        r.Company.Email.ToString().Contains(model.Searchfor) ||
                        r.Jobs.Count.ToString().Contains(model.Searchfor) );

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserIdentity.FirstName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "firstName":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.FirstName);
                            break;
                        case "surnam":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.Surname);
                            break;
                        case "company":
                            filteredQuery = filteredQuery.OrderBy(r => r.Company.Name);
                            break;
                        case "jobs":
                            filteredQuery = filteredQuery.OrderBy(r => r.Jobs.Count);
							break;
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "email":
                            filteredQuery = filteredQuery.OrderBy(r => r.Company.Email);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "firstName":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.FirstName);
                            break;
                        case "surnam":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.Surname);
                            break;
                        case "company":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Company.Name);
                            break;
                        case "jobs":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Jobs.Count);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "email":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Company.Email);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<RecruiterGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public RecruiterViewModel RecruiterGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                RecruiterViewModel model = new RecruiterViewModel();

                if (id > 0)
                {
                    model = RecruiterProvider.GetRecruiters().Where(a => a.Id == id).Select(recruiter => new RecruiterViewModel()
                    {
                        Id = recruiter.Id,
                        Jobs = recruiter.Jobs,
                        UserIdentity = new Models.SecurityData.UserEditModel()
                        {
                            Id = recruiter.UserIdentity.Id,
                            FirstName = recruiter.UserIdentity.FirstName,
                            Surname = recruiter.UserIdentity.Surname
                        },
                        Company = new Models.Company.CompanyViewModel()
                        {
                            Id = recruiter.Company.Id,
                            Name = recruiter.Company.Name,
                            Email = recruiter.Company.Email
                        },
                        CompanyId = recruiter.CompanyId,
                        UserIdentityId = recruiter.UserIdentityId,
                        Events = recruiter.Events,
                        IdType = recruiter.IdType
                    }).Single();

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}