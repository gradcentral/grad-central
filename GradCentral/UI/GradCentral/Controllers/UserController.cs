﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using GradCentral.BL.Provider.Graduate;
using GradCentral.BL.Provider.Recruiter;
using GradCentral.BL.Provider.Security;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.SecurityData;
using GradCentralUI.Models.SecurityData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class UserController : TCRControllerBase
    {
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private ISecurityProvider SecurityProvider { get; set; }

        private IGraduateProvider GraduateProvider { get; set; }

        private IRecruiterProvider RecruiterProvider { get; set; }

        public UserController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            SecurityProvider = new SecurityProvider(Context, CurrentUser);
            GraduateProvider = new GraduateProvider(Context, CurrentUser);
            RecruiterProvider = new RecruiterProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();
            base.Dispose(disposing);
        }

        [HttpPost]
        public UserEditModel UserSave(UserEditModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));


                if (model.Recruiter == null)
                {
                    model.Recruiter = new Recruiter();
                }
                else
                {
                    model.Graduate = new Graduate();
                }

                var userRole = SecurityProvider.SaveUser(model.Title, model.Type, model.Id,
                    model.UserName, model.FirstName, model.Surname, model.Email,
                    model.RoleList.Select(a => a.RoleId).ToList(), 
                    model.Graduate != null?model.Graduate.IsEmployed :false,
                    model.Graduate != null ? model.Graduate.JobId :null
                    , model.Graduate != null ? model.Graduate.TertiaryId :0,
                    model.Recruiter != null ? model.Recruiter.CompanyId :0, model.LockedOut,
                    model.Graduate != null ? model.Graduate.IsNasfas : false,
                    model.CellNumber,
                    model.Gender,
                    model.Graduate.IsDisable,
                    model.Graduate != null ? model.Graduate.Race : RaceType.Other,
                    model.Graduate != null ? model.Graduate.DisabilityDescription : null,
                    model.Graduate != null ? model.Graduate.CurrentEmployer : null, model.Marks, model.Graduate.Qualifications);

                var user = new UserEditModel()
                {
                    Title = userRole.Title,
                    Id = userRole.Id,
                    UserName = userRole.UserName,
                    FirstName = userRole.FirstName,
                    Surname = userRole.Surname,
                    Email = userRole.Email,
                    Type = userRole.Type,
                    RoleList = userRole.Roles.Select(a => new UserRoleModel()
                    {
                        RoleId = a.Id,
                        RoleName = a.RoleName,
                        Selected = false
                    }).ToList()
                };

                return user;
            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpPost]
        public GridResultModel<UserGridModel> UserGrid(GridModel model)
        {

            int begin = SetupGridParams(model);


            var filteredQuery = SecurityProvider.GetUserList().Select(a => new UserGridModel()
            {
                Id = a.Id,
                UserName = a.UserName,
                FirstName = a.FirstName,
                Surname = a.Surname,
                Email = a.Email,
                LockedOut = a.LockedOut,
                Type = a.Type,
                Gender = a.Gender,
            });

            if (model.Searchfor != "null")
            {
                filteredQuery = filteredQuery.Where(r => r.UserName.Contains(model.Searchfor)
                                                    || r.FirstName.Contains(model.Searchfor)
                                                    || r.Surname.Contains(model.Searchfor)
                                                    || r.Email.Contains(model.Searchfor)
                                                         || r.Type.ToString().Contains(model.Searchfor)
                                                         || r.LockedOut.ToString().Contains(model.Searchfor)
                                                     || r.GenderString.Contains(model.Searchfor));
            }
            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            //Setup sort order
            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserName);
                            break;
                        case "type":
                            filteredQuery = filteredQuery.OrderBy(r => r.Type);
                            break;
                        case "email":
                            filteredQuery = filteredQuery.OrderBy(r => r.Email);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(r => r.Surname);
                            break;
                        case "lockedOut":
                            filteredQuery = filteredQuery.OrderBy(r => r.LockedOut);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderBy(r => r.GenderString);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "username":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserName);
                            break;
                        case "type":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Type);
                            break;
                        case "email":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Email);
                            break;
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Surname);
                            break;
                        case "lockedOut":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.LockedOut);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Gender);
                            break;
                    }
                    break;
            }

            //setup paging
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);


            return new GridResultModel<UserGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public List<UserEditModel> UserList()
        {
            var itms = SecurityProvider.GetUserList()
                                     //.Where(a => a.Active == true)
                                     //.Select(b => new KeyValueModel { Id = b.Id, Description = b.UserName })
                                     .OrderBy(b => b.FirstName)
                                     .ToList();
            return itms.Select(a => new UserEditModel()
            {
                Email = a.Email,
                FirstName = a.FirstName,
                Id = a.Id,
                IsAdmin = a.IsSystemAdmin,
                LockedOut = a.LockedOut,
                Surname = a.Surname,
                Title = a.Title,
                UserName = a.UserName,
                Type = a.Type,
                Gender = a.Gender
            }).ToList();
        }

        [HttpGet]
        public UserEditModel UserGet(long? id)
        {
            if (id == null)
                id = 0;
            try
            {
                UserEditModel model = new UserEditModel();
                var filteredQuery = SecurityProvider.GetUserList().Where(a => a.Id == id)
                    .Select(a => new UserEditModel()
                    {
                        Title = a.Title,
                        Id = a.Id,
                        UserName = a.UserName,
                        FirstName = a.FirstName,
                        Surname = a.Surname,
                        Email = a.Email,
                        IsAdmin = a.IsSystemAdmin,
                        Type = a.Type,
                        LockedOut = a.LockedOut,
                        Gender = a.Gender,
                        CellNumber = a.CellNumber
                    });

                model = filteredQuery.Single();

                model.RoleList = SecurityProvider.GetRoles().Where(r => r.Status == StatusType.Active).Select(a => new UserRoleModel()
                {
                    RoleId = a.Id,
                    RoleName = a.RoleName,
                    Selected = false
                }).ToList();

                BL.Entities.GraduateData.Graduate graduate = GraduateProvider.GetGraduates()
                    .FirstOrDefault(g => g.UserIdentityId == model.Id);
                if (graduate != null)
                {
                    model.Graduate = new Graduate();
                    model.Graduate.Id = graduate.Id;
                    model.Graduate.IsEmployed = graduate.IsEmployed;
                    model.Graduate.JobId = graduate.JobId;
                    model.Graduate.TertiaryId = graduate.TertiaryId;
                    model.Graduate.UserIdentityId = graduate.UserIdentityId;
                    model.Graduate.IsDisable = graduate.IsDisable;
                    model.Graduate.DisabilityDescription = graduate.DisabilityDescription;
                    model.Graduate.Race = graduate.Race;
                    model.Graduate.CurrentEmployer = graduate.CurrentEmployer;
                    model.Graduate.IsNasfas = graduate.IsNasfas;
                }

                BL.Entities.RecruiterData.Recruiter recruiter = RecruiterProvider.GetRecruiters()
                    .FirstOrDefault(r => r.UserIdentityId == model.Id);

                if (recruiter != null)
                {
                    model.Recruiter = new Recruiter();
                    model.Recruiter.Id = recruiter.Id;
                    model.Recruiter.CompanyId = recruiter.CompanyId;
                    model.Recruiter.UserIdentityId = recruiter.UserIdentityId;
                }

                var selectedRoles = SecurityProvider.GetUserList().Where(a => a.Id == id)
                                                    .Select(a => a.Roles)
                                                    .Single()
                                                    .Select(a => a.Id).ToArray();

                foreach (var itm in model.RoleList.Where(a => selectedRoles.Contains(a.RoleId)).ToList())
                {
                    itm.Selected = true;
                }
                return model;
            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public UserEditModel UserUnlock(long id)
        {
            try
            {

                var filteredQuery = SecurityProvider.UnlockAccount(id);

                UserEditModel model = new UserEditModel();
                model.LockedOut = filteredQuery.LockedOut;
                return model;
            }
            catch (SecurityException e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }
    }
}