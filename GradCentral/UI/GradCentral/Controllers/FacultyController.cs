using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.TertiaryData.Faculty;
using GradCentral.BL.Provider.Tertiary.Faculty;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Faculty;
using GradCentral.Models.Tertiary;
using GradCentral.Models.Tertiary.Campus;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class FacultyController : TCRControllerBase
    {

        #region Constructor

        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.Tertiary.Faculty.IFacultyProvider FacultyProvider { get; set; }

        public FacultyController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            FacultyProvider = new FacultyProvider(Context, CurrentUser);
        }


        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        #endregion


        [HttpPost]
        public FacultyViewModel SaveFaculty(FacultyViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {

                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Error details"));

                }
                Faculty t = new Faculty()
                {
                    CampusId = model.CampusId,
                    Name = model.Name,
                    Qualifications = model.Qualifications,
                    Campus = model.Campus
                };
                var Faculty1 = FacultyProvider.SaveFaculty(t);

                var Faculty = new FacultyViewModel()
                {
                    CampusId = Faculty1.CampusId,
                    Name = Faculty1.Name,
                    Qualifications = Faculty1.Qualifications,
                    Campus = Faculty1.Campus

                };

                return Faculty;


            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }

        [HttpGet]
        public List<FacultyViewModel> FacultyList()
        {
            List<Faculty> FacultyList = FacultyProvider.GetFaculties().OrderBy(b => b.Name).ToList();

            var FacultyViewModel = FacultyList.Select(a => new FacultyViewModel()
            {
                Id = a.Id,
                CampusId = a.CampusId,
                Name = a.Name,
                Qualifications = a.Qualifications,
                Campus = a.Campus

            }).ToList();

            return FacultyViewModel;
        }

        [HttpPost]
        public GridResultModel<FacultyGridModel> FacultyGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = FacultyProvider.GetFaculties()
                .Select(a => new FacultyGridModel()
                {

                    Id = a.Id,
                    CampusId = a.CampusId,
                    Name = a.Name,
                    Qualifications = a.Qualifications,
                    Campus = a.Campus
                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Campus.Name.ToString().Contains(model.Searchfor));

            }

            //Get Record count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "campusName":
                            filteredQuery = filteredQuery.OrderBy(r => r.Campus.Name);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "campusName":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Campus.Name);
                            break;
                    }
                    break;
            }
            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<FacultyGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }


        [HttpGet]
        public FacultyViewModel FacultyGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                FacultyViewModel FacultyViewModel = new FacultyViewModel();

                if (id > 0)
                {
                    FacultyViewModel = FacultyProvider.GetFaculties().Where(a => a.Id == id).Select(a => new FacultyViewModel()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        CampusId = a.CampusId,
                        Campus = a.Campus

                    }).Single();
                
                }
                return FacultyViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}