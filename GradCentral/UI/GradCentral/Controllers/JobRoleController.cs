﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.JobData;
using GradCentral.BL.Provider.JobRole;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.JobRole;
using GradCentral.Models.QualificationData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class JobRoleController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.JobRole.IJobRoleProvider JobRoleProvider { get; set; }

        public JobRoleController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            JobRoleProvider = new JobRoleProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public JobRoleViewModel SaveJobRole(JobRoleViewModel model)
        {
            try
             {
                if (!ModelState.IsValid)
                {
                    //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                    var errors = ModelState.Values.SelectMany(v => v.Errors);
                }
                JobRole newJobRole = new JobRole()
                {
                    Id = model.Id,
                    Name = model.Name

                };

                JobRole SavedJobRole = JobRoleProvider.SaveJobRole(newJobRole);

                JobRoleViewModel viewModelJobRole = new JobRoleViewModel()
                {
                    Id = SavedJobRole.Id,
                    Name = SavedJobRole.Name
                };
                
                return viewModelJobRole;
            }
            catch (Exception e)
            {
                //throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
                var ex = e.Message;
            }
            return null;
        }

        [HttpGet]
        public List<JobRoleViewModel> JobRoleList()
        {
            List<JobRole> JobRoleList = JobRoleProvider.GetJobRoles().OrderBy(b => b.Name).ToList();
            
            var model = JobRoleList.Select(JobRoles => new JobRoleViewModel()
            {
                Id = JobRoles.Id,
                Name = JobRoles.Name
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<JobRoleGridModel> JobRoleGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = JobRoleProvider.GetJobRoles().Select(JobRoles => new JobRoleGridModel()
                {
                    Id = JobRoles.Id,
                    Name = JobRoles.Name
                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Id.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<JobRoleGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public JobRoleViewModel JobRoleGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                JobRoleViewModel model = new JobRoleViewModel();

                if (id > 0)
                {
                    model = JobRoleProvider.GetJobRoles().Where(a => a.Id == id).Select(JobRole => new JobRoleViewModel()
                    {
                        Id = id,
                        Name = JobRole.Name
                    }).Single();

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}