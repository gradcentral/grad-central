﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.TertiaryData.Campus;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Tertiary;
using GradCentral.BL.Provider.Tertiary.Campus;
using GradCentral.Models.Tertiary.Campus;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class CampusController : TCRControllerBase
    {

        #region Constructor

        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private BL.Provider.Tertiary.Campus.ICampusProvider CampusProvider { get; set; }

        public CampusController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            CampusProvider = new CampusProvider(Context, CurrentUser);
        }


        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        #endregion


        [HttpPost]
        public CampusViewModel SaveCampus(CampusViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {

                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Error details"));
                }

                Campus newCampus = new Campus()
                {
                    Id = model.Id,
                    Street = model.Street,
                    Number = model.Number,
                    Name = model.Name,
                    Town = model.Town,
                    City= model.City,
                    Zip = model.Zip,
                    TertiaryId = model.TertiaryId,
                    Province = model.Province
                };

                Campus SavedCampus = CampusProvider.SaveCampus(newCampus);


                CampusViewModel viewModelCampus = new CampusViewModel()
                {
                    Id = SavedCampus.Id,
                    Name = SavedCampus.Name,
                    Street = SavedCampus.Street,
                    Number = SavedCampus.Number,
                    Town = SavedCampus.Town,
                    City = SavedCampus.City,
                    Zip = SavedCampus.Zip,
                    TertiaryId = SavedCampus.TertiaryId
                };

                return viewModelCampus;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }

        [HttpGet]
        public List<CampusViewModel> CampusList()
        {
            List<Campus> campusList = CampusProvider.GetCampuses().OrderBy(b => b.Name).ToList();

            var model = campusList.Select(campuses => new CampusViewModel()
            {
                Id = campuses.Id,
                Name = campuses.Name,
                Street = campuses.Street,
                Number = campuses.Number,
                Town = campuses.Town,
                City = campuses.City,
                Zip = campuses.Zip,
                TertiaryId = campuses.TertiaryId,

                //Qualifications = campuses.Qualifications,
                Tertiary = new TertiaryViewModel() {
                    Id = campuses.Tertiary.Id,
                    Name= campuses.Tertiary.Name,
                    TertiaryType = campuses.Tertiary.TertiaryType,
                    //Campuses= campuses.c,
                    Graduates = campuses.Tertiary.Graduates
                }

            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<CampusGridModel> CampusGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = CampusProvider.GetCampuses()
                .Select(campus => new CampusGridModel()
                {

                    Id = campus.Id,
                    Name = campus.Name,
                    Street = campus.Street,
                    Number = campus.Number,
                    Town = campus.Town,
                    City = campus.City,
                    Zip = campus.Zip,
                    TertiaryId = campus.TertiaryId,

                    //Qualifications = campus.Qualifications,
                    Tertiary = new TertiaryViewModel()
                    {
                        Id = campus.Tertiary.Id,
                        Name = campus.Tertiary.Name,
                        TertiaryType = campus.Tertiary.TertiaryType,
                        Graduates = campus.Tertiary.Graduates

                    }

                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Street.Contains(model.Searchfor) || r.Number.ToString().Contains(model.Searchfor) || r.Town.Contains(model.Searchfor) || r.Zip.ToString().Contains(model.Searchfor) || r.Tertiary.Name.Contains(model.Searchfor) || r.Id.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "number":
                            filteredQuery = filteredQuery.OrderBy(r => r.Number);
                            break;
                        case "street":
                            filteredQuery = filteredQuery.OrderBy(r => r.Street);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderBy(r => r.City);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "town":
                            filteredQuery = filteredQuery.OrderBy(r => r.Town);
                            break;
                        case "tertiaryName":
                            filteredQuery = filteredQuery.OrderBy(r => r.Tertiary.Name);
                            break;
                        case "zip":
                            filteredQuery = filteredQuery.OrderBy(r => r.Zip);
                            break;
                        //case "province":
                        //    filteredQuery = filteredQuery.OrderBy(r => r.Province);
                        //    break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "number":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Number);
                            break;
                        case "street":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Street);
                            break;
                        case "city":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.City);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "town":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Town);
                            break;
                        case "tertiaryName":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Tertiary.Name);
                            break;
                        case "zip":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Zip);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<CampusGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }


        [HttpGet]
        public CampusViewModel CampusGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                CampusViewModel CampusViewModel = new CampusViewModel();

                if (id > 0)
                {
                    CampusViewModel = CampusProvider.GetCampuses().Where(a => a.Id == id).Select(campus => new CampusViewModel()
                    {
                        Id = campus.Id,
                        Name = campus.Name,
                        Street = campus.Street,
                        Number = campus.Number,
                        Town = campus.Town,
                        City = campus.City,
                        Zip = campus.Zip,
                        TertiaryId = campus.TertiaryId,
                        Province = campus.Province,

                        //Qualifications = campus.Qualifications,
                        Tertiary = new TertiaryViewModel()
                        {
                            Id = campus.Tertiary.Id,
                            Name = campus.Tertiary.Name,
                            TertiaryType = campus.Tertiary.TertiaryType,
                            Graduates = campus.Tertiary.Graduates

                        }

                    }).Single();
                
                }
                return CampusViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}