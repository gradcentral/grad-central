﻿using System.Web.Http;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Account;
using GC.Lib.Session;
using GradCentralUI.Models;

namespace GradCentral.Controllers
{
    [NoCache]
    public class TCRControllerBase : ApiController
    {
        protected ISessionProvider SessionProvider { get; set; }

        public TCRControllerBase()
            : this(new SessionProvider())
        {

        }
        public TCRControllerBase(ISessionProvider sessionProvider)
        {
            SessionProvider = sessionProvider;
        }


        public CurrentUserModel CurrentUser
        {
            get
            {
                return SessionProvider.GetObject<CurrentUserModel>("CurrentUser");
            }
            set
            {
                SessionProvider.Add<CurrentUserModel>("CurrentUser", value);
            }
        }

        protected int SetupGridParams(GridModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Searchfor))
                model.Searchfor = "null";
            if (string.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = string.Empty;
            if (string.IsNullOrWhiteSpace(model.SortOrder))
                model.SortOrder = "ASC";

            model.SortKey = model.SortKey.ToLower();

            if (model.CurrentPage == null)
                model.CurrentPage = 1;
            if (model.RecordsPerPage == null)
                model.RecordsPerPage = 100;

            int begin = (model.CurrentPage.Value - 1) * model.RecordsPerPage.Value;
            return begin;
        }

        protected int SetupCandidateGridParams(CandidatesGridModel model)
        {
            if (string.IsNullOrWhiteSpace(model.Searchfor))
                model.Searchfor = "null";
            if (string.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = string.Empty;
            if (string.IsNullOrWhiteSpace(model.SortOrder))
                model.SortOrder = "ASC";

            model.SortKey = model.SortKey.ToLower();

            if (model.CurrentPage == null)
                model.CurrentPage = 1;
            if (model.RecordsPerPage == null)
                model.RecordsPerPage = 100;

            int begin = (model.CurrentPage.Value - 1) * model.RecordsPerPage.Value;
            return begin;
        }
    }
}