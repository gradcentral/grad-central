﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Provider.Graduate;
using GradCentral.BL.Provider.Tertiary;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentralUI.Models.SecurityData;
using GradCentralUI.Models;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class GraduateController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private IGraduateProvider GraduateProvider { get; set; }

        public GraduateController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            GraduateProvider = new GraduateProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public GraduateViewModel SaveGraduate(GraduateViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                }
                Graduate newGraduate = new Graduate()
                {
                    UserIdentity = model.UserIdentity,
                    UserIdentityId = model.UserIdentityId,
                    JobId = model.JobId,
                    TertiaryId = model.TertiaryId,
                    IsEmployed = model.IsEmployed,
                    Tertiary = model.Tertiary,
                    Qualifications = model.Qualifications,
                    Job = model.Job,
                    IsNasfas = model.IsNasfas
                };


                Graduate savedGraduate = GraduateProvider.SaveGraduate(newGraduate);

                return new GraduateViewModel()
                {
                    Id = savedGraduate.Id,
                    UserIdentity = savedGraduate.UserIdentity,
                    UserIdentityId = savedGraduate.UserIdentityId,
                    JobId = savedGraduate.JobId,
                    TertiaryId = savedGraduate.TertiaryId,
                    IsEmployed = savedGraduate.IsEmployed,
                    Tertiary = savedGraduate.Tertiary,
                    Qualifications = savedGraduate.Qualifications,
                    Job = savedGraduate.Job,
                    IsNasfas = model.IsNasfas
                };
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public List<GraduateViewModel> GetJobCandidates(int jobId) {
            List<Graduate> graduateList = GraduateProvider.GetJobCandidates(jobId).OrderBy(b => b.UserIdentity.FirstName).ToList();

            var model = graduateList.Select(graduates => new GraduateViewModel()
            {
                Id = graduates.Id,
                UserIdentity = graduates.UserIdentity,
                UserIdentityId = graduates.UserIdentityId,
                JobId = graduates.JobId,
                TertiaryId = graduates.TertiaryId,
                IsEmployed = graduates.IsEmployed,
                Tertiary = graduates.Tertiary,
                Qualifications = graduates.Qualifications,
                Job = graduates.Job,
                IsNasfas = graduates.IsNasfas,
                Race = graduates.Race,
                IsDisable = graduates.IsDisable,
                DisablityDescription = graduates.DisabilityDescription,
                CurrectEmployer = graduates.CurrentEmployer
            }).ToList();

            return model;
        }

        [HttpGet]
        public List<GraduateViewModel> GraduateList()
        {
            List<Graduate> graduateList = GraduateProvider.GetGraduates().OrderBy(b => b.UserIdentity.FirstName).ToList();

            var model = graduateList.Select(graduates => new GraduateViewModel()
            {
                Id = graduates.Id,
                UserIdentity = graduates.UserIdentity,
                UserIdentityId = graduates.UserIdentityId,
                JobId = graduates.JobId,
                TertiaryId = graduates.TertiaryId,
                IsEmployed = graduates.IsEmployed,
                Tertiary = graduates.Tertiary,
                Qualifications = graduates.Qualifications,
                Job = graduates.Job,
                IsNasfas = graduates.IsNasfas,
                Race = graduates.Race,
                IsDisable = graduates.IsDisable,
                DisablityDescription = graduates.DisabilityDescription,
                CurrectEmployer = graduates.CurrentEmployer
            }).ToList();

            return model;
        }




        [HttpPost]
        public GridResultModel<GraduateGridModel> PossibleCandidatesGrid(CandidatesGridModel model)
        {
            int begin = SetupCandidateGridParams(model);
            TertiaryProvider tertiaryProvider = new TertiaryProvider(new DataContext(), CurrentUser);

            var filteredQuery = GraduateProvider.GetPossibleCandidates(model.IsDisable,
                model.Race, model.Gender, model.QualificationId, model.GPA).Select(graduates => new GraduateGridModel()
            {
                Id = graduates.Id,
                UserIdentity = graduates.UserIdentity,
                UserIdentityId = graduates.UserIdentityId,
                JobId = graduates.JobId,
                TertiaryId = graduates.TertiaryId,
                IsEmployed = graduates.IsEmployed,
                Tertiary = tertiaryProvider.GetTertiary(graduates.TertiaryId),
                Qualifications = graduates.Qualifications,
                Job = graduates.Job,
                IsNasfas = graduates.IsNasfas,
                Race = graduates.Race,
                IsDisable = graduates.IsDisable,
                DisabilityDescription = graduates.DisabilityDescription,
                CurrentEmployer = graduates.CurrentEmployer,
            });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.UserIdentity.FirstName.ToString().Contains(model.Searchfor) ||
                        r.UserIdentity.Surname.ToString().Contains(model.Searchfor) ||
                        r.Id.ToString().Contains(model.Searchfor) ||
                        r.IsEmployed.ToString().Contains(model.Searchfor) ||
                       (r.Qualifications != null ? r.Qualifications.Select(a => a.Name).Contains(model.Searchfor) : r.Tertiary.Name.ToString().Contains(model.Searchfor)) ||
                        r.Tertiary.Name.ToString().Contains(model.Searchfor) ||
                        r.IsNasfas.ToString().Contains(model.Searchfor) ||
                        r.IsDisable.ToString().Contains(model.Searchfor) ||
                        r.RaceString.ToString().Contains(model.Searchfor) ||
                        r.CurrentEmployer.ToString().Contains(model.Searchfor) ||
                        r.UserIdentity.GenderString.ToString().Contains(model.Searchfor)
                        );

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserIdentity.FirstName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.Surname);
                            break;
                        case "isemployed":
                            filteredQuery = filteredQuery.OrderBy(r => r.IsEmployed);
                            break;
                        case "qualifications":
                            filteredQuery = filteredQuery.OrderBy(r => r.Qualifications.Count);
                            break;
                        case "tertiary":
                            filteredQuery = filteredQuery.OrderBy(r => r.Tertiary.Name);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "isnasfas":
                            filteredQuery = filteredQuery.OrderBy(r => r.IsNasfas);
                            break;
                        case "isdisable":
                            filteredQuery = filteredQuery.OrderBy(r => r.IsDisable);
                            break;
                        case "race":
                            filteredQuery = filteredQuery.OrderBy(r => r.Race);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.Gender);
                            break;
                        case "currentemployer":
                            filteredQuery = filteredQuery.OrderBy(r => r.CurrentEmployer);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.Surname);
                            break;
                        case "isemployed":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.IsEmployed);
                            break;
                        case "qualifications":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Qualifications.Count);
                            break;
                        case "tertiary":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Tertiary.Name);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "isnasfas":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.IsNasfas);
                            break;
                        case "isdisable":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.IsDisable);
                            break;
                        case "race":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Race);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.Gender);
                            break;
                        case "currentemployer":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.CurrentEmployer);
                            break;
                    }
                    break;
            }

            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<GraduateGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public List<BL.Entities.Marks.Mark> GetMarks() {
            return GraduateProvider.GetMarks();
        }


        [HttpPost]
        public GridResultModel<GraduateGridModel> GraduateGrid(GridModel model)
        {
            int begin = SetupGridParams(model);
            TertiaryProvider tertiaryProvider = new TertiaryProvider(new DataContext(), CurrentUser);

            var filteredQuery = GraduateProvider.GetGraduates().Select(graduates => new GraduateGridModel()
            {
                Id = graduates.Id,
                UserIdentity = graduates.UserIdentity,
                UserIdentityId = graduates.UserIdentityId,
                JobId = graduates.JobId,
                TertiaryId = graduates.TertiaryId,
                IsEmployed = graduates.IsEmployed,
                Tertiary = tertiaryProvider.GetTertiary(graduates.TertiaryId),
                Qualifications = graduates.Qualifications,
                Job = graduates.Job,
                IsNasfas = graduates.IsNasfas,
                Race = graduates.Race,
                IsDisable = graduates.IsDisable,
                DisabilityDescription = graduates.DisabilityDescription,
                CurrentEmployer = graduates.CurrentEmployer,
            });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.UserIdentity.FirstName.ToString().Contains(model.Searchfor) ||
                        r.UserIdentity.Surname.ToString().Contains(model.Searchfor) ||
                        r.Id.ToString().Contains(model.Searchfor) ||
                        r.IsEmployed.ToString().Contains(model.Searchfor) ||
                       (r.Qualifications != null ? r.Qualifications.Select(a => a.Name).Contains(model.Searchfor) : r.Tertiary.Name.ToString().Contains(model.Searchfor)) ||
                        r.Tertiary.Name.ToString().Contains(model.Searchfor) ||
                        r.IsNasfas.ToString().Contains(model.Searchfor) ||
                        r.IsDisable.ToString().Contains(model.Searchfor) ||
                        r.RaceString.ToString().Contains(model.Searchfor) ||
                        r.CurrentEmployer.ToString().Contains(model.Searchfor) ||
                        r.UserIdentity.GenderString.ToString().Contains(model.Searchfor)
                        );

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.UserIdentity.FirstName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "firstname":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.Surname);
                            break;
                        case "isemployed":
                            filteredQuery = filteredQuery.OrderBy(r => r.IsEmployed);
                            break;
                        case "qualifications":
                            filteredQuery = filteredQuery.OrderBy(r => r.Qualifications.Count);
                            break;
                        case "tertiary":
                            filteredQuery = filteredQuery.OrderBy(r => r.Tertiary.Name);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "isnasfas":
                            filteredQuery = filteredQuery.OrderBy(r => r.IsNasfas);
                            break;
                        case "isdisable":
                            filteredQuery = filteredQuery.OrderBy(r => r.IsDisable);
                            break;
                        case "race":
                            filteredQuery = filteredQuery.OrderBy(r => r.Race);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderBy(r => r.UserIdentity.Gender);
                            break;
                        case "currentemployer":
                            filteredQuery = filteredQuery.OrderBy(r => r.CurrentEmployer);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "firstname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.FirstName);
                            break;
                        case "surname":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.Surname);
                            break;
                        case "isemployed":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.IsEmployed);
                            break;
                        case "qualifications":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Qualifications.Count);
                            break;
                        case "tertiary":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Tertiary.Name);
                            break;
                        case "id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "isnasfas":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.IsNasfas);
                            break;
                        case "isdisable":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.IsDisable);
                            break;
                        case "race":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Race);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.UserIdentity.Gender);
                            break;
                        case "currentemployer":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.CurrentEmployer);
                            break;
                    }
                    break;
            }

            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<GraduateGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public GraduateViewModel GraduateGet(long id)
        {
            try
            {
                GraduateViewModel model = new GraduateViewModel();

                if (id > 0)
                {
                    model = GraduateProvider.GetGraduates().Where(a => a.Id == id).Select(graduates => new GraduateViewModel()
                    {
                        Id = graduates.Id,
                        UserIdentity = graduates.UserIdentity,
                        UserIdentityId = graduates.UserIdentityId,
                        JobId = graduates.JobId,
                        TertiaryId = graduates.TertiaryId,
                        IsEmployed = graduates.IsEmployed,
                        Tertiary = graduates.Tertiary,
                        Qualifications = graduates.Qualifications,
                        Job = graduates.Job,
                        IsNasfas = graduates.IsNasfas,
                        Race = graduates.Race,
                        CurrectEmployer = graduates.CurrentEmployer,
                        IsDisable = graduates.IsDisable,
                        DisablityDescription = graduates.DisabilityDescription
                    }).Single();

                    TertiaryProvider t = new TertiaryProvider(new DataContext(), CurrentUser);
                    model.Tertiary = t.GetTertiary(model.TertiaryId);

                    //TODO: Get qualifications by graduateId
                    //QualificationProvider q = new QualificationProvider(new DataContext(), CurrentUser);
                    //model.Qualifications = q.

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}