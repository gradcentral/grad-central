﻿using System.Threading;
using System.Web.Mvc;
using GradCentral.Controllers.Filters;

namespace GradCentral.Controllers
{
    [NoCache]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {

            ViewBag.Message = "Grad-Central";

            ViewBag.Authenticated = Request.IsAuthenticated ? "true" : "false";
            ViewBag.UserName = string.Empty;

            if (Request.IsAuthenticated)
                ViewBag.UserName = Thread.CurrentPrincipal.Identity.Name;

            return View();
        }
    }
}