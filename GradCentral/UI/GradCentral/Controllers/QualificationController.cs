﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Faculty;
using GradCentral.BL.Provider.Qualification;
using GradCentral.Models.QualificationData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class QualificationController : TCRControllerBase
    {

        #region Constructor

        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private BL.Provider.Qualification.IQualificationProvider QualificationProvider { get; set; }

        public QualificationController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            QualificationProvider = new QualificationProvider(Context, CurrentUser);
        }


        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        #endregion


        [HttpPost]
        public QualificationViewModel SaveQualification(QualificationViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {

                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Error details"));
                }

                Qualification newQualification = new Qualification()
                {
                    Id = model.Id,
                    Date = DateTime.Now,
                    Name = model.Name,
                    FacultyId = model.FacultyId,
                    QualificationType = model.QualificationType,
                   
                   
                    
                };

                Qualification SavedQualification = QualificationProvider.saveQualification(newQualification);


                QualificationViewModel viewModelQualification = new QualificationViewModel()
                {
                    Id = SavedQualification.Id,
                    Name = SavedQualification.Name,
                    Date = SavedQualification.Date,
                    QualificationType = SavedQualification.QualificationType,
                    FacultyId = SavedQualification.FacultyId,
                };

                return viewModelQualification;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }

        [HttpGet]
        public List<QualificationViewModel> QualificationList()
        {
            List<Qualification> QualificationList = QualificationProvider.GetQualifications().OrderBy(b => b.Name).ToList();

            var model = QualificationList.Select(Qualifications => new QualificationViewModel()
            {
                Id = Qualifications.Id,
                Name = Qualifications.Name,
                Date = Qualifications.Date,
                QualificationType = Qualifications.QualificationType,
                FacultyId = Qualifications.FacultyId,

                //Qualifications = Qualificationes.Qualifications,
                Faculty = new FacultyViewModel()
                {
                    Id = Qualifications.Faculty.Id,
                    Name = Qualifications.Faculty.Name,
                    Campus = Qualifications.Faculty.Campus,
                    CampusId = Qualifications.Faculty.CampusId
                }

            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<QualificationGridModel> QualificationGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = QualificationProvider.GetQualifications()
                .Select(Qualification => new QualificationGridModel()
                {

                    Id = Qualification.Id,
                    Name = Qualification.Name,
                    Date = Qualification.Date,
                    FacultyId = Qualification.FacultyId,
                    QualificationType = Qualification.QualificationType,

                    //Qualifications = Qualification.Qualifications,
                    Faculty = new FacultyViewModel()
                    {
                        Id = Qualification.Faculty.Id,
                        Name = Qualification.Faculty.Name
                    }

                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.QualificationType.ToString().Contains(model.Searchfor) || r.Faculty.ToString().Contains(model.Searchfor)||  r.Date.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "Id":
                            filteredQuery = filteredQuery.OrderBy(r => r.Id);
                            break;
                        case "Name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "QualificationType":
                            filteredQuery = filteredQuery.OrderBy(r => r.QualificationType);
                            break;
                        case "Faculty":
                            filteredQuery = filteredQuery.OrderBy(r => r.Faculty.Name);
                            break;
                        case "Date":
                            filteredQuery = filteredQuery.OrderBy(r => r.Date);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "Id":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Id);
                            break;
                        case "Name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "QualificationType":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.QualificationType);
                            break;
                        case "Faculty":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Faculty.Name);
                            break;
                        case "Date":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Date);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<QualificationGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }


        [HttpGet]
        public QualificationViewModel QualificationGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                QualificationViewModel QualificationViewModel = new QualificationViewModel();

                if (id > 0)
                {
                    QualificationViewModel = QualificationProvider.GetQualifications().Where(a => a.Id == id).Select(Qualification => new QualificationViewModel()
                    {
                        Id = Qualification.Id,
                        Name = Qualification.Name,
                        Date = Qualification.Date,
                        FacultyId = Qualification.FacultyId,
                        QualificationType = Qualification.QualificationType,
                        

                        //Qualifications = Qualification.Qualifications,
                        Faculty = new FacultyViewModel()
                        {
                            Id = Qualification.Faculty.Id,
                            Name = Qualification.Faculty.Name,

                        }

                    }).Single();

                }
                return QualificationViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}