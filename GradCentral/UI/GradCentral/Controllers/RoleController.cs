﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Security;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.SecurityData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class RoleController : TCRControllerBase
    {

        #region Constructor

        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.Security.ISecurityProvider SecurityProvider { get; set; }

        public RoleController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            SecurityProvider = new SecurityProvider(Context, CurrentUser);
        }


        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        #endregion


        [HttpPost]
        public RoleViewModel SaveRole(RoleViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {

                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Error details"));

                }
                var role1 = SecurityProvider.SaveRole(model.Id, model.RoleName, model.Description, model.StatusType,
                    model.Permissions.Select(a => a.Privilege).ToList());

                var role = new RoleViewModel()
                {
                    Id = role1.Id,
                    RoleName = role1.RoleName,
                    Description = role1.Description,
                    Permissions = role1.Privileges.Select(a => new PermissionViewModel()
                    {
                        Privilege = a.Security
                    }).ToList()
                };

                return role;


            }
            catch (SecurityException e)
            {

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));


            }

        }

        [HttpGet]
        public List<RoleViewModel> RoleList()
        {
            var items = SecurityProvider.GetRoles().OrderBy(b => b.RoleName).ToList();

            var model = items.Select(a => new RoleViewModel()
            {
                Id = a.Id,
                RoleName = a.RoleName,
                Description = a.Description,
                StatusType = a.Status
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<RoleGridModel> RoleGrid(GridModel model)
        {
            int begin = SetupGridParams(model);



            var filteredQuery = SecurityProvider.GetRoles()
                .Select(a => new RoleGridModel()
                {

                    Id = a.Id,
                    RoleName = a.RoleName,
                    Description = a.Description,
                    StatusTypes = a.Status
                   
                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.RoleName.Contains(model.Searchfor) || r.Description.Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.RoleName); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "rolename":
                            filteredQuery = filteredQuery.OrderBy(r => r.RoleName);
                            break;
                        case "description":
                            filteredQuery = filteredQuery.OrderBy(r => r.Description);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderBy(r => r.StatusTypes);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "rolename":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.RoleName);
                            break;
                        case "description":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Description);
                            break;
                        case "status":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.StatusTypes);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<RoleGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }


        [HttpGet]
        public RoleViewModel RoleGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                RoleViewModel model = new RoleViewModel();

                if (id > 0)
                {
                    model = SecurityProvider.GetRoles().Where(a => a.Id == id).Select(a => new RoleViewModel()
                    {
                        Id = a.Id,
                        RoleName = a.RoleName,
                        Description = a.Description,
                        StatusType = a.Status,
                        

                    }).Single();
                }

                model.Permissions = new List<PermissionViewModel>();

                foreach (PrivilegeType privType in Enum.GetValues(typeof(PrivilegeType)))
                {
                    model.Permissions.Add(new PermissionViewModel()
                    {
                        Privilege = privType,
                        Selected = false
                    });
                }


                if (id > 0)
                {
                    var privs = SecurityProvider.GetRoles().Where(a => a.Id == id).Single().Privileges;
                        //Gets all the privlege assigned to the role found using the ID
                    foreach (var p in privs)
                    {
                        var priv = model.Permissions.Where(a => a.Privilege == p.Security).SingleOrDefault();
                            //Gets the single privlege assigned to the role
                        if (priv != null)
                        {
                            priv.Selected = true;
                        }
                        else
                        {
                            model.Permissions.Add(new PermissionViewModel()
                            {
                                Privilege = p.Security,
                                Selected = true
                            });
                        }
                    }
                }
                return model;
            }
            catch (SecurityException e)
            {

                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}