﻿using GradCentral.BL.Context;
using GradCentral.BL.Provider.Job;
using GradCentral.Controllers;
using GradCentral.Controllers.Filters;
using GradCentralUI.Models.Job;
using GradCentral.BL.Entities.JobData;
using System;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Collections.Generic;
using System.Linq;
using GradCentralUI.Models.SecurityData;
using GradCentral.Models.Company;
using GradCentral.Models.JobRole;
using GradCentral.Models;

namespace GradCentralUI.Controllers
{
    [Authorize]
    [NoCache]
    public class JobController : TCRControllerBase
    {
        #region Constructor
        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private IJobProvider JobProvider { get; set; }

        public JobController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            JobProvider = new JobProvider(Context, CurrentUser);
        }

        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }
        #endregion

        [HttpPost]
        public JobViewModel SaveJob(JobViewModel jobModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Error details"));
                    //var errors = ModelState.Values.SelectMany(v => v.Errors);
                }
                Job job = new Job()
                {
                    Id = jobModel.Id,
                    Title = jobModel.Title,
                    Disability = jobModel.Disability,
                    GPA = jobModel.GPA,
                    Gender = jobModel.Gender,
                    Race = jobModel.Race,
                    Position = jobModel.Position,
                    RecruiterId = jobModel.RecruiterId,
                    JobRoleId = jobModel.JobRoleId,
                    QualificationId = jobModel.QualificationId
                };

                Job Job1 = JobProvider.SaveJob(job);

                JobViewModel Job = new JobViewModel()
                {
                    Id = Job1.Id,
                    Title = Job1.Title,
                    Disability = Job1.Disability,
                    GPA = Job1.GPA,
                    Gender = Job1.Gender,
                    Race = Job1.Race,
                    Position = Job1.Position,
                    RecruiterId = Job1.RecruiterId,
                    JobRoleId = Job1.JobRoleId,
                    QualificationId = Job1.QualificationId
                };

                return Job;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }
        }

        [HttpGet]
        public List<JobViewModel> JobList()
        {
            List<Job> jobList = JobProvider.GetJobs().OrderBy(b => b.Title).ToList();

            var model = jobList.Select(jobs => new JobViewModel()
            {
                Id = jobs.Id,
                Title = jobs.Title,
                Disability = jobs.Disability,
                GPA = jobs.GPA,
                Gender = jobs.Gender,
                Race = jobs.Race,
                Position = jobs.Position,
                RecruiterId = jobs.RecruiterId,
                Recruiter = new RecruiterViewModel()
                {
                    Id = jobs.Recruiter.Id,

                    CompanyId = jobs.Recruiter.Company.Id,
                    Company = new CompanyViewModel()
                    {
                        Id = jobs.Recruiter.Company.Id,
                        Name = jobs.Recruiter.Company.Name
                    }
                },

                JobRoleId = jobs.JobRoleId,
                JobRole = new JobRoleViewModel()
                {
                    Id = jobs.JobRole.Id,
                    Name = jobs.JobRole.Name
                },

                QualificationId = jobs.QualificationId,
                Qualification = new GradCentral.Models.QualificationData.QualificationViewModel()
                {
                    Id = jobs.Qualification.Id,
                    Name = jobs.Qualification.Name
                }

                
            }).ToList();

            return model;
        }

        [HttpPost]
        public GridResultModel<JobGridModel> JobGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            var filteredQuery = JobProvider.GetJobs().Select(job => new JobGridModel()
                {
                    Id = job.Id,
                    Title = job.Title,
                    Disability = job.Disability,
                    GPA = job.GPA,
                    GenderType = job.Gender,
                    RaceType = job.Race,
                    PositionType = job.Position,
                    RecruiterId = job.RecruiterId,
                    Recruiter = new RecruiterViewModel()
                    {
                        Id = job.Recruiter.Id,

                        CompanyId = job.Recruiter.CompanyId,
                        Company = new CompanyViewModel()
                        {
                            Id = job.Recruiter.Company.Id,
                            Name = job.Recruiter.Company.Name
                        }
                    },

                    JobRoleId = job.JobRoleId,
                    JobRole = new JobRoleViewModel()
                    {
                        Id = job.JobRole.Id,
                        Name = job.JobRole.Name
                    },

                    QualificationId = job.QualificationId,
                    Qualification = new GradCentral.Models.QualificationData.QualificationViewModel()
                    {
                        Id = job.Qualification.Id,
                        Name = job.Qualification.Name
                    }
                    
                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Title.Contains(model.Searchfor) || r.Race.ToString().Contains(model.Searchfor) || r.Qualification.ToString().Contains(model.Searchfor) ||
                        r.Gender.ToString().Contains(model.Searchfor) || r.Disability.ToString().Contains(model.Searchfor) || r.Position.ToString().Contains(model.Searchfor) ||
                        r.Recruiter.Company.Name.ToString().Contains(model.Searchfor) || r.GPA.ToString().Contains(model.Searchfor) ||
                        r.JobRole.Name.ToString().Contains(model.Searchfor));

            }

            //Get Reord count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Title); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Title);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderBy(r => r.Gender);
                            break;
                        case "race":
                            filteredQuery = filteredQuery.OrderBy(r => r.Race);
                            break;
                        case "gpa":
                            filteredQuery = filteredQuery.OrderBy(r => r.GPA);
                            break;
                        case "position":
                            filteredQuery = filteredQuery.OrderBy(r => r.Position);
                            break;
                        case "recruiter":
                            filteredQuery = filteredQuery.OrderBy(r => r.Recruiter.Company.Name);
                            break;
                        case "jobRole":
                            filteredQuery = filteredQuery.OrderBy(r => r.JobRole.Name);
                            break;
                        case "disability":
                            filteredQuery = filteredQuery.OrderBy(r => r.Disability);
                            break;
                        case "qualification":
                            filteredQuery = filteredQuery.OrderBy(r => r.Qualification);
                            break;
                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Title);
                            break;
                        case "gender":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Gender);
                            break;
                        case "race":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Race);
                            break;
                        case "gpa":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.GPA);
                            break;
                        case "position":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Position);
                            break;
                        case "recruiter":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Recruiter.Company.Name);
                            break;
                        case "jobRole":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.JobRole.Name);
                            break;
                        case "disability":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Disability);
                            break;
                        case "qualification":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Qualification);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<JobGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }

        [HttpGet]
        public JobViewModel JobGet(long id)
        {
            if (id == null)
                id = 0;

            try
            {
                JobViewModel model = new JobViewModel();

                if (id > 0)
                {
                    model = JobProvider.GetJobs().Where(a => a.Id == id).Select(a => new JobViewModel()
                    {
                        Id = a.Id,
                        Title = a.Title,
                        Disability = a.Disability,
                        GPA = a.GPA,
                        Race = a.Race,
                        Gender = a.Gender,
                        Position = a.Position,
                        RecruiterId = a.RecruiterId,
                        Recruiter = new RecruiterViewModel()
                        {
                            Id = a.Recruiter.Id,

                            CompanyId = a.Recruiter.CompanyId,
                            Company = new CompanyViewModel()
                            {
                                Id = a.Recruiter.Company.Id,
                                Name = a.Recruiter.Company.Name
                            }
                        },

                        JobRoleId = a.JobRoleId,
                        JobRole = new JobRoleViewModel()
                        {
                            Id = a.JobRole.Id,
                            Name = a.JobRole.Name
                        },

                        QualificationId = a.QualificationId,
                        Qualification = new GradCentral.Models.QualificationData.QualificationViewModel()
                        {
                            Id = a.Qualification.Id,
                            Name = a.Qualification.Name
                        }
                    }).Single();

                }
                return model;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}