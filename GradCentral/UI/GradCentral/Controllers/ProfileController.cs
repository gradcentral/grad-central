﻿using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Security;
using GradCentral.Controllers.Filters;
using GradCentral.Models.SecurityData;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class ProfileController : TCRControllerBase
    {
        #region Ctor

        public GradCentral.BL.Provider.Security.ISecurityProvider SecurityProvider { get; set; }
        public DataContext DataContext { get; set; }
        private bool _MustDisposeContext = true;

        public ProfileController()
        {
            DataContext = new DataContext();
            SecurityProvider = new SecurityProvider(DataContext, CurrentUser);
        }

        public ProfileController(ISecurityProvider securityProvider)
        {
            _MustDisposeContext = false;
            SecurityProvider = securityProvider;

        }


        protected override void Dispose(bool disposing)
        {
            if (_MustDisposeContext)
                DataContext.Dispose();
            base.Dispose(disposing);
        }


        #endregion

        [HttpPost]
        public ProfileViewModel EditProfile(UserIdentity model)
        {
            var profile = SecurityProvider.EditProfile(model.FirstName, model.Surname, model.Email, model.Title);
            var model1 = new ProfileViewModel()
            {
                FirstName = profile.FirstName,
                Surname = profile.Surname,
                Email = profile.Email,
                Title = profile.Title,
                UserName = profile.UserName

            };

            return model1;

        }

        [HttpGet]
        public ProfileViewModel GetMyProfile()
        {
            var profile = SecurityProvider.GetMyProfile();
            var model1 = new ProfileViewModel()
            {
                FirstName = profile.FirstName,
                Surname = profile.Surname,
                Email = profile.Email,
                Title = profile.Title,
                UserName = profile.UserName

            };

            return model1;

        }
    }
}
