﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.TertiaryData;
using GradCentral.BL.Provider.Graduate;
using GradCentral.BL.Provider.Qualification;
using GradCentral.BL.Provider.Tertiary;
using GradCentral.Controllers.Filters;
using GradCentral.Models;
using GradCentral.Models.Tertiary;

namespace GradCentral.Controllers
{
    [Authorize]
    [NoCache]
    public class TertiaryController : TCRControllerBase
    {

        #region Constructor

        private DataContext Context { get; set; }
        private bool _DisposeContext = false;

        private GradCentral.BL.Provider.Tertiary.ITertiaryProvider TertiaryProvider { get; set; }

        public TertiaryController()
        {
            Context = new DataContext();
            _DisposeContext = true;
            TertiaryProvider = new TertiaryProvider(Context, CurrentUser);
        }


        protected override void Dispose(bool disposing)
        {
            if (_DisposeContext)
                Context.Dispose();

            base.Dispose(disposing);

        }

        #endregion


        [HttpPost]
        public TertiaryViewModel SaveTertiary(TertiaryViewModel model)
        {
            try
            {

                if (!ModelState.IsValid)
                {

                    throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest,
                        "Error details"));

                }
                Tertiary t = new Tertiary()
                {
                    Id = model.Id,
                    TertiaryType = model.TertiaryType,
                    ProvinceType = model.Province,
                    Name = model.Name,
                    Graduates = model.Graduates
                };
                var Tertiary1 = TertiaryProvider.SaveTertiary(t);

                var Tertiary = new TertiaryViewModel()
                {
                    Name = Tertiary1.Name,
                    TertiaryType = Tertiary1.TertiaryType,
                    Province = model.Province,
                    Graduates = model.Graduates
                };

                return Tertiary;


            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }

        [HttpGet]
        public List<TertiaryViewModel> TertiaryList()
        {
            List<Tertiary> tertiaryList = TertiaryProvider.GetTertiarys().OrderBy(b => b.Name).ToList();

            QualificationProvider qp = new QualificationProvider(new DataContext(), CurrentUser);
            GraduateProvider gp = new GraduateProvider(new DataContext(), CurrentUser);

            var tertiaryViewModel = tertiaryList.Select(a => new TertiaryViewModel()
            {
                Id = a.Id,
                Name = a.Name,
                TertiaryType = a.TertiaryType,
                Province = a.ProvinceType,
                //Graduates = gp.GetGraduates().Where(g => g.Tertiary.Id == a.Id).ToList(),
                Graduates = a.Graduates,
                Qualifications = qp.GetQualifications().Where(q => q.Faculty.Campus.TertiaryId == a.Id).ToList()

            }).ToList();

            return tertiaryViewModel;
        }

        [HttpPost]
        public GridResultModel<TertiaryGridModel> TertiaryGrid(GridModel model)
        {
            int begin = SetupGridParams(model);

            QualificationProvider qp = new QualificationProvider(new DataContext(), CurrentUser);
            GraduateProvider gp = new GraduateProvider(new DataContext(), CurrentUser);

            var filteredQuery = TertiaryProvider.GetTertiarys()
                .Select(a => new TertiaryGridModel()
                {

                    Id = a.Id,
                    Name = a.Name,
                    TertiaryType = a.TertiaryType,
                    Provinces = a.ProvinceType,
                    Graduates = a.Graduates,
                    Qualifications = qp.GetQualifications().Where(q => q.Faculty.Campus.TertiaryId == a.Id).ToList()

                });

            if (model.Searchfor != "null")
            {
                filteredQuery =
                    filteredQuery.Where(
                        r => r.Name.Contains(model.Searchfor) || r.Tertiary.ToString().Contains(model.Searchfor) || r.Province.ToString().Contains(model.Searchfor));

            }

            //Get Record count
            var totalNumberOfRecords = filteredQuery.Count();

            if (String.IsNullOrWhiteSpace(model.SortKey))
                filteredQuery = filteredQuery.OrderBy(a => a.Name); //default sort order

            if (!String.IsNullOrWhiteSpace(model.SortKey))
                model.SortKey = model.SortKey.ToLower();

            switch (model.SortOrder)
            {
                case "ASC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderBy(r => r.Name);
                            break;
                        case "tertiarytype":
                            filteredQuery = filteredQuery.OrderBy(r => r.Tertiary);
                            break;
                        case "province":
                            filteredQuery = filteredQuery.OrderBy(r => r.Province);
                            break;

                    }
                    break;
                case "DESC":
                    switch (model.SortKey)
                    {
                        case "name":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Name);
                            break;
                        case "tertiarytype":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Tertiary);
                            break;
                        case "province":
                            filteredQuery = filteredQuery.OrderByDescending(r => r.Province);
                            break;
                    }
                    break;
            }



            filteredQuery = filteredQuery.Skip(begin).Take(model.RecordsPerPage.Value);

            return new GridResultModel<TertiaryGridModel>(filteredQuery.ToList(), totalNumberOfRecords);
        }


        [HttpGet]
        public TertiaryViewModel TertiaryGet(long? id)
        {
            if (id == null)
                id = 0;

            try
            {
                TertiaryViewModel tertiaryViewModel = new TertiaryViewModel();

                if (id > 0)
                {
                    tertiaryViewModel = TertiaryProvider.GetTertiarys().Where(a => a.Id == id).Select(a => new TertiaryViewModel()
                    {
                        Id = a.Id,
                        Name = a.Name,
                        TertiaryType = a.TertiaryType,
                        Province = a.ProvinceType,
                        Graduates = a.Graduates,

                    }).Single();

                }
                return tertiaryViewModel;
            }
            catch (Exception e)
            {
                throw new HttpResponseException(Request.CreateErrorResponse(HttpStatusCode.BadRequest, e.Message));
            }

        }
    }
}