﻿var AngularApp = angular.module('AngularApp',
    ['ngRoute', 'ui.bootstrap', 'validation.match', 'ui.bootstrap.showErrors',
        'angular-loading-bar', 'ngAnimate']);

AngularApp.config(
    ['$routeProvider', '$httpProvider', '$locationProvider',
        function ($routeProvider, $httpProvider, $locationProvider) {
            $locationProvider.hashPrefix('');
            $routeProvider
                .when("/", {
                    templateUrl: "app/Views/Account/Login.html",
                    controller: "LoginController"
                })
                .when("/home", {
                    templateUrl: "app/Views/Home/Home.html",
                    controller: "HomeController"
                })
                .when('/login', {
                    templateUrl: 'app/Views/Account/Login.html',
                    controller: 'LoginController'
                })
                .when('/register', {
                    templateUrl: 'app/Views/Account/Register.html',
                    controller: 'RegisterController'
                })
                .when('/userMaintenance', {
                    templateUrl: 'app/Views/Security/UserMaintenance.html',
                    controller: 'UserController'
                })
                .when('/roleMaintenance', {
                    templateUrl: 'app/Views/Security/RoleMaintenance.html',
                    controller: 'RoleController'
                })
                .when('/roleEdit/:id', {
                    templateUrl: 'app/Views/Security/SaveRole.html',
                    controller: 'RoleController'
                })
                .when('/userEdit/:id', {
                    templateUrl: 'app/Views/Security/SaveUserRole.html',
                    controller: 'UserController'
                })
                .when('/dashboard', {
                    templateUrl: 'app/Views/Dashboard/Dashboard.html',
                    controller: 'DashboardController'
                })
                .when('/recruiterDashboard', {
                    templateUrl: 'app/Views/Dashboard/RecruiterDashboard.html',
                    controller: 'RecruiterDashboardController'
                })
                .when('/graduateDashboard', {
                    templateUrl: 'app/Views/Dashboard/GraduateDashboard.html',
                    controller: 'GraduateDashboardController'
                })
                .when('/tertiaryMaintenance', {
                    templateUrl: 'app/Views/Tertiary/TertiaryMaintenance.html',
                    controller: 'TertiaryController'
                })
                .when('/tertiaryEdit/:id', {
                    templateUrl: 'app/Views/Tertiary/SaveTertiary.html',
                    controller: 'TertiaryController',
                })
                .when('/campusMaintenance', {
                    templateUrl: 'app/Views/Tertiary/Campus/CampusMaintenance.html',
                    controller: 'CampusController'
                })
                .when('/campusEdit/:id', {
                    templateUrl: 'app/Views/Tertiary/Campus/SaveCampus.html',
                    controller: 'CampusController'
                })
                //.when('/myProfile', {
                //})
                .when('/myProfile', {
                    templateUrl: 'app/Views/Account/Profile.html',
                    controller: 'ProfileController'
                })
                .when('/qualificationMaintenance', {
                    templateUrl: 'app/Views/Qualification/QualificationMaintenance.html',
                    controller: 'QualificationController'
                })

                .when('/qualificationEdit/:id', {
                    templateUrl: 'app/Views/Qualification/SaveQualification.html',
                    controller: 'QualificationController'
                }).

                when('/moduleMaintenance', {
                    templateUrl: 'app/Views/Module/ModuleMaintenance.html',
                    controller: 'ModuleController'
                })
                .when('/moduleEdit/:id', {
                    templateUrl: 'app/Views/Module/SaveModule.html',
                    controller: 'ModuleController'
                })
                .when('/FacultyMaintenance', {
                    templateUrl: 'app/Views/Tertiary/Faculty/FacultyMaintenance.html',
                    controller: 'FacultyController'
                })
                .when('/FacultyEdit/:id', {
                    templateUrl: 'app/Views/Tertiary/Faculty/SaveFaculty.html',
                    controller: 'FacultyController'
                })
                .when('/CompanyMaintenance', {
                    templateUrl: 'app/Views/Company/CompanyMaintenance.html',
                    controller: 'CompanyController'
                })
                .when('/CompanyEdit/:id', {
                    templateUrl: 'app/Views/Company/SaveCompany.html',
                    controller: 'CompanyController'
                })
                .when('/JobRoleMaintenance', {
                    templateUrl: 'app/Views/JobRole/JobRoleMaintenance.html',
                    controller: 'JobRoleController'
                })
                .when('/JobRoleEdit/:id', {
                    templateUrl: 'app/Views/JobRole/SaveJobRole.html',
                    controller: 'JobRoleController'
                })

                .when('/RecruiterMaintenance', {
                    templateUrl: 'app/Views/Recruiter/RecruiterMaintenance.html',
                    controller: 'RecruiterController'
                })
                .when('/RecruiterEdit/:id', {
                    templateUrl: 'app/Views/Recruiter/SaveRecruiter.html',
                    controller: 'RecruiterController'
                })
                   .when('/GraduateView/:id', {
                       templateUrl: 'app/Views/Graduate/SaveGraduate.html',
                       controller: 'GraduateController'
                   })
                .when('/GraduateMaintenance', {
                    templateUrl: 'app/Views/Graduate/GraduateMaintenance.html',
                    controller: 'GraduateController'
                })

                .when('/JobMaintenance', {
                    templateUrl: 'app/Views/Job/JobMaintenance.html',
                    controller: 'JobController'
                })
                .when('/JobCandidates/:id', {
                    templateUrl: 'app/Views/PossibleCandidates/JobCandidates.html',
                    controller: 'PossibleCandidateController'
                })
                  .when('/PossibleCandidate', {
                      templateUrl: 'app/Views/PossibleCandidates/PossibleCandidate.html',
                      controller: 'PossibleCandidateController'
                  })

                .when('/JobEdit/:id', {
                    templateUrl: 'app/Views/Job/SaveJob.html',
                    controller: 'JobController'
                })
                .when('/DepartmentMaintenance', {
                    templateUrl: 'app/Views/Department/DepartmentMaintenance.html',
                    controller: 'DepartmentController'
                })
                .when('/DepartmentEdit/:id', {
                    templateUrl: 'app/Views/Department/SaveDepartment.html',
                    controller: 'DepartmentController'
                })
                .otherwise({
                    redirectTo: '/'
                });

            //$locationProvider.html5Mode(true);

            $httpProvider.interceptors.push('AuthHttpResponseInterceptor');

            //initialize get if not there
            if (!$httpProvider.defaults.headers.get) {
                $httpProvider.defaults.headers.get = {};
            }

            $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
            $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
            $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';

        }]);