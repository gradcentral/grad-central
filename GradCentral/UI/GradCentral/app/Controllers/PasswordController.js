﻿AngularApp.controller('PasswordController',
    ['$scope', '$modalInstance', '$timeout', '$location', 'AccountService',
    function ($scope, $modalInstance, $timeout, $location, AccountService) {
        $scope.viewModel = {};

        $scope.successMessage = "Password Changed";

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;

        };

        $scope.ok = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.passwordForm.$invalid)
            {
                $scope.hasError = true;
                $scope.errorMessage = 'Please enter passwords';
                return;
            }

            var result = AccountService.ChangePassword($scope.viewModel);

            result.then(function (result) {

                $scope.handleSuccess = function () { };

                $timeout(function () {
                    $modalInstance.close($location.path('/myProfile'));
                }, 1000);
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');

        };

    }
    ]);