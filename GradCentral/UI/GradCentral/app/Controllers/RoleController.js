﻿AngularApp.controller('RoleController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'RoleService', 'EnumsService',
    function($scope, $location, $window, $timeout, $routeParams, RoleService, EnumsService) {

        $scope.viewModel = {};

        $scope.viewModel.id = $routeParams.id;

        if ($scope.viewModel.id > 0) {
            $scope.updateStatus = function() {};
        }


        RoleService.RoleGet($scope.viewModel.id).then(
            function(result) {
                $scope.viewModel = result.data;
            },
            function(error) {
                handleError(error);
            });


        $scope.statusTypes = EnumsService.statusTypes();

        $scope.options = [
            { value: null, lable: 'Select' },
            { value: 0, lable: $scope.statusTypes[0].value },
            { value: 1, lable: $scope.statusTypes[2].value },
            { value: 2, lable: $scope.statusTypes[1].value }
        ];

        $scope.successMessage = "Saved Successfully";

         $scope.print = function (layer) {
            var generator = window.open("Role Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewRoleForm.$invalid)
                return;


            var result = RoleService.RoleSave($scope.viewModel);

            result.then(function (result) {


                $scope.handleSuccess = function () { };

                $timeout(function () { $window.history.back(); }, 2000);

            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        };

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        };

        $scope.newRole = function () {
            $location.path("/roleEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };

        $scope.sortKeyOrder = {
            key: 'roleName',
            order: 'ASC'
        };

        $scope.data = RoleService.roles;

        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order === 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        };


        var loadGrid = function () {
            var searchFor = '';

            RoleService.RoleGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.roleGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });


        };


        $scope.pageChanged = function () {
            loadGrid();
        };




        loadGrid();
    }
]);