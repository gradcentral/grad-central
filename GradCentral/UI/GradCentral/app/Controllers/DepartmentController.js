﻿AngularApp.controller('DepartmentController',
    [
        '$scope', '$location', '$window', '$timeout', '$routeParams', 'DepartmentService', 'EnumsService', 'FacultyService',
        function ($scope, $location, $window, $timeout, $routeParams, DepartmentService, EnumsService, FacultyService) {

            $scope.viewModel = {
                province: undefined
            };
            $scope.provinceList = [];
            $scope.facultyList = [];

            $scope.viewModel.id = $routeParams.id;

            var getFaculties = function () {
                FacultyService.FacultyList().then(
                    function (result) {
                        $scope.facultyList = result.data;
                        $scope.facultyList.unshift({ 'id': undefined, 'name': 'Select faculty' });
                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });
            };

            $scope.print = function (layer) {
                var generator = window.open("Department Report");
                var layerText = document.getElementById(layer);
                generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

                generator.document.close();
            }

            if ($scope.viewModel.id > 0) {
                getFaculties();
                $scope.updateStatus = function () { };

                DepartmentService.DepartmentGet($scope.viewModel.id).then(
                    function (result) {
                        $scope.viewModel = result.data;
                    },
                    function (error) {
                        handleError(error);
                    });
            }
            else if ($scope.viewModel.id === 0) {
                
                getFaculties();
            }
            else {
                getFaculties();
                $scope.sortKeyOrder = {
                    key: 'Name',
                    order: 'ASC'
                };

                $scope.totalItems = 0;
                $scope.currentPage = 1;
                $scope.maxSize = 5;
                $scope.recordsPerPage = 20;
                $scope.numberOfPageButtons = 5;

                $scope.sort = function (col) {
                    if ($scope.sortKeyOrder.key === col) {
                        if ($scope.sortKeyOrder.order === 'ASC')
                            $scope.sortKeyOrder.order = 'DESC';
                        else
                            $scope.sortKeyOrder.order = 'ASC';
                    } else {
                        $scope.sortKeyOrder.key = col;
                        $scope.sortKeyOrder.order = 'ASC';
                    }
                    loadGrid();
                };

                $scope.searchFor = '';
                $scope.search = function () {
                    loadGrid();
                }


                var loadGrid = function () {
                    var searchFor = '';

                    DepartmentService.DepartmentGrid($scope.currentPage, $scope.recordsPerPage,
                        $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                        function (result) {
                            $scope.DepartmentGrid = result.data.results;
                            $scope.totalItems = result.data.recordCount;
                        },
                        function (error) {
                            alert("an error occured: unable to get data");
                        });


                };
                $scope.pageChanged = function () {
                    loadGrid();
                };
                loadGrid();
            };


            $scope.successMessage = "Saved Successfully";

            $scope.submitForm = function () {
                $scope.$broadcast('show-errors-check-validity');

                if ($scope.NewDepartmentForm.$invalid)
                    return;


                var result = DepartmentService.DepartmentSave($scope.viewModel);

                result.then(function (result) {

                    $scope.handleSuccess = function () { };

                    $timeout(function () { $window.history.back(); }, 2000);

                },
                    function (error) {
                        $scope.hasError = true;
                        $scope.errorMessage = error.data.message;
                    });
            };

            $scope.resetForm = function () {
                $scope.$broadcast('show-errors-reset');
            }

            $scope.cancelForm = function () {
                $scope.$broadcast('show-errors-reset');
                $window.history.back();
            }

            $scope.newDepartment = function () {
                $location.path("/DepartmentEdit/0");
            };

            var handleError = function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            };
        }
    ]);