﻿AngularApp.controller('GraduateDashboardController',
    ['$scope', 'Security', '$rootScope', 'EnumsService', 'UserService', 'CompanyService', 'TertiaryService', 'NotificationService', 'JobService',
        function ($scope, Security, $rootScope, EnumsService, UserService, CompanyService, TertiaryService, NotificationService, JobService) {

            $scope.users = [];
            $scope.companies = [];
            $scope.tertiaries = [];
            $scope.companies = [];
            $scope.notifications = [];
            $scope.jobs = [];
            $scope.UserNotification = [];

            $scope.newConfirm = 'True';
            $scope.IsRead = 'True';

            $scope.myvalue = false;

            $scope.showAlert = function () {
                $scope.myvalue = true;
            };



            JobService.JobList().then(
                function (result) {
                    $scope.jobs = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                    Security.logout();
                }
            );

            NotificationService.GetUserNotifications().then(
                function (result) {
                    $scope.notifications = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                    Security.logout();
                }
            );

            UserService.UserList().then(
                function (result) {
                    $scope.users = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                    Security.logout();
                }
            );

            CompanyService.CompanyList().then(
                function (result) {
                    $scope.companies = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                    Security.logout();
                }
            );

            TertiaryService.TertiaryList().then(
                function (result) {
                    $scope.tertiaries = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                    Security.logout();
                }
            );



        }]);


