AngularApp.controller('GraduateController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'GraduateService', 'EnumsService', 'TertiaryService',
    function ($scope, $location, $window, $timeout, $routeParams, GraduateService, EnumsService, TertiaryService) {
        $scope.viewModel = {
            province: undefined
        };
        $scope.provinceList = [];
        $scope.viewModel.id = $routeParams.id;

        $scope.dateTime = Date.now();

        var loadMarks = function () {
            GraduateService.MarksList().then(function (result) {
                var marks = result.data;
                for (var i = 0; i < $scope.GraduateGrid.length; i++) {
                    for (var j = 0; j < $scope.GraduateGrid[i].qualifications.length; j++) {
                        $scope.GraduateGrid[i].qualifications[j].marks = marks.filter(function (el) {
                            return el.qualificationId === $scope.GraduateGrid[i].qualifications[j].id && el.graduateId === $scope.GraduateGrid[i].id;
                        });
                    }
                }
            },
            function (error) {

            });
        }

        var loadMark = function () {
            GraduateService.MarksList().then(function (result) {
                var marks = result.data;
                for (var j = 0; j < $scope.viewModel.qualifications.length; j++) {
                    $scope.viewModel.qualifications[j].marks = marks.filter(function (el) {
                        return el.qualificationId === $scope.viewModel.qualifications[j].id && el.graduateId === $scope.viewModel.id;
                    });
                }
            },
            function (error) {

            });
        }

        $scope.getTertiaries = function () {
            TertiaryService.TertiaryList().then(
                function (result) {
                    $scope.tertiaries = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                }
            );
        }
        $scope.getTertiaries();


        var getProvinces = function () {
            $scope.provinceList = EnumsService.provinceTypes();
            if ($scope.viewModel.id === 0)
                $scope.provinceList.unshift({ 'value': undefined, 'description': 'Select province' });
        };

        if ($scope.viewModel.id > 0) {
            getProvinces();
            $scope.updateStatus = function () { };

            GraduateService.GraduateGet($scope.viewModel.id).then(
           function (result) {
               $scope.viewModel = result.data;
               loadMark();
           },
           function (error) {
               handleError(error);
           });
        }
        else if ($scope.viewModel.id === 0) {
            getProvinces();
        }
        else {
            getProvinces();
            $scope.sortKeyOrder = {
                key: 'Name',
                order: 'ASC'
            };


            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5;
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order === 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };

            $scope.searchFor = '';
            $scope.search = function () {
                loadGrid();
            }




            var loadGrid = function () {
                var searchFor = '';

                GraduateService.GraduateGrid($scope.currentPage, $scope.recordsPerPage,
                                          $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                    function (result) {
                        $scope.GraduateGrid = result.data.results;
                        $scope.totalItems = result.data.recordCount;
                        loadMarks();

                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });

            };
            $scope.pageChanged = function () {
                loadGrid();
            };
            loadGrid();
        };


        $scope.successMessage = "Saved Successfully";

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewGraduateForm.$invalid)
                return;
            var result = GraduateService.GraduateSave($scope.viewModel);
            result.then(function (result) {
                $scope.handleSuccess = function () { };
                $timeout(function () { $window.history.back(); }, 2000);
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        $scope.newGraduate = function () {
            $location.path("/GraduateEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };



        $scope.print = function (layer) {
            var generator = window.open("Graduate Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }
    }
]);