﻿AngularApp.controller('IndexController',
['$scope', 'Security', 'AccountService', '$rootScope', 'EnumsService', '$location', 
function ($scope, Security, AccountService, $rootScope, EnumsService, $location) {

    $scope.applicationReady = false;
    $scope.navbarProperties = {
        isCollapsed: true
    };

    $scope.currentUser = Security.currentUser;

    $scope.goHome = function () {
        var user = Security.currentUser;

        if (user.userType == 'Admin')
              $location.path('/dashboard');
        else if (user.userType == 'ServiceProvider')
            $location.path('/editServiceProviderServices/');
        else
           $location.path('/home');
    }

    //Login state
    $scope.initHome = function (loggedIn, currentUser) {
        if (loggedIn) {
            AccountService.currentUser().then(
             function (result) {
                 Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges, result.data.userTypeString);

                 $scope.userTypes = EnumsService.userTypes();

                 if (result.data.userTypeString == 'Consumer')
                     $location.path('/consumerChecks');
                 else 
                     $location.path('/home');
             },

             function (error) {
                 $scope.hasError = true;
                 $scope.errorMessage = error.data.message;
                 Security.logout();
             }

             );
        }
    }

    $scope.logout = function () {
        AccountService.singOut().then(function (data) {
            $scope.currentUser.loggedIn = false;
             
            Security.logout();
            $location.path('/login');
           
        },
        function (error) {
            $scope.currentUser.loggedIn = false;
            $scope.$apply();

            Security.logout();
            $location.path('/login');
        });
    }

    $rootScope.$on(Security.scopeUpdateEvent, function (event, currentUser) {
        $scope.currentUser = Security.currentUser;
    });


    $rootScope.$on(EnumsService.enumsLoadedEvent, function (event) {
        $scope.applicationReady = true;

    });

    $scope.isAllowed = function (privilegeType) {
        return Security.isAllowed(privilegeType);
    }

    $scope.userHasPrivileges = function () {
        return Security.userHasPrivileges();
    }

    
}
]);