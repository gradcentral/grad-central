﻿AngularApp.controller('TertiaryController',
    [
        '$scope', '$location', '$window', '$timeout', '$routeParams', 'TertiaryService', 'EnumsService',
        function($scope, $location, $window, $timeout, $routeParams, TertiaryService, EnumsService) {

            $scope.viewModel = {
                province: undefined
            };

            $scope.viewModel.id = $routeParams.id;

            if ($scope.viewModel.id > 0) {
                $scope.updateStatus = function() {};

                $scope.tertiaryTypes = EnumsService.tertiaryTypes();

                $scope.options = [
                    { value: 0, lable: $scope.tertiaryTypes[0].description },
                    { value: 1, lable: $scope.tertiaryTypes[1].description },
                    { value: 2, lable: $scope.tertiaryTypes[2].description }
                ];


                $scope.provinceTypes = EnumsService.provinceTypes();

                $scope.provinceOptions = [
                    { value: 0, lable: $scope.provinceTypes[0].description },
                    { value: 1, lable: $scope.provinceTypes[1].description },
                    { value: 2, lable: $scope.provinceTypes[2].description },
                    { value: 3, lable: $scope.provinceTypes[3].description },
                    { value: 4, lable: $scope.provinceTypes[4].description },
                    { value: 5, lable: $scope.provinceTypes[5].description },
                    { value: 6, lable: $scope.provinceTypes[6].description },
                    { value: 7, lable: $scope.provinceTypes[7].description },
                    { value: 8, lable: $scope.provinceTypes[8].description }
                ];

                TertiaryService.TertiaryGet($scope.viewModel.id).then(
                    function(result) {
                        $scope.viewModel = result.data;
                    },
                    function(error) {
                        handleError(error);
                    });
            } else if ($scope.viewModel.id == 0) {

                $scope.tertiaryTypes = EnumsService.tertiaryTypes();

                $scope.options = [
                    { value: undefined, lable: 'Select' },
                    { value: 0, lable: $scope.tertiaryTypes[0].description },
                    { value: 1, lable: $scope.tertiaryTypes[1].description },
                    { value: 2, lable: $scope.tertiaryTypes[2].description }
                ];


                $scope.provinceTypes = EnumsService.provinceTypes();

                $scope.provinceOptions = [
                    { value: undefined, lable: 'Select' },
                    { value: 0, lable: $scope.provinceTypes[0].description },
                    { value: 1, lable: $scope.provinceTypes[1].description },
                    { value: 2, lable: $scope.provinceTypes[2].description },
                    { value: 3, lable: $scope.provinceTypes[3].description },
                    { value: 4, lable: $scope.provinceTypes[4].description },
                    { value: 5, lable: $scope.provinceTypes[5].description },
                    { value: 6, lable: $scope.provinceTypes[6].description },
                    { value: 7, lable: $scope.provinceTypes[7].description },
                    { value: 8, lable: $scope.provinceTypes[8].description }
                ];
            } else {


                $scope.sortKeyOrder = {
                    key: 'Name',
                    order: 'ASC'
                };


                $scope.totalItems = 0;
                $scope.currentPage = 1;
                $scope.maxSize = 5;
                $scope.recordsPerPage = 20;
                $scope.numberOfPageButtons = 5;

                $scope.sort = function(col) {
                    if ($scope.sortKeyOrder.key === col) {
                        if ($scope.sortKeyOrder.order == 'ASC')
                            $scope.sortKeyOrder.order = 'DESC';
                        else
                            $scope.sortKeyOrder.order = 'ASC';
                    } else {
                        $scope.sortKeyOrder.key = col;
                        $scope.sortKeyOrder.order = 'ASC';
                    }
                    loadGrid();
                };

                $scope.searchFor = '';
                $scope.search = function() {
                    loadGrid();
                }


                var loadGrid = function() {
                    var searchFor = '';

                    TertiaryService.TertiaryGrid($scope.currentPage,
                        $scope.recordsPerPage,
                        $scope.sortKeyOrder.key,
                        $scope.sortKeyOrder.order,
                        $scope.searchFor).then(
                        function(result) {
                            $scope.TertiaryGrid = result.data.results;
                            $scope.totalItems = result.data.recordCount;
                        },
                        function(error) {
                            alert("an error occured: unable to get data");
                        });


                };
                $scope.pageChanged = function() {
                    loadGrid();
                };
                loadGrid();
            };

            $scope.successMessage = "Saved Successfully";

            $scope.submitForm = function() {
                $scope.$broadcast('show-errors-check-validity');

                if ($scope.NewTertiaryForm.$invalid)
                    return;

                var result = TertiaryService.TertiarySave($scope.viewModel);

                result.then(function(result) {
                        $scope.handleSuccess = function() {};
                        $timeout(function() { $window.history.back(); }, 2000);
                    },
                    function(error) {
                        $scope.hasError = true;
                        $scope.errorMessage = error.data.message;
                    });
            };

            $scope.resetForm = function() {
                $scope.$broadcast('show-errors-reset');
            }

            $scope.cancelForm = function() {
                $scope.$broadcast('show-errors-reset');
                $window.history.back();
            }

            $scope.newTertiary = function() {
                $location.path("/tertiaryEdit/0");
            };

            var handleError = function(error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            };
            $scope.print = function(layer) {
                var generator = window.open("Tertiary Report");
                var layerText = document.getElementById(layer);
                generator.document.write(
                    '<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

                generator.document.close();
            };
        }
]);