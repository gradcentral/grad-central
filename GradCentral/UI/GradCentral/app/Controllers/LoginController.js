﻿AngularApp.controller('LoginController',
    ['$scope', '$timeout', 'AccountService', '$modal', 'Security', '$location', 'EnumsService',
        function ($scope, $timeout, AccountService, $modal, Security, $location, EnumsService) {

            $scope.viewModel = {}

            $scope.successMessage = "Login Successfull";

            var po = document.createElement('script');
            po.type = 'text/javascript';
            po.async = true;
            po.src = 'https://apis.google.com/js/client:plusone.js';
            var s = document.getElementsByTagName('script')[0];
            s.parentNode.insertBefore(po, s);

            $scope.onSignIn = function onSignIn(googleUser) {
                var profile = googleUser.getBasicProfile();
                console.log('ID: ' + profile.getId()); // Do not send to your backend! Use an ID token instead.
                console.log('Name: ' + profile.getName());
                console.log('Image URL: ' + profile.getImageUrl());
                console.log('Email: ' + profile.getEmail()); // This is null if the 'email' scope is not present.
            };

            $scope.signOut =  function signOut() {
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {
                    console.log('User signed out.');
                });
            }

            $scope.submitForm = function () {
                $scope.hasError = false;
                $scope.$broadcast('show-errors-check-validity');

                if ($scope.loginForm.$valid) {
                    AccountService.login($scope.viewModel).then(
                        function (result) {
                            Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges, result.data.userTypeString);
                            $scope.handleSuccess = function () { };
                            if (result.data.userTypeString === 'Graduate') {
                                $location.path('/graduateDashboard');
                            }
                            else if (result.data.userTypeString === 'Recruiter') {
                                $location.path('/recruiterDashboard');
                            }
                            else {
                                $location.path('/dashboard');
                            }
                        },
                        function (error) {
                            $scope.hasError = true;
                            $scope.errorMessage = error.data.message;
                            Security.logout();
                        }
                        );
                }
            }

            $scope.forgotPassword = function () {

                var modalInstance = $modal.open({
                    animation: false,
                    templateUrl: 'app/Views/Account/ForgotPasswordView.html',
                    controller: 'ForgotPasswordController'
                });
                modalInstance.result.then(function (result) {
                    // $log.info(' button ok reached');
                }, function () {
                    // $log.info(' button cancel reached');
                });
            };

            $scope.resetForm = function () {
                $scope.$broadcast('show-errors-reset');
            }
        }
    ]);

