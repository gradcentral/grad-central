AngularApp.controller('RecruiterController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'RecruiterService', 'EnumsService',
    function ($scope, $location, $window, $timeout, $routeParams, RecruiterService, EnumsService) {
        $scope.viewModel = {
            province: undefined
        };
        $scope.provinceList = [];
        $scope.viewModel.id = $routeParams.id;

        var getProvinces = function () {
            $scope.provinceList = EnumsService.provinceTypes();
            if ($scope.viewModel.id === 0)
                $scope.provinceList.unshift({ 'value': undefined, 'description': 'Select province' });
        };

        var loadGrid = function () {
            var searchFor = '';

            RecruiterService.RecruiterGrid($scope.currentPage, $scope.recordsPerPage,
                $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.RecruiterGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });


        };

        if ($scope.viewModel.id > 0) {
            getProvinces();
            $scope.updateStatus = function () { };

            RecruiterService.RecruiterGet($scope.viewModel.id).then(
           function (result) {
               console.log(result);
               $scope.viewModel = result.data;
           },
           function (error) {
               handleError(error);
           });
        }
        else if ($scope.viewModel.id === 0) {
            getProvinces();
        }
        else {
            getProvinces();
            $scope.sortKeyOrder = {
                key: 'Name',
                order: 'ASC'
            };


            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5;
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order === 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };

            $scope.searchFor = '';
            $scope.search = function () {
                loadGrid();
            };
            $scope.pageChanged = function () {
                loadGrid();
            };
            loadGrid();
        }


        $scope.successMessage = "Saved Successfully";

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewRecruiterForm.$invalid)
                return;

            var result = RecruiterService.RecruiterSave($scope.viewModel);
            result.then(function (result) {
                $scope.handleSuccess = function () { };
                $timeout(function () { $window.history.back(); }, 2000);
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        };

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        };

        $scope.newRecruiter = function () {
            $location.path("/RecruiterEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };

        $scope.print = function(layer) {
            var generator = window.open("Recruiter Report");
            var layerText = document.getElementById(layer);
            generator.document.write(
                '<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        };
    }
]);