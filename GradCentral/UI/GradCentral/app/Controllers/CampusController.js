﻿AngularApp.controller('CampusController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'CampusService', 'EnumsService', 'TertiaryService',
    function ($scope, $location, $window, $timeout, $routeParams, CampusService, EnumsService, TertiaryService) {

        $scope.viewModel = {
            tertiary: {},
            province: 'Select'
        };

        $scope.provinceTypes = [];
        $scope.provinceOptions = [];
        $scope.tertiaryList = [];

        $scope.viewModel.id = $routeParams.id;

        var loadTypes = function () {
            $scope.provinceTypes = EnumsService.provinceTypes();

            $scope.provinceOptions = [
                { value: 0, lable: $scope.provinceTypes[0].value },
                { value: 1, lable: $scope.provinceTypes[1].value },
                { value: 2, lable: $scope.provinceTypes[2].value },
                { value: 3, lable: $scope.provinceTypes[3].value },
                { value: 4, lable: $scope.provinceTypes[4].value },
                { value: 5, lable: $scope.provinceTypes[5].value },
                { value: 6, lable: $scope.provinceTypes[6].value },
                { value: 7, lable: $scope.provinceTypes[7].value },
                { value: 8, lable: $scope.provinceTypes[8].value }
            ];
        }

        var getTertiaries = function () {
            TertiaryService.TertiaryList().then(
                function (result) {
                    $scope.tertiaryList = result.data;
                    $scope.tertiaryList.unshift({ 'id': undefined, 'name': 'Select tertiary' });
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });
        }

        if ($scope.viewModel.id > 0) {
            loadTypes();
            CampusService.CampusGet($scope.viewModel.id).then(
               function (result) {
                   $scope.viewModel = result.data;
               },
               function (error) {
                   handleError(error);
               });
            getTertiaries();

        }
        else if ($scope.viewModel.id == 0) {

            $scope.provinceTypes = EnumsService.provinceTypes();

            $scope.provinceOptions = [
                { value: undefined, lable: 'Select' },
                { value: 0, lable: $scope.provinceTypes[0].value },
                { value: 1, lable: $scope.provinceTypes[1].value },
                { value: 2, lable: $scope.provinceTypes[2].value },
                { value: 3, lable: $scope.provinceTypes[3].value },
                { value: 4, lable: $scope.provinceTypes[4].value },
                { value: 5, lable: $scope.provinceTypes[5].value },
                { value: 6, lable: $scope.provinceTypes[6].value },
                { value: 7, lable: $scope.provinceTypes[7].value },
                { value: 8, lable: $scope.provinceTypes[8].value }
            ];
            getTertiaries();
        }
        else {



            $scope.sortKeyOrder = {
                key: 'Name',
                order: 'ASC'
            };


            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5;
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order == 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };

            $scope.searchFor = '';
            $scope.search = function () {
                loadGrid();
            }

            var loadGrid = function () {
                var searchFor = '';

                CampusService.CampusGrid($scope.currentPage, $scope.recordsPerPage,
                                          $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                    function (result) {
                        $scope.CampusGrid = result.data.results;
                        $scope.totalItems = result.data.recordCount;
                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });


            };
            $scope.pageChanged = function () {
                loadGrid();
            };
            loadGrid();
        };


         $scope.print = function (layer) {
            var generator = window.open("Campus Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }

        $scope.successMessage = "Saved Successfully";

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewCampusForm.$invalid)
                return;

            $scope.viewModel.tertiary.TertiaryId = 1;
            var result = CampusService.CampusSave($scope.viewModel);
            result.then(function (result) {
                $scope.handleSuccess = function () { };
                $timeout(function () { $window.history.back(); }, 2000);
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        $scope.newCampus = function () {
            $location.path("/campusEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };
    }
]);