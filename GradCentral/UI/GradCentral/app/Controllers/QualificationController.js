﻿AngularApp.controller('QualificationController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'QualificationService', 'EnumsService', 'FacultyService',
function ($scope, $location, $window, $timeout, $routeParams, QualificationService, EnumsService, FacultyService) {

    $scope.viewModel = {
        facultylist: {}
    };

    $scope.facultyList = [];

    $scope.viewModel.id = $routeParams.id;

    var getFaculties = function () {
        FacultyService.FacultyList().then(
            function (result) {
                $scope.facultyList = result.data;
                $scope.facultyList.unshift({ 'id': undefined, 'name': 'Select faculty' });
            },
            function (error) {
                alert("an error occured: unable to get data");
            });
    };

    if ($scope.viewModel.id > 0) {
        //loadTypes();
        $scope.qualificationTypes = EnumsService.qualificationTypes();

        $scope.qualificationOptions = [
            { value: 0, lable: $scope.qualificationTypes[0].value },
            { value: 1, lable: $scope.qualificationTypes[1].value },
            { value: 2, lable: $scope.qualificationTypes[2].value },
            { value: 3, lable: $scope.qualificationTypes[3].value },
            { value: 3, lable: $scope.qualificationTypes[4].value },
            { value: 5, lable: $scope.qualificationTypes[5].value },
            { value: 6, lable: $scope.qualificationTypes[6].value },
            { value: 7, lable: $scope.qualificationTypes[7].value },
            { value: 8, lable: $scope.qualificationTypes[8].value },
            { value: 9, lable: $scope.qualificationTypes[9].value },
            { value: 10, lable: $scope.qualificationTypes[10].value },

        ];

        QualificationService.QualificationGet($scope.viewModel.id).then(
            function (result) {
                $scope.viewModel = result.data;
            },
            function (error) {
                handleError(error);
            });
        getFaculties();
    }
    else if ($scope.viewModel.id == 0) {

        $scope.qualificationTypes = EnumsService.qualificationTypes();

        $scope.qualificationOptions = [
            { value: 0, lable: $scope.qualificationTypes[0].value },
            { value: 1, lable: $scope.qualificationTypes[1].value },
            { value: 2, lable: $scope.qualificationTypes[2].value },
            { value: 3, lable: $scope.qualificationTypes[3].value },
            { value: 3, lable: $scope.qualificationTypes[4].value },
            { value: 5, lable: $scope.qualificationTypes[5].value },
            { value: 6, lable: $scope.qualificationTypes[6].value },
            { value: 7, lable: $scope.qualificationTypes[7].value },
            { value: 8, lable: $scope.qualificationTypes[8].value },
            { value: 9, lable: $scope.qualificationTypes[9].value },
            { value: 10, lable: $scope.qualificationTypes[10].value }
        ];
        getFaculties();
    }
    else {
        $scope.sortKeyOrder = {
            key: 'Name',
            order: 'ASC'
        };
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order === 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        }


        var loadGrid = function () {
            var searchFor = '';

            QualificationService.QualificationGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.QualificationGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });
        };
        $scope.pageChanged = function () {
            loadGrid();
        };
        loadGrid();
    };

    $scope.print = function (layer) {
        var generator = window.open("Qualification Report");
        var layerText = document.getElementById(layer);
        generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

        generator.document.close();
    }

    $scope.successMessage = "Saved Successfully";

    $scope.submitForm = function () {
        $scope.$broadcast('show-errors-check-validity');

        if ($scope.NewQualificationForm.$invalid)
            return;

        
        var result = QualificationService.QualificationSave($scope.viewModel);

        result.then(function (result) {


            $scope.handleSuccess = function () { };

            $timeout(function () { $window.history.back(); }, 5000);

        },
        function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        });
    };

    $scope.resetForm = function () {
        $scope.$broadcast('show-errors-reset');
    };

    $scope.cancelForm = function () {
        $scope.$broadcast('show-errors-reset');
        $window.history.back();
    };

    $scope.newQualification = function () {
        $location.path("/qualificationEdit/0");
    };

    var handleError = function (error) {
        $scope.hasError = true;
        $scope.errorMessage = error.data.message;
    };

}

]);