AngularApp.controller('PossibleCandidateController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'GraduateService', 'EnumsService', 'TertiaryService', 'NotificationService',
    function ($scope, $location, $window, $timeout, $routeParams, GraduateService, EnumsService, TertiaryService, NotificationService) {
        $scope.viewModel = {
            province: undefined
        };
        $scope.provinceList = [];
        $scope.possibleCandidatesList = [];
        $scope.jobCandidates = [];
        $scope.viewModel.id = $routeParams.id;
        $scope.gender = sessionStorage.getItem('gender');
        $scope.isDisable = sessionStorage.getItem('isDisable');
        $scope.qualificationId = 2;
        $scope.position = sessionStorage.getItem('position');
        $scope.race = sessionStorage.getItem('race');
        $scope.gpa = sessionStorage.getItem('gpa');
        $scope.qualification = JSON.parse(sessionStorage.getItem('qualification'));
        $scope.job = JSON.parse(sessionStorage.getItem('job'));
        sessionStorage.clear();
        $scope.id = $routeParams.id;

        var loadMark = function () {
            GraduateService.MarksList().then(function (result) {
                var marks = result.data;
                for (var i = 0; i < $scope.GraduateGrid.length; i++) {
                    for (var j = 0; j < $scope.GraduateGrid[i].qualifications.length; j++) {
                        $scope.GraduateGrid[i].qualifications[j].marks = marks.filter(function (el) {
                            return el.qualificationId === $scope.GraduateGrid[i].qualifications[j].id && el.graduateId === $scope.GraduateGrid[i].id;
                        });
                    }
                }
            },
            function (error) {

            });
        }


        $scope.getJobCandidates = function (jobId) {
            GraduateService.JobCandidates(jobId).then(
                function (result) {
                    $scope.jobCandidates = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                }
            );
        }

        if ($scope.id) {
            $scope.getJobCandidates($scope.id);
        }

        $scope.addCandidate = function (graduate) {
            $scope.possibleCandidatesList.push(graduate);

            $scope.GraduateGrid = $scope.GraduateGrid.filter(function (el) {
                return el !== graduate;
            });
        };


        $scope.removeGraduate = function (graduate) {
            $scope.GraduateGrid.push(graduate);
            //$scope.possibleCandidatesList.splice(possibleCandidatesList);
            $scope.possibleCandidatesList = $scope.possibleCandidatesList.filter(function (el) {
                return el !== graduate;
            });
        }

        $scope.dateTime = Date.now();

        $scope.getTertiaries = function () {
            TertiaryService.TertiaryList().then(
                function (result) {
                    $scope.tertiaries = result.data;
                },
                function (error) {
                    $scope.hasError = true;
                    $scope.errorMessage = error.data.message;
                }
            );
        }

        $scope.getTertiaries();



        var getProvinces = function () {
            $scope.provinceList = EnumsService.provinceTypes();
            if ($scope.viewModel.id === 0)
                $scope.provinceList.unshift({ 'value': undefined, 'description': 'Select province' });
        };

        if ($scope.viewModel.id > 0) {
            // getProvinces();
            // $scope.updateStatus = function () { };

            // GraduateService.GraduateGet($scope.viewModel.id).then(
            //function (result) {
            //    $scope.viewModel = result.data;
            //},
            //function (error) {
            //    handleError(error);
            //});
        }
        else if ($scope.viewModel.id === 0) {
            getProvinces();
        }
        else {
            getProvinces();
            $scope.sortKeyOrder = {
                key: 'Name',
                order: 'ASC'
            };


            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5;
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order === 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };

            $scope.searchFor = '';
            $scope.search = function () {
                loadGrid();
            }


            var loadGrid = function () {
                var searchFor = '';

                GraduateService.PossibleCandidatesGrid($scope.currentPage, $scope.recordsPerPage,
                                          $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor,
                                          $scope.race, $scope.gpa, $scope.isDisable, $scope.gender, $scope.qualificationId).then(
                    function (result) {
                        $scope.GraduateGrid = result.data.results;
                        loadMark();
                        for (var i = 0; i < $scope.GraduateGrid.length; i++) {
                            for (var j = 0; j < $scope.GraduateGrid[i].qualifications.length; j++) {
                                $scope.GraduateGrid[i].qualifications[j].faculty = null;
                            }

                        }
                        $scope.totalItems = result.data.recordCount;

                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });

            };
            $scope.pageChanged = function () {
                loadGrid();
            };
            loadGrid();
        };


        $scope.successMessage = "Sent Successfully and email sent";

        $scope.submitForm = function () {
            //$scope.$broadcast('show-errors-check-validity');

            if ($scope.possibleCandidatesList.length === 0)
                return;

            var notification = {
                Job: $scope.job,
                Graduates: $scope.possibleCandidatesList
            }
            var result = NotificationService.NotifyCandidates(notification);
            result.then(function (result) {
                $scope.handleSuccess = function () { };
                setTimeout(function () {
                    $scope.handleSuccess = false;
                }, 2000);
                //$timeout(function () { $window.history.back(); }, 2000);
            },
            function (error) {
                $scope.hasError = true;
                setTimeout(function () {
                    $scope.handleError = false;
                }, 2000);
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        $scope.newGraduate = function () {
            $location.path("/GraduateEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };



        $scope.print = function (layer) {
            var generator = window.open("Graduate Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }
    }
]);