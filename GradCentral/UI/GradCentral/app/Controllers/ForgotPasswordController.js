﻿AngularApp.controller('ForgotPasswordController',
    ['$scope', '$modalInstance', '$timeout', '$location', 'AccountService',
    function ($scope, $modalInstance, $timeout, $location, AccountService) {
        $scope.viewModel = {};

        $scope.successMessage = "Password Sent";

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;

        };

        $scope.ok = function () {

            if ($scope.forgotPasswordForm.$invalid)
            {
                $scope.hasError = true;
                $scope.errorMessage = "Please enter user name";
                return;
            }

            var result = AccountService.SendPassword($scope.viewModel);

            result.then(function (result) {

                $scope.handleSuccess = function () { };

                $timeout(function () {
                    $modalInstance.close($location.path('/login'));
                }, 1000);
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');

        };

    }
    ]);