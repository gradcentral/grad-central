﻿AngularApp.controller('ModuleController',
   [
       '$scope', '$location', '$window', '$timeout', '$routeParams', 'ModuleService', 'EnumsService', 'QualificationService',
    function ($scope, $location, $window, $timeout, $routeParams, ModuleService, EnumsService, QualificationService) {

        $scope.viewModel = {
            qualificationList: {}
        };

        $scope.qualificationList = [];
        
        $scope.viewModel.id = $routeParams.id;

        var getQualifications = function () {
            QualificationService.QualificationList().then(
                function (result) {
                    $scope.qualificationList = result.data;
                    $scope.qualificationList.unshift({ 'id': undefined, 'name': 'Select qualification' });
                },
                function (error) {
                    alert("an error occured: unable to get data");

                });
        };

        if ($scope.viewModel.id > 0) {
            //loadTypes();
            $scope.semesterTypes = EnumsService.semesterTypes();

            $scope.semesterOptions = [
                { value: 0, lable: $scope.semesterTypes[0].value },
                { value: 1, lable: $scope.semesterTypes[1].value }
            ];
            
            ModuleService.ModuleGet($scope.viewModel.id).then(
                function (result) {
                    $scope.viewModel = result.data;
                },
                function (error) {
                    handleError(error);
                });
            getQualifications();
        }
        else if ($scope.viewModel.id == 0) {

            $scope.semesterTypes = EnumsService.semesterTypes();

            $scope.semesterOptions = [
                { value: undefined, lable: 'Select' },
                { value: 0, lable: $scope.semesterTypes[0].value },
                { value: 1, lable: $scope.semesterTypes[1].value }
            ];
            getQualifications();
        }
        else {
            $scope.sortKeyOrder = {
                key: 'Name',
                order: 'ASC'
            };
            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5;
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order === 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };

            $scope.searchFor = '';
            $scope.search = function () {
                loadGrid();
            }


            var loadGrid = function () {
                var searchFor = '';

                ModuleService.ModuleGrid($scope.currentPage, $scope.recordsPerPage,
                                          $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                    function (result) {
                        $scope.ModuleGrid = result.data.results;
                        $scope.totalItems = result.data.recordCount;
                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });
            };
            $scope.pageChanged = function () {
                loadGrid();
            };
            loadGrid();
        };

         $scope.print = function (layer) {
            var generator = window.open("Module Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }

        $scope.successMessage = "Saved Successfully";

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewModuleForm.$invalid)
                return;

            //$scope.viewModel.qualification.QualificationId = 1;
            var result = ModuleService.ModuleSave($scope.viewModel);

            result.then(function (result) {


                $scope.handleSuccess = function () { };

                $timeout(function () { $window.history.back(); }, 5000);

            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        };

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        };

        $scope.newModule = function () {
            $location.path("/moduleEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };

    }

   ]);