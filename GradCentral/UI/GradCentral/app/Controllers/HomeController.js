﻿AngularApp.controller('HomeController',
    ['$scope',  'Security', '$rootScope', 'EnumsService',
function ($scope,  Security, $rootScope, EnumsService) {
    $scope.applicationReady = EnumsService.enumsLoaded();

    $scope.navbarProperties = {
        isCollapsed: true
    };

    $scope.currentUser = Security.currentUser;
    $scope.userLoggedIn = false;


    $rootScope.$on(Security.scopeUpdateEvent, function (event, currentUser) {
        $scope.currentUser = Security.currentUser;
        $scope.userLoggedIn = true;

    });

    $rootScope.$on(EnumsService.enumsLoadedEvent, function (event) {
        $scope.applicationReady = true;
        //$scope.$apply();
    });

}]);


