﻿AngularApp.controller('NotificationController',
    [
        '$scope', '$location', '$window', '$timeout', '$routeParams', 'NotificationService', 'EnumsService', 'JobService', 'UserService',
        function ($scope, $location, $window, $timeout, $routeParams, NotificationService, EnumsService, JobService, UserService) {

            $scope.viewModel = {
                Joblist: {},


            };

            $scope.JobList = [];

            $scope.viewModel.id = $routeParams.id;

            var getJobs = function () {
                JobsService.JobList().then(
                    function (result) {
                        $scope.JobList = result.data;
                        $scope.JobList.unshift({ 'id': undefined, 'Title': 'Select Job' });
                    },
                    function (error) {
                        alert("an error occured: unable to get data");
                    });
            };



            if ($scope.viewModel.id > 0) {

                NotificationService.NotificationGet($scope.viewModel.id).then(
                    function (result) {
                        $scope.viewModel = result.data;
                    },
                    function (error) {
                        handleError(error);
                    });
                getJobs();
            }
            else if ($scope.viewModel.id == 0) {
                getJobs();
            }
            else {
                $scope.sortKeyOrder = {
                    key: 'Name',
                    order: 'ASC'
                };
                $scope.totalItems = 0;
                $scope.currentPage = 1;
                $scope.maxSize = 5;
                $scope.recordsPerPage = 20;
                $scope.numberOfPageButtons = 5;

                $scope.sort = function (col) {
                    if ($scope.sortKeyOrder.key === col) {
                        if ($scope.sortKeyOrder.order === 'ASC')
                            $scope.sortKeyOrder.order = 'DESC';
                        else
                            $scope.sortKeyOrder.order = 'ASC';
                    } else {
                        $scope.sortKeyOrder.key = col;
                        $scope.sortKeyOrder.order = 'ASC';
                    }
                    loadGrid();
                };

                $scope.searchFor = '';
                $scope.search = function () {
                    loadGrid();
                }


                var loadGrid = function () {
                    var searchFor = '';

                    NotificationService.NotificationGrid($scope.currentPage, $scope.recordsPerPage,
                        $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                        function (result) {
                            $scope.NotificationGrid = result.data.results;
                            $scope.totalItems = result.data.recordCount;
                        },
                        function (error) {
                            alert("an error occured: unable to get data");
                        });
                };
                $scope.pageChanged = function () {
                    loadGrid();
                };
                loadGrid();
            };

            $scope.successMessage = "Saved Successfully";

            $scope.submitForm = function () {
                $scope.$broadcast('show-errors-check-validity');


                var result = NotificationService.NotificationSave($scope.viewModel);

                result.then(function (result) {


                    $scope.handleSuccess = function () { };

                    $timeout(function () { $window.history.back(); }, 5000);

                },
                    function (error) {
                        $scope.hasError = true;
                        $scope.errorMessage = error.data.message;
                    });
            };

            $scope.resetForm = function () {
                $scope.$broadcast('show-errors-reset');
            };

            $scope.cancelForm = function () {
                $scope.$broadcast('show-errors-reset');
                $window.history.back();
            };
            var handleError = function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            };

        }

    ]);