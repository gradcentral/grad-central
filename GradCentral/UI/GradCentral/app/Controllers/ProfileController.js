﻿AngularApp.controller('ProfileController',
[
    '$scope', '$location', '$window', '$timeout', '$modal', '$log', '$routeParams', 'EnumsService', 'ProfileService',
    function($scope, $location, $window, $timeout, $modal, $log, $routeParams, EnumsService, ProfileService) {

        $scope.viewModel = {};

        ProfileService.ProfileGet().then(
            function(result) {
                $scope.viewModel = result.data;
            },
            function (error) {
                handleError(error);

            });

        $scope.editProfile = function() {
            $location.path('/myProfile');
        }


        $scope.successMessage = "Profile Changes Saved";

        $scope.submitForm = function() {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.profileEditForm.$invalid)
                return;

            
                var result = ProfileService.ProfileSave($scope.viewModel);

                result.then(function(result) {
                    $scope.handleSuccess = function () { };

                    $timeout(function () { $window.history.back(); }, 2000);

                },
                function (error) {
                    $scope.hasError(error);

                });
            
        }


        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
            Security.logout();
        };

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        };

        $scope.changePassword = function () {
            $scope.$broadcast('show-errors-check-validity');
            if ($scope.profileEditForm.$invalid)
                return;

            var modalInstance = $modal.open({
                animation: true,
                templateUrl: 'app/Views/Account/PasswordView.html',
                controller: 'PasswordController',
                size: 'sm'
            });
            modalInstance.result.then(function (result) {
                $log.info(' button ok reached');
            }, function () {
                $log.info(' button cancel reached');
            });

        };



    }
]);