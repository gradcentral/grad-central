﻿AngularApp.controller('RegisterController',
    ['$scope', '$location', '$window', '$timeout', 'AccountService', 'Security', 'EnumsService', '$http',

    function ($scope, $location, $window, $timeout, AccountService, Security, EnumsService, $http) {

        $scope.viewModel = {

        }
        $scope.files = [];
        $scope.genderOptions = [];
        $scope.titleOptions = [];
        $scope.raceOptions = [];

        $scope.titleTypes = [];
        $scope.raceTypes = []
        $scope.genderTypes = [];

        $scope.successMessage = "Registered successfully";

        $scope.submitForm = function () {

            $scope.$broadcast('show-errors-check-validity');

            if ($scope.registerForm.$invalid)
                return;

            var result = AccountService.register($scope.viewModel);

            result.then(function (result) {

                $scope.handleSuccess = function () { };

                $timeout(function () {
                    Security.login(result.data.id, result.data.userName, result.data.displayName, result.data.allowedPrivileges, result.data.userTypeString);
                    $location.path('/consumerChecks');
                }, 2000);


            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
                Security.logout();
            });
        }

        $scope.uploadFile = function (file) {
            $scope.files = document.getElementById('DocumentUploadTB').files;
            //var deferred = $q.defer();

            if ($scope.files) {
                $http({
                    url: '/api/Account/AddFile',
                    method: "POST",
                    fields: {'username': 'Siya'},
                    file: $scope.files,
                    transformRequest: angular.identity,
                    headers: { 'Content-Type': undefined }
            }).success(function (response) {
                    callback(response);
                }).error(function (error) {
                    console.log(error);
                });
            }
            //$upload.upload({
            //    url: "/api/Account/AddFile",
            //    method: "POST",
            //    file: files,
            //    data: args
            //}).progress(function (evt) {
            //    // get upload percentage
            //    console.log('percent: ' + parseInt(100.0 * evt.loaded / evt.total));
            //}).success(function (data, status, headers, config) {
            //    // file is uploaded successfully
            //    deferred.resolve(data);
            //}).error(function (data, status, headers, config) {
            //    // file failed to upload
            //    deferred.reject();
            //});

            //return deferred.promise;
        }

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }


        var loadGenderTypes = function () {
            if ($scope.genderOptions.length === 0)
                $scope.genderTypes = EnumsService.genderType();
            $scope.viewModel.gender = null;

            $scope.genderOptions = $scope.genderTypes;
            $scope.genderOptions.unshift({ 'value': null, 'description': 'Select gender' });
        }

        var loadTitleTypes = function () {
            if ($scope.titleOptions.length === 0)
                $scope.titleTypes = EnumsService.titleType();
            $scope.viewModel.title = null;
            $scope.titleOptions = $scope.titleTypes;
            $scope.titleOptions.unshift({ 'value': null, 'description': 'Select title' });
        }

        loadGenderTypes();
        loadTitleTypes();

        $scope.$broadcast('show-errors-reset');
    }

    ]);


