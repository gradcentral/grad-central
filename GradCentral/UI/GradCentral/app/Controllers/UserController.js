﻿AngularApp.controller('UserController',
['$scope', '$location', '$window', '$routeParams', '$timeout', 'AccountService', 'UserService',
    'RoleService', 'EnumsService', 'CompanyService', 'TertiaryService', 'QualificationService',
    function ($scope, $location, $window, $routeParams, $timeout, AccountService, UserService,
        RoleService, EnumsService, CompanyService, TertiaryService, QualificationService) {

        $scope.viewModel = {
            title: null,
            gender: null,
            type: null,
            roleList: [],
            recruiter: {
                companyId: null
            },
            marks: [],
            graduate: {
                jobId: 0,
                tertiaryId: null,
                qualifications: [],
                race: null
            }
        };

        $scope.qualificationId = String;
        $scope.qualificationId = null;

        $scope.dateTime = Date.now();

        $scope.options = [];

        $scope.genderOptions = [];
        $scope.titleOptions = [];
        $scope.raceOptions = [];

        $scope.titleTypes = [];
        $scope.raceTypes = []
        $scope.genderTypes = [];

        $scope.companies = [];
        $scope.tertiaries = [];
        $scope.qualifications = [];

        $scope.filteredQualifications = [];

        $scope.addedQualifications = [];

        $scope.successMessage = "Saved Successfully";

        $scope.viewModel.id = $routeParams.id;

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };

        var loadCompanies = function () {
            CompanyService.CompanyList().then(
                function (result) {
                    $scope.companies = result.data;
                    if ($scope.viewModel.id === 0 || $scope.companies.length === 0)
                        $scope.companies.unshift({ 'id': null, 'name': 'Select Company' });
                },
                function (error) {
                    handleError(error);
                });
        }

        var loadQualifications = function () {
            QualificationService.QualificationList().then(
                function (result) {
                    $scope.qualifications = result.data;
                },

                function (error) {
                    handleError(error);
                });
        }


        var loadTertiaries = function () {
            TertiaryService.TertiaryList().then(
                function (result) {
                    $scope.tertiaries = result.data;

                    if ($scope.viewModel.id === 0 || $scope.tertiaries.length === 0)
                        $scope.tertiaries.unshift({ 'id': null, 'name': 'Select Tertiary' });
                },

                function (error) {
                    handleError(error);
                });
        }


        var loadUserTypes = function () {
            if ($scope.options.length == 0)
                $scope.userTypes = EnumsService.userTypes();

            $scope.options = $scope.userTypes;
            if ($scope.viewModel.id == 0 && $scope.options[0].value !== null)
                $scope.options.unshift({ 'value': null, 'description': 'Select user type' });
        }

        var loadGenderTypes = function () {
            if ($scope.genderOptions.length == 0)
                $scope.genderTypes = EnumsService.genderType();

            $scope.genderOptions = $scope.genderTypes;
            if ($scope.viewModel.id === 0 && $scope.genderOptions[0].value !== null)
                $scope.genderOptions.unshift({ 'value': null, 'description': 'Select gender' });
        }

        var loadTitleTypes = function () {
            if ($scope.titleOptions.length === 0)
                $scope.titleTypes = EnumsService.titleType();

            $scope.titleOptions = $scope.titleTypes;
            if ($scope.viewModel.id == 0 && $scope.titleOptions[0].value !== null)
                $scope.titleOptions.unshift({ 'value': null, 'description': 'Select title' });
        }

        var loadRaceTypes = function () {
            if ($scope.raceOptions.length === 0)
                $scope.raceTypes = EnumsService.raceType();

            $scope.raceOptions = $scope.raceTypes;
            if ($scope.viewModel.id == 0)
                $scope.raceOptions.unshift({ 'value': null, 'description': 'Select race' });
        }


        if ($scope.viewModel.id > 0) {
            //loadRecruitmentAgencies();
            loadCompanies();
            loadUserTypes();
            loadGenderTypes();
            loadTitleTypes();
            loadRaceTypes();
            loadTertiaries();
            loadQualifications();
            UserService.UserGet($scope.viewModel.id).then(
                function (result) {
                    $scope.viewModel = result.data;
                    $scope.viewModel.title = $scope.viewModel.titleString;
                    $scope.viewModel.gender = $scope.viewModel.genderString;

                    if ($scope.viewModel.graduate !== null) {
                        $scope.viewModel.type = 'Graduate';
                        $scope.viewModel.graduate.race = $scope.viewModel.graduate.raceString;
                    }
                    else if ($scope.viewModel.recruiter !== null)
                        $scope.viewModel.type = 'Recruiter';
                    else
                        $scope.viewModel.type = 'Admin';

                },

                function (error) {
                    handleError(error);
                });
        }
        else if ($scope.viewModel.id == 0) {
            loadUserTypes();
            //loadRecruitmentAgencies();
            loadCompanies();
            loadGenderTypes();
            loadTitleTypes();
            loadRaceTypes();
            loadTertiaries();
            loadQualifications();
            RoleService.RoleList().then(
                function (result) {
                    $scope.viewModel.roleList = result.data;

                    for (var i = 0; i < $scope.viewModel.roleList.length; i++) {
                        if ($scope.viewModel.roleList[i].status !== "Active")
                            $scope.viewModel.roleList.splice(i, 1);
                    }

                },
                function (error) {
                    handleError(error);
                });
        }

        $scope.newUser = function () {
            $location.path("/userEdit/0");
        };

        $scope.addQualifications = function (newQualification) {
            if (newQualification.id !== null) {
                $scope.viewModel.graduate.qualifications.push(newQualification);

                $scope.viewModel.marks.push({
                    qualificationId: newQualification.id,
                    GPA: newQualification.GPA,
                    qualification: newQualification
                });

                $scope.filteredQualifications = $scope.filteredQualifications.filter(function (el) {
                    return el !== newQualification;
                });
             }
            //for (var i = 0; i < $scope.filteredQualifications.length; i++) {
            //    if ($scope.filteredQualifications[i].id === $scope.qualificationId) {
            //        $scope.viewModel.graduate.qualifications.add($scope.qualificationId[i]);
            //    }
            //}
        }

        $scope.filterQualifications = function () {
            if ($scope.viewModel.graduate.tertiaryId) {
                $scope.filteredQualifications = [];
                for (var i = 0; i < $scope.qualifications.length; i++) {
                    if ($scope.qualifications[i].id !== null) {

                        if ($scope.qualifications[i].faculty.campus.tertiaryId === $scope.viewModel.graduate.tertiaryId) {
                            $scope.filteredQualifications.push($scope.qualifications[i]);
                        }
                    }
                    else {
                        $scope.filteredQualifications.push($scope.qualifications[i]);
                    }
                }
            }
        }

        $scope.unLock = function (id) {

            UserService.UserUnlock(id).then(
                    function (result) {
                        loadGrid();
                    },
                    function (error) {
                        handleError(error);
                    });
        };

        $scope.sendPassword = function (id) {

            UserService.UserGet(id).then(
                function (result) {

                    $scope.viewModel = result.data;

                    AccountService.SendPassword($scope.viewModel);

                    $scope.handleSuccess = function () { };

                    $timeout(function () {

                    }, 2000);
                },

                function (error) {
                    handleError(error);
                });
        };

        $scope.saveUserForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.UserRoleForm.$invalid)
                return;
            if ($scope.viewModel.recruiter == null) {
                $scope.viewModel.recruiter = null;
            }
            else if ($scope.viewModel.recruiter.companyId === null) {
                $scope.viewModel.recruiter = null;
            } else
                $scope.viewModel.graduate = null;

            if ($scope.viewModel.type == 'Admin') {
                $scope.viewModel.roleList[0].selected = true;
            }

            if ($scope.viewModel.lockedOut === true)
                $scope.viewModel.lockedOut = new Date().toJSON().slice(0, 10).replace("/-/g,'/'");
            else
                $scope.viewModel.lockedOut = null;

            var result = UserService.UserSave($scope.viewModel);

            result.then(function (result) {
                $scope.handleSuccess = function () { };
                $timeout(function () { $window.history.back(); }, 2000);
            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        };

        var loadGrid = function () {
            UserService.UserGrid($scope.currentPage, $scope.recordsPerPage,
                                      $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                function (result) {
                    $scope.userGrid = result.data.results;
                    $scope.totalItems = result.data.recordCount;
                },
                function (error) {
                    alert("an error occured: unable to get data");
                });
        };

        $scope.sortKeyOrder = {
            key: 'userName',
            order: 'ASC'
        };

        $scope.data = UserService.UserList;
        $scope.totalItems = 0;
        $scope.currentPage = 1;
        $scope.maxSize = 5;
        $scope.recordsPerPage = 20;
        $scope.numberOfPageButtons = 5;

        $scope.sort = function (col) {
            if ($scope.sortKeyOrder.key === col) {
                if ($scope.sortKeyOrder.order == 'ASC')
                    $scope.sortKeyOrder.order = 'DESC';
                else
                    $scope.sortKeyOrder.order = 'ASC';
            } else {
                $scope.sortKeyOrder.key = col;
                $scope.sortKeyOrder.order = 'ASC';
            }
            loadGrid();
        };

        $scope.searchFor = '';
        $scope.search = function () {
            loadGrid();
        };

        $scope.print = function (layer) {
            var generator = window.open("User Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }

        $scope.pageChanged = function () {
            loadGrid();
        };
        loadGrid();
    }
]);