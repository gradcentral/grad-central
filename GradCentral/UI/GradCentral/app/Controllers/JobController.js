﻿AngularApp.controller('JobController',
[
    '$scope', '$location', '$window', '$timeout', '$routeParams', 'JobService', 'EnumsService', 'CompanyService', 'JobRoleService', 'RecruiterService', 'QualificationService', 'Security', 
    function ($scope, $location, $window, $timeout, $routeParams, JobService, EnumsService, CompanyService, JobRoleService, RecruiterService, QualificationService, Security) {

        $scope.viewModel = {
            recruiter: {},
            jobRole: {}
        };

        $scope.currentUser = Security.currentUser;

        $scope.recruiterList = [];
        $scope.jobRoleList = [];
        $scope.genderTypes = [];
        $scope.genderOptions = [];
        $scope.raceTypes = [];
        $scope.raceOptions = [];
        $scope.positionTypes = [];
        $scope.positionOptions = [];
        $scope.qualificationList = [];

        $scope.viewModel.id = $routeParams.id;

        var loadRaceTypes = function () {
            if ($scope.raceOptions.length === 0)
                $scope.raceTypes = EnumsService.raceType();

            $scope.raceOptions = $scope.raceTypes;
            if ($scope.viewModel.id === 0)
                $scope.raceOptions.unshift({ 'value': null, 'description': 'Select race' });
        }
        var loadPositionTypes = function () {
            if ($scope.positionOptions.length == 0)
                $scope.positionTypes = EnumsService.positionType();

            $scope.positionOptions = $scope.positionTypes;
            if ($scope.viewModel.id == 0)
                $scope.positionOptions.unshift({ 'value': null, 'description': 'Select position' });
        }
        var loadGenderTypes = function () {
            if ($scope.genderOptions.length === 0)
                $scope.genderTypes = EnumsService.genderType();

            $scope.genderOptions = $scope.genderTypes;
            if ($scope.viewModel.id == 0)
                $scope.genderOptions.unshift({ 'value': null, 'description': 'Select gender' });
        }

        var getRecruiters = function () {

            RecruiterService.RecruiterList().then(
                CompanyService.CompanyList().then(
                    function (result) {
                        $scope.recruiterList = result.data;
                        $scope.recruiterList.unshift({ 'id': undefined, 'name': 'Select recruiter' });
                    },
                    function (error) {
                        alert("an error occured: unable to get recruiter data");
                    }
            )
            );
        }

        var getQualifications = function () {
            QualificationService.QualificationList().then(
                function (result) {
                    $scope.qualificationList = result.data;
                    $scope.qualificationList.unshift({ 'id': undefined, 'name': 'Select qualification' });
                },
                function (error) {
                    alert("an error occured: unable to get data");

                });
        };
        var getJobRoles= function () {
            JobRoleService.JobRoleList().then(
                function (result) {
                    $scope.jobRoleList = result.data;
                    $scope.jobRoleList.unshift({ 'id': undefined, 'name': 'Select job role' });
                },
                function (error) {
                    alert("an error occured: unable to get job role data");
                });
        }
        
        if ($scope.viewModel.id > 0) {
            loadGenderTypes();
            loadRaceTypes();
            loadPositionTypes();
            JobService.JobGet($scope.viewModel.id).then(
               function (result) {
                   $scope.viewModel = result.data;
               },
               function (error) {
                   handleError(error);
               });
            getQualifications();
            getRecruiters();
            getJobRoles();
        }
        else if ($scope.viewModel.id === 0) {
            loadGenderTypes();
            loadRaceTypes();
            loadPositionTypes();

            getRecruiters();
            getJobRoles();
            getQualifications();
        }
        else {
            $scope.sortKeyOrder = {
                key: 'Name',
                order: 'ASC'
            };

            $scope.totalItems = 0;
            $scope.currentPage = 1;
            $scope.maxSize = 5;
            $scope.recordsPerPage = 20;
            $scope.numberOfPageButtons = 5;

            $scope.sort = function (col) {
                if ($scope.sortKeyOrder.key === col) {
                    if ($scope.sortKeyOrder.order === 'ASC')
                        $scope.sortKeyOrder.order = 'DESC';
                    else
                        $scope.sortKeyOrder.order = 'ASC';
                } else {
                    $scope.sortKeyOrder.key = col;
                    $scope.sortKeyOrder.order = 'ASC';
                }
                loadGrid();
            };

            $scope.searchFor = '';
            $scope.search = function () {
                loadGrid();
            }

            var loadGrid = function () {
                var searchFor = '';

                JobService.JobGrid($scope.currentPage, $scope.recordsPerPage,
                                          $scope.sortKeyOrder.key, $scope.sortKeyOrder.order, $scope.searchFor).then(
                    function (result) {
                        $scope.JobGrid = result.data.results;
                        $scope.totalItems = result.data.recordCount;
                    },
                    function (error) {
                        alert("an error occured: unable to get job data");
                    });
            };
            $scope.pageChanged = function () {
                loadGrid();
            };
            loadGrid();
        }
        $scope.print = function (layer) {
            var generator = window.open("Job Report");
            var layerText = document.getElementById(layer);
            generator.document.write('<html><body onload="window.print()">' + layerText.innerHTML + '</body></html>');

            generator.document.close();
        }

        $scope.successMessage = "Saved Successfully";

        $scope.submitForm = function () {
            $scope.$broadcast('show-errors-check-validity');

            if ($scope.NewJobForm.$invalid)
                return;

            sessionStorage.setItem("qualification", JSON.stringify($scope.viewModel.qualification));
            sessionStorage.setItem("isDisable", $scope.viewModel.disability);
            sessionStorage.setItem("gpa", $scope.viewModel.gpa);
            sessionStorage.setItem("race", $scope.viewModel.race);
            sessionStorage.setItem("gender", $scope.viewModel.gender);
            sessionStorage.setItem("position", $scope.viewModel.position);

            $scope.viewModel.recruiterId = 1;
            var result = JobService.JobSave($scope.viewModel);  

            result.then(function (result) {
                sessionStorage.setItem("job", JSON.stringify(result.data));
                $window.location.href = 'http://'+$window.location.host +'/#/PossibleCandidate';

            },
            function (error) {
                $scope.hasError = true;
                $scope.errorMessage = error.data.message;
            });
        };

        $scope.resetForm = function () {
            $scope.$broadcast('show-errors-reset');
        }

        $scope.cancelForm = function () {
            $scope.$broadcast('show-errors-reset');
            $window.history.back();
        }

        $scope.newJob = function () {
            $location.path("/JobEdit/0");
        };

        var handleError = function (error) {
            $scope.hasError = true;
            $scope.errorMessage = error.data.message;
        };
    }
]);