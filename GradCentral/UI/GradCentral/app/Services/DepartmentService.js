﻿AngularApp.factory('DepartmentService',
    [
        '$http',
        function ($http) {

            var DepartmentSave = function (viewModel) {

                return $http.post("/api/Department/SaveDepartment", viewModel);
            }


            var DepartmentList = function () {
                return $http.get("api/Department/DepartmentList");
            };

            var DepartmentGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
                var req = {
                    CurrentPage: currentPage,
                    RecordsPerPage: recordsPerPage,
                    SortKey: sortKey,
                    SortOrder: sortOrder,
                    Searchfor: searchfor
                }
                return $http.post("api/Department/DepartmentGrid", req);
            }

            var DepartmentGet = function (DepartmentId) {
                return $http.get("api/Department/DepartmentGet/" + DepartmentId);
            }

            return {
                DepartmentSave: DepartmentSave,
                DepartmentList: DepartmentList,
                DepartmentGrid: DepartmentGrid,
                DepartmentGet: DepartmentGet
            }
        }
    ]);