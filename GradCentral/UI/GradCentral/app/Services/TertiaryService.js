﻿AngularApp.factory('TertiaryService',
[
    '$http',
    function ($http) {

        var TertiarySave = function(viewModel) {

            return $http.post("/api/Tertiary/SaveTertiary", viewModel);
        }


        var TertiaryList = function() {
            return $http.get("api/Tertiary/TertiaryList");
        };

        var TertiaryGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Tertiary/TertiaryGrid", req);
        }

        var TertiaryGet = function (TertiaryId) {
            return $http.get("api/Tertiary/TertiaryGet/" + TertiaryId);
        }

        return {
            TertiarySave: TertiarySave,
            TertiaryList: TertiaryList,
            TertiaryGrid: TertiaryGrid,
            TertiaryGet: TertiaryGet
        }
    }
]);