﻿AngularApp.factory('UserService',
    ['$http',
    function ($http) {


        var _userSave = function (viewModel) {
            var roleList = [];
            for (x = 0; x < viewModel.roleList.length; x++) {
                var role;
                if (viewModel.roleList[x].selected) {
                    if (viewModel.roleList[x].roleId !== undefined) {
                        role = {
                            RoleId: viewModel.roleList[x].roleId,
                            RoleName: viewModel.roleList[x].roleName,
                            Selected: viewModel.roleList[x].selected
                        }
                    } else {
                        role = {
                            RoleId: viewModel.roleList[x].id,
                            RoleName: viewModel.roleList[x].roleName,
                            Selected: viewModel.roleList[x].selected
                        }
                    }

                    roleList.push(role);
                }
            }
            viewModel.roleList = [];
            viewModel.roleList = roleList;
            return $http.post("/api/User/UserSave", viewModel);
        }

        var _userList = function () {
            return $http.get("/api/User/UserList");
        }

        var _userGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("/api/User/UserGrid", req);
        }

        var _userGet = function (userId) {
            return $http.get("/api/User/UserGet/" + userId);
        }

        var _userUnlock = function (lockedUserId) {
            return $http.get("/api/User/UserUnlock/" + lockedUserId);
        }

        return {

            UserSave: _userSave,
            UserUnlock: _userUnlock,
            UserGrid: _userGrid,
            UserGet: _userGet,
            UserList: _userList

        }
    }]);