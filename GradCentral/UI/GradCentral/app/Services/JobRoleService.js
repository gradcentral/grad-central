AngularApp.factory('JobRoleService',
[
    '$http',
    function ($http) {

        var JobRoleSave = function(viewModel) {

            return $http.post("/api/JobRole/SaveJobRole", viewModel);
        }


        var JobRoleList = function() {
            return $http.get("api/JobRole/JobRoleList");
        };

        var JobRoleGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/JobRole/JobRoleGrid", req);
        }

        var JobRoleGet = function (JobRoleId) {
            return $http.get("api/JobRole/JobRoleGet/" + JobRoleId);
        }

        return {
            JobRoleSave: JobRoleSave,
            JobRoleList: JobRoleList,
            JobRoleGrid: JobRoleGrid,
            JobRoleGet: JobRoleGet
        }
    }
]);