﻿AngularApp.factory('ModuleService',
[
    '$http',
    function ($http) {

        var ModuleSave = function (viewModel) {

            return $http.post("/api/Module/SaveModule", viewModel);
        };


        var ModuleList = function () {
            return $http.get("api/Module/ModuleList");
        };

        var ModuleGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            };
            return $http.post("api/Module/ModuleGrid", req);
        };

        var ModuleGet = function (ModuleId) {
            return $http.get("api/Module/ModuleGet/" + ModuleId);
        };

        return {
            ModuleSave: ModuleSave,
            ModuleList: ModuleList,
            ModuleGrid: ModuleGrid,
            ModuleGet: ModuleGet
        };
    }
]);