﻿AngularApp.factory('JobService',
[
    '$http',
    function ($http) {

        var JobSave = function (viewModel) {

            return $http.post("/api/Job/SaveJob", viewModel);
        };

        var JobList = function () {
            return $http.get("api/Job/JobList");
        };

        var JobGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            };
            return $http.post("api/Job/JobGrid", req);
        };

        var JobGet = function (JobId) {
            return $http.get("api/Job/JobGet/" + JobId);
        };

        return {
            JobSave: JobSave,
            JobList: JobList,
            JobGrid: JobGrid,
            JobGet: JobGet
        };
    }
]);