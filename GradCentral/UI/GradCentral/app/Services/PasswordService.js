﻿AngularApp.factory('PasswordService',
    ['$http',
    function ($http) {

        
        var sendPassword = function (viewModel) {
            return $http.post("/api/ServiceProvider/SendPassword", viewModel);
        };

        return {
            SendPassword: sendPassword
            
        };
    }]);