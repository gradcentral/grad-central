﻿AngularApp.factory('RoleService',
[
    '$http',
    function ($http) {

        var roleSave = function(viewModel) {
            var permissions = [];

            for (x = 0; x < viewModel.permissions.length; x++) {

                if (viewModel.permissions[x].selected) {
                    var permission = {
                        Privilege: viewModel.permissions[x].privilege,
                        Selected: viewModel.permissions[x].selected
                    }

                    permissions.push(permission);
                }
            }

            viewModel.permissions = [];
            viewModel.permissions = permissions;

            return $http.post("/api/Role/SaveRole", viewModel);
        }


        var roleList = function() {
            return $http.get("api/Role/RoleList");
        };

        var roleGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Role/RoleGrid", req);
        }

        var roleGet = function (roleId) {
            return $http.get("api/Role/RoleGet/" + roleId);
        }

        return {
            RoleSave: roleSave,
            RoleList: roleList,
            RoleGrid: roleGrid,
            RoleGet: roleGet
        }
    }
]);