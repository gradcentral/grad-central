﻿AngularApp.factory('ProfileService',
[
    '$http',
    function($http) {

        var profileSave = function (viewModel) {

            return $http.post("/api/Profile/EditProfile", viewModel);
        }

        var profileGet = function () {

            return $http.get("/api/Profile/GetMyProfile");
        }

        return {
            ProfileSave: profileSave,
            ProfileGet: profileGet
        }
    }
]);