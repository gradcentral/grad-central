AngularApp.factory('CompanyService',
[
    '$http',
    function ($http) {

        var CompanySave = function(viewModel) {

            return $http.post("/api/Company/SaveCompany", viewModel);
        }


        var CompanyList = function() {
            return $http.get("api/Company/CompanyList");
        };

        var CompanyGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Company/CompanyGrid", req);
        }

        var CompanyGet = function (CompanyId) {
            return $http.get("api/Company/CompanyGet/" + CompanyId);
        }

        return {
            CompanySave: CompanySave,
            CompanyList: CompanyList,
            CompanyGrid: CompanyGrid,
            CompanyGet: CompanyGet
        }
    }
]);