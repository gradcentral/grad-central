AngularApp.factory('GraduateService',
[
    '$http',
    function ($http) {

        var GraduateSave = function(viewModel) {

            return $http.post("/api/Graduate/SaveGraduate", viewModel);
        }


        var GraduateList = function() {
            return $http.get("api/Graduate/GraduateList");
        };

         var MarksList = function() {
             return $http.get("api/Graduate/GetMarks");
        };

        var JobCandidates = function(jobId){
            return $http.get("api/Graduate/GetJobCandidates/" + jobId);
        };

        var GraduateGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Graduate/GraduateGrid", req);
        }

        var PossibleCandidatesGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor, race, gpa, isDisable, gender, qualificationId) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor,
                Race: race,
                Gender: gender,
                GPA: gpa,
                IsDisable: isDisable,
                QualificationId: qualificationId
            }
            return $http.post("api/Graduate/PossibleCandidatesGrid", req);
        }

        var GraduateGet = function (GraduateId) {
            return $http.get("api/Graduate/GraduateGet/" + GraduateId);
        }

        return {
            GraduateSave: GraduateSave,
            GraduateList: GraduateList,
            GraduateGrid: GraduateGrid,
            GraduateGet: GraduateGet,
            PossibleCandidatesGrid: PossibleCandidatesGrid,
            JobCandidates: JobCandidates,
            MarksList: MarksList
        }
    }
]);