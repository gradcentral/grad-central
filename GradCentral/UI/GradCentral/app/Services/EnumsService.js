﻿AngularApp.factory('EnumsService',
   ['$http', '$rootScope', function ($http, $rootScope) {

       // Models/Variables----------------------------------------------M-

       var _statusType = [];
       var _userType = [];
       var _securityType = [];

       var _provinceType = [];
       var _tertiaryType = [];

       var _raceType = [];
       var _genderType = [];
       var _titleType = [];
       var _positionType = [];
       var _semesterType = [];
       var _attachmentType = [];
       var _qualificationType = [];
       //var _countries = [];

       var _enumsLoadedEvent = 'enums-loaded';
       var _allEnumsLoaded = false;

       // Functions ----------------------------------------------------F-

       //_QualificationType -Start

       var loadQualificationType = function () {
           return $http.get("api/Enums/QualificationTypeEnum").then(function (result) {
               _qualificationType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getQualificationTypes = function () {
           return _qualificationType;
       }
       //_QualificationType -End


       //_genderType -Start
       var loadGenderType = function () {
           return $http.get("api/Enums/genderTypeEnum").then(function (result) {
               _genderType = result.data;
               checkAllEnumsLoaded();
           });
       };

       getGenderTypes = function () {
           return _genderType;
       };
       //_genderType -End

       //_positionType -Start
       var loadPositionType = function () {
           return $http.get("api/Enums/positionTypeEnum").then(function (result) {
               _positionType = result.data;
               checkAllEnumsLoaded();
           });
       };

       getPositionTypes = function () {
           return _positionType;
       };
       //_positionType -End


       //_raceType -Start

       var loadRaceType = function () {
           return $http.get("api/Enums/raceTypeEnum").then(function (result) {
               _raceType = result.data;
               checkAllEnumsLoaded();
           });
       };

       var getRaceTypes = function () {
           return _raceType;
       }
       //_raceType -End
       var getStatusTypes = function () {
           return _statusType;
       }



       //_titleType -Start
       var loadTitleType = function () {
           return $http.get("api/Enums/titleTypeEnum").then(function (result) {
               _titleType = result.data;
               checkAllEnumsLoaded();
           });
       };

       getTitleTypes = function () {
           return _titleType;
       };
       //_titleType -End


       //_statusType -Start

       var loadStatusType = function () {
           return $http.get("api/Enums/StatusTypeEnum").then(function (result) {
               _statusType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getStatusTypes = function () {
           return _statusType;
       }
       //_statusType -End



       //_userType -Start
       var loadUserType = function () {
           return $http.get("api/Enums/UserTypeEnum").then(function (result) {
               _userType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getUserTypes = function () {
           return _userType;
       }
       //_userType -End

       //_securityType -Start
       var loadSecurityType = function () {
           return $http.get("api/Enums/SecurityEnum").then(function (result) {
               _securityType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getSecurityTypes = function () {
           return _securityType;
       }
       //_securityType -End





       //_provinceType -Start
       var loadProvinceType = function () {
           return $http.get("api/Enums/ProvinceTypeEnum").then(function (result) {
               _provinceType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getProvinceTypes = function () {
           return _provinceType;
       }
       //_provinceType -End

       //_tertiaryType -Start
       var loadTertiaryType = function () {
           return $http.get("api/Enums/TertiaryTypeEnum").then(function (result) {
               _tertiaryType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getTertiaryTypes = function () {
           return _tertiaryType;
       }
       //_tertiaryType -End

       //_semesterType -Start
       var loadSemesterType = function () {
           return $http.get("api/Enums/SemesterTypeEnum").then(function (result) {
               _semesterType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getSemesterTypes = function () {
           return _semesterType;
       }
       //_tertiaryType -End


       //_attachmentType -Start
       var loadAttachmentFormatType = function () {
           return $http.get("api/Enums/AttachmentFormatTypeEnum").then(function (result) {
               _attachmentType = result.data;
               checkAllEnumsLoaded();
           });
       };
       var getAttachmentFormatTypes = function () {
           return _attachmentType;
       }
       //_attachmentType -End



       var checkAllEnumsLoaded = function () {
           if (_statusType.length > 0
                && _securityType.length > 0
                && _qualificationType.length > 0
               && _provinceType.length > 0
               && _semesterType.length > 0
               && _tertiaryType.length > 0
               && _attachmentType.length > 0
               && _userType.length > 0
               && _raceType.length > 0
               && _genderType.length > 0
               && _positionType.length > 0
               && _titleType.length > 0) {
               _allEnumsLoaded = true;
               $rootScope.$emit(_enumsLoadedEvent, _allEnumsLoaded);
           }
       };
       var _getAllEnumsLoaded = function () {
           return _allEnumsLoaded;
       }

       // Executing Actions -------------------------------------------EA-

       loadStatusType();
       loadUserType();
       loadSecurityType();
       loadProvinceType();
       loadTertiaryType();
       loadQualificationType();
       loadSemesterType();
       loadAttachmentFormatType();
       loadTitleType();
       loadGenderType();
       loadRaceType();
       loadPositionType();

       // Angular Updates & Actions ----------------------------------AUA-


       // Return Result ------------------------------------------------R-

       return {
           statusTypes: getStatusTypes,
           //countries: getCountries,
           provinceTypes: getProvinceTypes,
           tertiaryTypes: getTertiaryTypes,
           qualificationTypes: getQualificationTypes,
           semesterTypes: getSemesterTypes,
           attachmentTypes: getAttachmentFormatTypes,
           securityTypes: getSecurityTypes,
           userTypes: getUserTypes,
           enumsLoadedEvent: _enumsLoadedEvent,
           enumsLoaded: _getAllEnumsLoaded,
           titleType: getTitleTypes,
           raceType: getRaceTypes,
           genderType: getGenderTypes,
           positionType: getPositionTypes
       }
   }
   ]);