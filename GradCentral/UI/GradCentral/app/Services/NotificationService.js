﻿AngularApp.factory('NotificationService',
    [
        '$http',
        function ($http) {

            var NotificationSave = function (viewModel) {

                return $http.post("/api/Notification/SaveNotification", viewModel);
            }

            var NotifyCandidates = function (viewModel) {
                return $http.post("/api/Notification/NotifyCandidates", viewModel);
            }


            var NotificationList = function () {
                return $http.get("api/Notification/NotificationList");
            };

            var GetUserNotifications = function () {
                return $http.get("api/Notification/GetUserNotifications");
            };

            var NotificationGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
                var req = {
                    CurrentPage: currentPage,
                    RecordsPerPage: recordsPerPage,
                    SortKey: sortKey,
                    SortOrder: sortOrder,
                    Searchfor: searchfor
                }
                return $http.post("api/Notification/NotificationGrid", req);
            }

            var NotificationGet = function (NotificationId) {
                return $http.get("api/Notification/NotificationGet/" + NotificationId);
            }

            return {
                NotificationSave: NotificationSave,
                NotificationList: NotificationList,
                NotificationGrid: NotificationGrid,
                NotificationGet: NotificationGet,
                GetUserNotifications: GetUserNotifications,
                NotifyCandidates: NotifyCandidates
            }
        }
    ]);