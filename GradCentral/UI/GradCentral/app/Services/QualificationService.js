﻿AngularApp.factory('QualificationService',
[
    '$http',
    function ($http) {

        var QualificationSave = function (viewModel) {

            return $http.post("/api/Qualification/SaveQualification", viewModel);
        }


        var QualificationList = function () {
            return $http.get("api/Qualification/QualificationList");
        };

        var QualificationGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Qualification/QualificationGrid", req);
        }

        var QualificationGet = function (QualificationId) {
            return $http.get("api/Qualification/QualificationGet/" + QualificationId);
        }

        return {
            QualificationSave: QualificationSave,
            QualificationList: QualificationList,
            QualificationGrid: QualificationGrid,
            QualificationGet: QualificationGet
        }
    }
]);