﻿AngularApp.factory('CampusService',
[
    '$http',
    function ($http) {

        var CampusSave = function(viewModel) {

            return $http.post("/api/Campus/SaveCampus", viewModel);
        }


        var CampusList = function() {
            return $http.get("api/Campus/CampusList");
        };

        var CampusGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Campus/CampusGrid", req);
        }

        var CampusGet = function (CampusId) {
            return $http.get("api/Campus/CampusGet/" + CampusId);
        }

        return {
            CampusSave: CampusSave,
            CampusList: CampusList,
            CampusGrid: CampusGrid,
            CampusGet: CampusGet
        }
    }
]);