AngularApp.factory('FacultyService',
[
    '$http',
    function ($http) {

        var FacultySave = function(viewModel) {

            return $http.post("/api/Faculty/SaveFaculty", viewModel);
        }


        var FacultyList = function() {
            return $http.get("api/Faculty/FacultyList");
        };

        var FacultyGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
            var req = {
                CurrentPage: currentPage,
                RecordsPerPage: recordsPerPage,
                SortKey: sortKey,
                SortOrder: sortOrder,
                Searchfor: searchfor
            }
            return $http.post("api/Faculty/FacultyGrid", req);
        }

        var FacultyGet = function (FacultyId) {
            return $http.get("api/Faculty/FacultyGet/" + FacultyId);
        }

        return {
            FacultySave: FacultySave,
            FacultyList: FacultyList,
            FacultyGrid: FacultyGrid,
            FacultyGet: FacultyGet
        }
    }
]);