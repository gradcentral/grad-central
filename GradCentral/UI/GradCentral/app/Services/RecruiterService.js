﻿AngularApp.factory('RecruiterService',
    [
        '$http',
        function ($http) {

            var RecruiterSave = function(viewModel) {

                return $http.post("/api/Recruiter/SaveRecruiter", viewModel);
            }


            var RecruiterList = function() {
                return $http.get("api/Recruiter/RecruiterList");
            };

            var RecruiterGrid = function (currentPage, recordsPerPage, sortKey, sortOrder, searchfor) {
                var req = {
                    CurrentPage: currentPage,
                    RecordsPerPage: recordsPerPage,
                    SortKey: sortKey,
                    SortOrder: sortOrder,
                    Searchfor: searchfor
                }
                return $http.post("api/Recruiter/RecruiterGrid", req);
            }

            var RecruiterGet = function (RecruiterId) {
                return $http.get("api/Recruiter/RecruiterGet/" + RecruiterId);
            }

            return {
                RecruiterSave: RecruiterSave,
                RecruiterList: RecruiterList,
                RecruiterGrid: RecruiterGrid,
                RecruiterGet: RecruiterGet
            }
        }
    ]);