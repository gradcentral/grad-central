﻿AngularApp.factory('AccountService',
    ['$http',
    function ($http) {

        // Models -------------------------------------------------------M-


        // Functions ----------------------------------------------------F-

        var register = function (viewModel) {
            return $http.post("/api/Account/Register", viewModel);
        };

        var login = function (viewModel) {
            return $http.post("/api/Account/Login", viewModel);
        };

        var addFile = function (viewModel) {
            return $http.post("/api/Account/AddFile", viewModel);
        };


        var getServiceProviderId = function () {
            return $http.get("/api/Account/GetServiceProviderId");
        };
      

        var currentUser = function () {
            return $http.get('/api/Account/GetCurrentUser');
        }

        var singOut = function () {
            return $http.post('/api/Account/SignOut/1',{});
        }

        var _changePassword = function (viewModel) {
            return $http.post("/api/Account/ChangePassword", viewModel);
        };

        var _sendPassword = function (viewModel) {
            return $http.post("/api/Account/SendPassword", viewModel);
        };

        // Executing Actions -------------------------------------------EA-


        // Angular Updates & Actions ----------------------------------AUA-


        // Return Result ------------------------------------------------R-

        return {
            login: login,
            register: register,
            currentUser: currentUser,
            singOut: singOut,
            ChangePassword: _changePassword,
            SendPassword: _sendPassword,
            GetServiceProviderId : getServiceProviderId,
            addFile: addFile
        }
    }

    ]);

