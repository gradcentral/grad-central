﻿namespace GradCentral.Models.Enums
{
    public class CountryModel
    {
        public int Id { get; set; }
        public string CountryName { get; set; }
    }
}