﻿namespace GradCentral.Models.Enums
{
    public class EnumModel
    {

        public string Value { get; set; }

        public string Description { get; set; }
    }
}