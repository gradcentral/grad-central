﻿using GradCentral.BL.Entities.SecurityData;

namespace GradCentral.Models.Enums
{
    public class SecurityTypeModel
    {
        private GradCentral.BL.Entities.SecurityData.PrivilegeType _priv;

        public SecurityTypeModel(GradCentral.BL.Entities.SecurityData.PrivilegeType priv)
        {
            _priv = priv;
        }

        public int OrdinalValue { get { return (int)_priv; } }

        public string Name
        {
            get { return _priv.ToString(); }
        }
    }
}