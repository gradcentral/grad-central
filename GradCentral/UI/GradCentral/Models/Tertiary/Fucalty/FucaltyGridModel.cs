using System.Collections.Generic;
using GradCentral.BL.Entities.QualificationData;

namespace GradCentral.Models.Tertiary.Campus
{
    public class FacultyGridModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CampusId { get; set; }

        public BL.Entities.TertiaryData.Campus.Campus Campus { get; set; }
        
        public ICollection<Qualification> Qualifications { get; set; }
    }
}