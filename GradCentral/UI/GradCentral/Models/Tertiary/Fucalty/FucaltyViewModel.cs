

using System.Collections.Generic;

namespace GradCentral.Models.Faculty
{
    public class FacultyViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long CampusId { get; set; }
        
        public BL.Entities.TertiaryData.Campus.Campus Campus { get; set; }
        
        public ICollection<BL.Entities.QualificationData.Qualification> Qualifications { get; set; }
    }
}