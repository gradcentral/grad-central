﻿

using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.TertiaryData.Faculty;
using GradCentral.BL.Entities.Types;
using System.Collections.Generic;
using GradCentral.Models.Faculty;

namespace GradCentral.Models.Tertiary
{
    public class TertiaryViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public TertiaryTypes TertiaryType { get; set; }
        public ProvinceType Province { get; set; }
        public ICollection<Qualification> Qualifications { get; internal set; }
        public ICollection<BL.Entities.GraduateData.Graduate> Graduates { get; internal set; }
        public ICollection<FacultyViewModel> Faculties { get; internal set; }

    }
}