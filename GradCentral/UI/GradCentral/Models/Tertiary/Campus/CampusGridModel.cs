﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;
using System.Collections;
using System.Collections.Generic;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.TertiaryData.Faculty;

namespace GradCentral.Models.Tertiary.Campus
{
    public class CampusGridModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public int Zip { get; set; }
        public long TertiaryId { get; set; }
        public TertiaryViewModel Tertiary { get; set; }
        //public List<TertiaryViewModel> Qualifications { get; set; }
    }
}