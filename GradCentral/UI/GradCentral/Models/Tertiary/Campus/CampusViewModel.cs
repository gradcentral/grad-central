﻿

namespace GradCentral.Models.Tertiary
{
    public class CampusViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Street { get; set; }
        public string Number { get; set; }
        public string Town { get; set; }
        public string City { get; set; }
        public string Province { get; set; }
        public int Zip { get; set; }
        public long TertiaryId { get; set; }
        public TertiaryViewModel Tertiary { get; set; }
        //public List<FacultyViewMode> Qualifications { get; set; }
    }
}