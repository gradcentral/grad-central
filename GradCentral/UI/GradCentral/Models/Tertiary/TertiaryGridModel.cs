﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;
using System.Collections;
using System.Collections.Generic;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.TertiaryData.Faculty;

namespace GradCentral.Models.Tertiary
{
    public class TertiaryGridModel
    {
        public long? Id { get; set; }


        public string Name { get; set; }

        public TertiaryTypes TertiaryType { get; set; }
        public string Tertiary { get { return NameSplitting.SplitCamelCase(TertiaryType); } }

        public ProvinceType Provinces { get; set; }
        public string Province { get { return NameSplitting.SplitCamelCase(Provinces); } }

        public ICollection<Qualification> Qualifications { get; set; }

        public ICollection<Graduate> Graduates { get; set; }

        public ICollection<BL.Entities.TertiaryData.Faculty.Faculty> Faculties { get; set; }
    }
}