﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GradCentralUI.Models.SecurityData
{
    public class Graduate
    {
        public long Id { get; set; }
        public long UserIdentityId { get; set; }

        public long TertiaryId { get; set; }

        public long? JobId { get; set; }

        public bool IsEmployed { get; set; }

    }
}