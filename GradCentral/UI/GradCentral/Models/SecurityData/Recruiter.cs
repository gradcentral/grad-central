﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GradCentralUI.Models.SecurityData
{
    public class Recruiter
    {
        public long  Id { get; set; }

        public long UserIdentityId { get; set; }

        public long RecruitmentAgencyId { get; set; }
    }
}