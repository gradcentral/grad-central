﻿using System;
using System.Collections.Generic;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.Types;
using GradCentralUI.Models.SecurityData;
using GradCentral.BL.Entities.Marks;

namespace GradCentral.Models.SecurityData
{
    public class UserEditModel
    {
        public long? Id { get; set; }

        public string UserName { get; set; }

        public TitleType Title { get; set; }

        public string TitleString { get { return Title.ToString(); } }

        public GradCentral.BL.Entities.Types.UserType Type { get; set; }

        public DateTime? LockedOut { get; set; }

        public string TypeString
        {
            get { return Type.ToString(); }
        }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public string Password { get; set; }

        public string Email { get; set; }

        public GenderType Gender { get; set; }
        public string GenderString { get { return Gender.ToString(); } }
        public bool IsAdmin { get; set; }

        public bool IsDisable { get; set; }

        public string CellNumber { get; set; }

        public List<UserRoleModel> RoleList { get; set; }

        public Recruiter Recruiter { get; set; }

        public Graduate Graduate { get; set; }

        public ICollection<Mark> Marks { get; set; }


    }
}