﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GradCentral.BL.Entities;
using GradCentral.BL.Entities.JobData;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;

namespace GradCentralUI.Models.SecurityData
{
    public class RecruiterGridModel
    {
        public long  Id { get; set; }

        public long UserIdentityId { get; set; }

        public long CompanyId { get; set; }
        public virtual UserIdentity UserIdentity { get; set; }

        public virtual IdType IdType { get; set; }

        public ICollection<Event> Events { get; set; }

        public ICollection<GradCentral.BL.Entities.JobData.Job> Jobs { get; set; }
        public GradCentral.BL.Entities.CompanyData.Company Company { get; set; }
    }
}