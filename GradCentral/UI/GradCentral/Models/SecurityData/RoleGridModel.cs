﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;

namespace GradCentral.Models.SecurityData
{
    public class RoleGridModel
    {
        public long? Id { get; set; }

        public string RoleName { get; set; }

        public string Description { get; set; }

        public StatusType StatusTypes { get; set; }

        public string Status { get { return NameSplitting.SplitCamelCase(StatusTypes); } }
    }
}