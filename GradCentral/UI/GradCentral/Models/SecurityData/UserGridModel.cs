﻿using GradCentral.BL.Entities.Types;
using System;
using System.Collections.Generic;

namespace GradCentral.Models.SecurityData
{
    public class UserGridModel
    {
        public long? Id { get; set; }

        public string UserName { get; set; }

        public GradCentral.BL.Entities.Types.UserType Type { get; set; }

        public string TypeString
        {
            get { return Type.ToString();}
        }

        public DateTime? LockedOut { get; set; }

        public string FirstName { get; set; }

        public string Surname { get; set; }

        public GenderType Gender { get; set; }

        public string GenderString { get { return Gender.ToString(); } }

        public string Email { get; set; }

        public bool IsAdmin { get; set; }

        public List<UserRoleModel> RoleList { get; set; }
    }
}