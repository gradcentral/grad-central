﻿namespace GradCentral.Models.SecurityData
{
    public class UserIdentityViewModel
    {
        
            public virtual long Id { get; set; }

            public virtual string UserName { get; set; }

            public bool Active { get; set; }

            public virtual string Title { get; set; }

            public virtual string FirstName { get; set; }

            public virtual string Surname { get; set; }

            //public virtual string PasswordHash { get; set; }

            public string Email { get; set; }

            //public int FailedLoginAttempts { get; set; }

            //public DateTime? LockedOut { get; set; }

            //public DateTime? Deactivated { get; set; }

            //public DateTime? LastLogin { get; set; }

            //public bool IsSystemAdmin { get; set; }

            //public  ConsumerCheckListItem ConsumerCheckList { get; set; }

            //public  ICollection<RoleViewModel> Roles { get; set; }

            //public  ICollection<ServiceProviderViewModel> ServiceProviders { get; set; }

            //public List<PrivilegeType> AllowedPrivileges { get; set; }

        
    }
}