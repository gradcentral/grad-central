﻿using GradCentral.BL.Entities.Types;
using System.ComponentModel.DataAnnotations;
using GradCentral.BL.Entities.Types;

namespace GradCentral.Models.SecurityData
{
    public class ProfileViewModel
    {
        [Required]
        public string UserName { get; set; }
        [Required]
        public TitleType Title { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public string Email { get; set; }
    }
}
