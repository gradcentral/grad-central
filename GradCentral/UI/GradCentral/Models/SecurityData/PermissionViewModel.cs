﻿using System.ComponentModel.DataAnnotations;
using GradCentral.BL.Entities.SecurityData;
using GC.Lib.Utility;

namespace GradCentral.Models.SecurityData
{
    public class PermissionViewModel
    {
        public GradCentral.BL.Entities.SecurityData.PrivilegeType Privilege { get; set; }

        public string Description
        {
            get { return NameSplitting.SplitCamelCase(Privilege); }
        }

        [Display(Name = "Selected")]
        public bool Selected { get; set; }
    }
}