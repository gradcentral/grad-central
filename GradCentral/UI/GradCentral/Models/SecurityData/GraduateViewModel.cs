﻿using System.Collections.Generic;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.TertiaryData;
using GradCentral.BL.Entities.Types;

namespace GradCentralUI.Models.SecurityData
{
    public class GraduateViewModel
    {
        public long Id { get; set; }
        public long UserIdentityId { get; set; }
        public long TertiaryId { get; set; }
        public long? JobId { get; set; }

        public bool IsDisable { get; set; }

        public string DisablityDescription { get; set; }

        public RaceType Race { get; set; }

        public string RaceString { get { return Race.ToString(); } }

        public string CurrectEmployer { get; set; }

        public bool IsEmployed { get; set; }
        public bool IsNasfas { get; set; }
        public virtual UserIdentity UserIdentity { get; set; }
        public GradCentral.BL.Entities.JobData.Job Job { get; set; }
        public ICollection<Qualification> Qualifications { get; set; }
        public Tertiary Tertiary { get; set; }
    }
}