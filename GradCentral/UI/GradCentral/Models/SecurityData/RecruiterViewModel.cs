﻿using System.Collections.Generic;
using GradCentral.BL.Entities;
using GradCentral.BL.Entities.Types;
using GradCentral.Models.Company;
using GradCentral.Models.SecurityData;

namespace GradCentralUI.Models.SecurityData
{
    public class RecruiterViewModel
    {
        public long  Id { get; set; }

        public long UserIdentityId { get; set; }

        public long CompanyId { get; set; }
        public UserEditModel UserIdentity { get; set; }

        public virtual IdType IdType { get; set; }

        public ICollection<Event> Events { get; set; }

        public ICollection<GradCentral.BL.Entities.JobData.Job> Jobs { get; set; }
        public CompanyViewModel Company { get; set; }
    }
}