﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GradCentral.BL.Entities.Types;

namespace GradCentral.Models.SecurityData
{
    public class RoleViewModel
    {
        public long? Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string RoleName { get; set; }

        [Display(Name = "Description")]
        public string Description { get; set; }

        public bool Selected { get; set; }

        public GradCentral.BL.Entities.Types.StatusType StatusType { get; set; }

        public string Status
        {
            get
            {
                return StatusType.ToString();
            }
        }


        public List<PermissionViewModel> Permissions { get; set; }

    }
}