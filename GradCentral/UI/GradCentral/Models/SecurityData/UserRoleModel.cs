﻿using System.ComponentModel.DataAnnotations;

namespace GradCentral.Models.SecurityData
{
    public class UserRoleModel
    {
        public long RoleId { get; set; }

        public string RoleName { get; set; }

        [Display(Name = "Selected")]
        public bool Selected { get; set; }
    }
}