﻿using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GradCentralUI.Models
{
    public class CandidatesGridModel
    {
        public int? CurrentPage { get; set; }

        public int? RecordsPerPage { get; set; }

        public string SortKey { get; set; }

        public string SortOrder { get; set; }

        public string Searchfor { get; set; }

        public bool IsDisable { get; set; }

        public RaceType Race { get; set; }

        public int QualificationId { get; set; }

        public int GPA { get; set; }

        public GenderType Gender { get; set; }


    }
}