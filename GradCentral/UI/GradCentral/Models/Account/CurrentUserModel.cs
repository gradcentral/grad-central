﻿using System;
using System.Collections.Generic;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using GradCentral.BL.Provider.Security;

namespace GradCentral.Models.Account
{
    [Serializable]
    public class CurrentUserModel : ICurrentUser
    {
        private ICurrentUser _source;

        public CurrentUserModel(ICurrentUser source)
        {
            
            _source = source;
        }

        public long Id
        {
            get { return  _source.Id; }
        }

        public string UserName
        {
            get { return _source.UserName; }
        }

        public string DisplayName
        {
            get { return _source.DisplayName; }
        }

        public List<GradCentral.BL.Entities.SecurityData.PrivilegeType> AllowedPrivileges
        {
            get { return _source.AllowedPrivileges; }
        }

        public bool IsSystemAdmin
        {
            get { return _source.IsSystemAdmin; }
        }

        public string UserTypeString
        {
            get { return _source.UserTypeString; }
        }

        public GradCentral.BL.Entities.Types.UserType UserType
        {
            get { return _source.UserType; }
        }

        
    }
}