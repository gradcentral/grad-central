﻿using GradCentral.BL.Entities.GraduateData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GradCentralUI.Models.Notification
{
    public class GraduateNotification
    {
        public GradCentral.BL.Entities.JobData.Job Job { get; set; }
        public List<Graduate> Graduates { get; set; }
    }
}