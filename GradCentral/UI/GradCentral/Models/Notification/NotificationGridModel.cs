﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;
using System;
using GradCentral.BL.Entities.SecurityData;
using GradCentralUI.Models.Job;
using GradCentral.Models.SecurityData;
using System.Collections.Generic;
using GradCentral.BL.Entities.JobData;

namespace GradCentral.Models.NotificationData
{
    public class NotificationGridModel
    {

        public long Id { get; set; }
        public DateTime Date { get; set; }
        public DateTime ExpiryDate { get; set; }
        public DateTime DateCreated { get; set; }
        public string Message { get; set; }
        public long JobId { get; set; }
        public JobViewModel Job { get; set; }
        public UserIdentityViewModel User { get; set; }
        public long UseridentityId { get; set; }
        public bool Read { get; set; }
        public bool Confirm { get; set; }

        public ICollection<Job> Jobs { get; set; }


    }
}