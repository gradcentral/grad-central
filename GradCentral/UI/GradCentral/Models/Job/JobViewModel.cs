﻿
using GradCentral.BL.Entities.Types;
using GradCentral.Models.JobRole;
using GradCentral.Models.QualificationData;
using GradCentralUI.Models.SecurityData;
using System;

namespace GradCentralUI.Models.Job
{
    public class JobViewModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        //public DateTime Date { get; set; }
        //public DateTime ClosingDate { get; set; }
        public Boolean Disability { get; set; }
        public double GPA { get; set; }
        public GenderType Gender { get; set; }
        public RaceType Race { get; set; }
        public PositionTypes Position { get; set; }
        public long RecruiterId { get; set; }
        public RecruiterViewModel Recruiter { get; set; }
        public long JobRoleId { get; set; }
        public JobRoleViewModel JobRole { get; set; }
        public long QualificationId { get; set; }
        public QualificationViewModel Qualification { get; set; }
    }
}