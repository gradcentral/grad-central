﻿
using GC.Lib.Utility;
using GradCentral.BL.Entities.Types;
using GradCentral.Models.JobRole;
using GradCentral.Models.QualificationData;
using GradCentralUI.Models.SecurityData;
using System;

namespace GradCentralUI.Models.Job
{
    public class JobGridModel
    {
        public long Id { get; set; }
        public string Title { get; set; }
        //public DateTime Date { get; set; }
        //public DateTime ClosingDate { get; set; }
        public Boolean Disability { get; set; }
        public double GPA { get; set; }
        public GenderType GenderType { get; set; }
        public string Gender { get { return NameSplitting.SplitCamelCase(GenderType); } }
        public RaceType RaceType { get; set; }
        public string Race { get { return NameSplitting.SplitCamelCase(RaceType); } }
        public PositionTypes PositionType { get; set; }
        public string Position { get { return NameSplitting.SplitCamelCase(PositionType); } }

        public long RecruiterId { get; set; }
        public RecruiterViewModel Recruiter { get; set; }
        public long JobRoleId { get; set; }
        public JobRoleViewModel JobRole { get; set; }
        public long QualificationId { get; set; }
        public QualificationViewModel Qualification { get; set; }
    }
}