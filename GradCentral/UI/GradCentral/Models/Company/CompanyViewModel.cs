﻿using System.Collections.Generic;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.Types;
using GradCentral.Models.QualificationData;

namespace GradCentral.Models.Company
{
    public class CompanyViewModel
    {
        public  long Id { get; set; }

        public  string Number { get; set; }

        public  string Street { get; set; }

        public  string Town { get; set; }

        public  string City { get; set; }

        public  string Province { get; set; }

        public  int Zip { get; set; }

        public  string Name { get; set; }

        public ICollection<Recruiter> Recruiters { get; set; }
        public string Email { get; set; }
    }
}