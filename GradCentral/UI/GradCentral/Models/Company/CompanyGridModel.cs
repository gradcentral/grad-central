﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;
using System.Collections;
using System.Collections.Generic;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.Models.QualificationData;

namespace GradCentral.Models.Company
{
    public class CompanyGridModel
    {
        public long Id { get; set; }

        public string Number { get; set; }

        public string Street { get; set; }

        public string Town { get; set; }

        public string City { get; set; }

        public string Province { get; set; }

        public int Zip { get; set; }

        public string Name { get; set; }
        

        public ICollection<Recruiter> Recruiters { get; set; }
        public string Email { get; internal set; }
    }
}