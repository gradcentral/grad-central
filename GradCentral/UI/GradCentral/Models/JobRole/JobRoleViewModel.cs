﻿namespace GradCentral.Models.JobRole
{
    public class JobRoleViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        
    }
}