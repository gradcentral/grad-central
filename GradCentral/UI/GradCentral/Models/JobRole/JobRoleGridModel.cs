﻿namespace GradCentral.Models.JobRole
{
    public class JobRoleGridModel
    {
        public long Id { get; set; }
        
        public string Name { get; set; }
    }
}