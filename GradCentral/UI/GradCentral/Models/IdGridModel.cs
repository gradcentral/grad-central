﻿namespace GradCentral.Models
{
    public class IdGridModel
    {
        public int? Id { get; set; }
        public GridModel GridModel { get; set; }
    }
}