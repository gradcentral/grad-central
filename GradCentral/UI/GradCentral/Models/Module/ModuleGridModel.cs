﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;
using System.Collections;
using System.Collections.Generic;
using GradCentral.Models.QualificationData;

namespace GradCentral.Models.Module
{
    public class ModuleGridModel
    {
        public long Id { get; set; }
        
        public string Name { get; set; }

        public SemesterTypes SemesterTypes { get; set; }

        public string Semester { get { return NameSplitting.SplitCamelCase(SemesterTypes); } }

        public long QualificationId { get; set; }

        public QualificationViewModel Qualification { get; set; }
        
    }
}