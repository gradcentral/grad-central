﻿using System.Collections.Generic;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.Types;
using GradCentral.Models.QualificationData;

namespace GradCentral.Models.Module
{
    public class ModuleViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public SemesterTypes SemesterType  { get; set; }
        public long QualificationId { get; set; }
        public QualificationViewModel Qualification { get; set; }
        
    }
}