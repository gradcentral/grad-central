﻿using GradCentral.BL.Entities.Types;
using GC.Lib.Utility;
using System;

namespace GradCentral.Models.QualificationData
{
    public class QualificationGridModel
    {

        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public long FacultyId { get; set; }
        public QualificationType QualificationType { get; set; }
        public string Qualification { get { return NameSplitting.SplitCamelCase(QualificationType); } }
        public Faculty.FacultyViewModel Faculty { get; set; }

    }
}