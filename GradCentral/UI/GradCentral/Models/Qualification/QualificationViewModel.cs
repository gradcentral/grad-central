﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using GradCentral.BL.Entities.Types;
using System;
using GradCentral.Models.QualificationData;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.TertiaryData.Faculty;
using GradCentral.Models.SecurityData;
using GradCentral.BL.Entities.TertiaryData.Campus;

namespace GradCentral.Models.QualificationData
{
    public class QualificationViewModel
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public string Name { get; set; }
        public long FacultyId { get; set; }
        public QualificationType QualificationType { get; set; }
        public Faculty.FacultyViewModel Faculty { get; set; }


    }
}