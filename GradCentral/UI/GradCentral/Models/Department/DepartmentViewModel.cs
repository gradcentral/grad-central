﻿using GradCentral.Models.Faculty;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace GradCentral.Models.Department
{
    public class DepartmentViewModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long FacultyId { get; set; }
        public FacultyViewModel Faculty { get; set; }
    }
}