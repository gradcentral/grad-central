﻿using GradCentral.Models.Faculty;

namespace GradCentralUI.Models.Department
{
    public class DepartmentGridModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public long FacultyId { get; set; }
        public FacultyViewModel Faculty { get; set; }
    }
}