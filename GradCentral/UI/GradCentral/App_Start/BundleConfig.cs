﻿using System.Diagnostics;
using System.Web.Optimization;

namespace GradCentral
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                         "~/Content/bootstrap.css",
                           //"~/Content/app-theme.min.css",
                         "~/Content/site.css",
                         "~/Content/dist/css/AdminLTE.min.css",
                         "~/Content/dist/css/ionicons-2.0.1/css/ionicons.min.css",
                         "~/Content/dist/css/font-awesome-4.7.0/css/font-awesome.min.css",
                         "~/Content/loading-bar.css"));

            bundles.Add(new ScriptBundle("~/bundles/AngularApp")
                .Include("~/Scripts/angular.js")
                .Include("~/Scripts/angular-route.js")
                .Include("~/Scripts/angular-ui/ui-bootstrap-tpls.js")
                .Include("~/Scripts/angular-animate.js")


                .Include("~/Scripts/lib/angular-input-match.js")
                .Include("~/Scripts/lib/showErrors.js")
                .Include("~/Scripts/lib/loading-bar.js")

               .Include("~/Content/plugins/jQuery/jquery-2.2.3.min.js")
               .Include("~/Content/plugins/chartjs/Chart.js")


                .Include("~/app/AngularApp.js")
                .IncludeDirectory("~/app/Services", "*.js")
                .IncludeDirectory("~/app/Directives", "*.js")
                .IncludeDirectory("~/app/Controllers", "*.js")
                );

            BundleTable.EnableOptimizations = !Debugger.IsAttached;
        }
    }
}
