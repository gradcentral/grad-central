﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.MasterData;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Security;
using GradCentral.BL.Util;
using GC.Lib.Utility;
using GradCentral.BL.Entities.Types;

namespace GradCentral.BL.Test
{
    public static class SeedData
    {
        #region Security
        public static ICurrentUser CreateAdmin(DataContext context, string userName = "Admin", string password = "password@1")
        {
            var result = CreateUser(context, userName, password);
            var role = CreateRole(context);
            var currentUser = result.Roles.Where(a => a.RoleName == role.RoleName).SingleOrDefault();

            if (currentUser == null)
            {
                result.Roles.Add(role);
                context.SaveChanges();
            }

            var qPrivs = from userP in context.UserIdentitySet
                         from r in userP.Roles
                         from privileges in r.Privileges
                         where userP.Id == result.Id
                         select privileges.Security;
            result.AllowedPrivileges = qPrivs.Distinct().ToList();

            var provider = new SecurityProvider(context,null);
            return provider.UserIdentityToCurrentUser(result);
        }

        public static Role CreateRole(DataContext context, string roleName = "admin", bool hasAllPrivileged = true)
        {
            Role r = context.RoleSet.Include(a => a.Privileges).Where(a => a.RoleName == roleName).SingleOrDefault();
            if (r == null)
            {
                r = new Role()
                {
                    RoleName = roleName,
                    Description = "My Role Description",
                    Privileges = new List<Privilege>()
                };
                context.RoleSet.Add(r);
            }
            if (hasAllPrivileged)
            {
                foreach (PrivilegeType p in Enum.GetValues(typeof(PrivilegeType)))
                {
                    var priv = context.PrivilegeSet.Where(a => a.Security == p).SingleOrDefault();
                    if (priv == null)
                    {
                        priv = new Privilege()
                        {
                            Description = NameSplitting.SplitCamelCase(p),
                            Security = p
                        };
                        context.PrivilegeSet.Add(priv);
                    }
                    var curr = r.Privileges.Where(a => a.Security == priv.Security).SingleOrDefault();
                    if (curr == null)
                        r.Privileges.Add(priv);
                }
            }
            context.SaveChanges();

            return r;
        }

        public static UserIdentity CreateUser(DataContext context, string userName = "James", string password = "password@1", TitleType title = TitleType.Mr,
        string surname = "Bond", string firstName = "James",string email = "james@jamesbond.com")
        {
            try
            {
                var currentUser = context.UserIdentitySet
                                         .Include(a => a.Roles)
                                         .Where(a => a.UserName == userName).SingleOrDefault();

                if (currentUser == null)
                {
                    currentUser = new UserIdentity();
                    currentUser.Roles = new List<Role>();
                    context.UserIdentitySet.Add(currentUser);
                }

                currentUser.UserName = userName;
                currentUser.Title = title;
                currentUser.FirstName = firstName;
                currentUser.Surname = surname;
                currentUser.Email = email;
                currentUser.PasswordHash = Cipher.Encrypt(password);
                currentUser.Active = true;

                context.SaveChanges();
                return currentUser;
            }
            catch (DbEntityValidationException ex)
            {
                throw ex;
            }
        }

        public static List<Privilege>GetPrivileges(DataContext context)
        {
            var privileges = context.PrivilegeSet.OrderBy(p => p.Id).Take(2);

            return privileges.ToList();
        }

        public static List<long>GetRoles(DataContext context)
        {
            var roles = context.RoleSet.OrderBy(r => r.Id).Select(s => s.Id).Take(2);

            return roles.ToList();
        }
        #endregion

        #region MasterData

        public static Country CreateCountry(DataContext context, int? iSOCountryId= 4, string twoCharCode = "AF", string threeCharCode = "AFG", string countryName = "Afghanistan")
        {
            Country country = context.CountrySet.Where(c => c.CountryName == countryName).SingleOrDefault();


            if (country == null)
            {
                country = new Country()
                {
                    ISOCountryId = (int)iSOCountryId,
                    TwoCharCode = twoCharCode,
                    ThreeCharCode = threeCharCode,
                    CountryName = countryName
                };

                context.CountrySet.Add(country);

                context.SaveChanges();
            }
            return country;
        }

        #endregion

        //#region Consumer

        //public static Consumer createConsumer(DataContext context,long? id,int fromCountry,int toCountry);



        //#endregion
    }
}
