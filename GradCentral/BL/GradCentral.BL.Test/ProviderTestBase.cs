﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Email;
using GradCentral.BL.Provider.Settings;
using GradCentral.BL.Test.DataConnections;
using GradCentral.BL.Test.Mocs;

namespace GradCentral.BL.Test
{
    public abstract class ProviderTestBase
    {
        private ITestDataConnection _testDataConnection;
        protected IAppSettings AppSettings { get { return new FakeAppSettings(); } }
        protected IEmailProvider EmailProvider { get { return new FakeEmailProvider(); } }

        protected DataContext Context
        {
            get
            {
                return _testDataConnection.Context;
            }
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _testDataConnection = new SQLDataConnection(TearDownDb);
            InitTest();
        }

        [TestCleanup]
        public void TestCleanup()
        {
            _testDataConnection.Dispose();
        }

        public virtual bool TearDownDb { get { return true; } }

        protected virtual void InitTest()
        {
        }

        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public string ReportFolder
        {
            get
            {
                string x = AssemblyDirectory;
                if (!x.EndsWith("\\"))
                    x = x + "\\";
                return x + @"Report\";
            }
        }

        public IAppSettings FakeSettings
        {
            get
            {
                return new FakeAppSettings();
            }
        }
    }
}
