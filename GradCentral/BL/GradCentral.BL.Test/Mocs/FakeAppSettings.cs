﻿using GradCentral.BL.Provider.Settings;

namespace GradCentral.BL.Test.Mocs
{
    public class FakeAppSettings : IAppSettings
    {
        public string EmailFromAddress
        {
            get
            {
                return "Willie@mail.com";
            }
        }

        public string EmailFromName
        {
            get
            {
                return "Willie";
            }
        }

        public string RootURL
        {
            get
            {
                return "http://test@mail.com";
            }
        }
    }
}
