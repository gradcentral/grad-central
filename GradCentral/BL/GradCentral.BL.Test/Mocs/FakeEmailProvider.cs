﻿using System;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Email;
using GradCentral.BL.Provider.Security;
using GC.Lib.BL;

namespace GradCentral.BL.Test.Mocs
{
    public class FakeEmailProvider : IEmailProvider
    {
        public IAuditDBContext<PrivilegeType> AuditDbContext
        {
            get; set;

        }

        public ICurrentUser CurrentUser
        {
            get; set;

        }

        public DataContext DataContext
        {
            get; set;

        }

        public IUserContext<PrivilegeType> LoggedInUser
        {
            get; set;

        }

        public int DataContextSaveChanges()
        {
            return 0;
        }

        public void SendCriticalError(Exception exception)
        {
        }

        public void SendError(Exception exception)
        {
        }

        public void SendInformation(string message)
        {
        }

        public void SendPasswordEmailToUser(UserIdentity user, string password)
        {
        }

        public void SendWarning(string infoMessage)
        {
        }

        public bool UserIsAllowed(PrivilegeType privelege)
        {
            return true;
        }
    }
}
