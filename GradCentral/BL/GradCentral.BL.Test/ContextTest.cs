﻿using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;

namespace GradCentral.BL.Test
{
    [TestClass]
    public class ContextTest : ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Utilities")]
        public void TestCreateDb()
        {
            DataContext.Setup();
        }

        [TestMethod]
        [TestCategory("Utilities")]
        public void SeedAdminUser()
        {
            using (var context = new DataContext())
            {
                    string userName = "AdminUser";
                    var myUser = SeedData.CreateAdmin(context, userName, "password@1");
            }
        }
    }
}
