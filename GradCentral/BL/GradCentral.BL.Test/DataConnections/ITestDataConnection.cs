﻿using System;
using GradCentral.BL.Context;

namespace GradCentral.BL.Test.DataConnections
{
    public interface ITestDataConnection : IDisposable
    {
        DataContext Context { get; set; }
    }
}
