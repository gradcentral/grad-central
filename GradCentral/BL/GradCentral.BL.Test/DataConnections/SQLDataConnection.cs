﻿using System;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using GradCentral.BL.Context;

namespace GradCentral.BL.Test.DataConnections
{
    [ExcludeFromCodeCoverage]
    public class SQLDataConnection :IDisposable,ITestDataConnection
    {

        private string DatabaseName
        {
            get
            {
                var conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"];
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(conStr.ConnectionString);
                return builder.InitialCatalog;
            }
        }

        private string SqlServerName
        {
            get
            {
                var conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"];
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(conStr.ConnectionString);
                return builder.DataSource;
            }
        }

        private string TestUserName
        {
            get
            {
                var conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"];
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(conStr.ConnectionString);
                return builder.UserID;
            }
        }

        private string TestPassword
        {
            get
            {
                var conStr = ConfigurationManager.ConnectionStrings["DefaultConnection"];
                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(conStr.ConnectionString);
                return builder.Password;
            }
        }

        private string _connectionString = "";

        private DbConnection _db;
        public DataContext Context { get; set; }
        public DbConnection DataBase { get { return _db; } }

        public string SQLServerName
        {
            get
            {
                return SqlServerName;
            }
        }

        public string DbConnectionString
        {
            get
            {
                return _connectionString;
            }
        }

        public SQLDataConnection(bool teardownDatabase = true)
        {
            var ensureDllIsCopied = SqlProviderServices.Instance;

            if (teardownDatabase)
                TearDownDatabase();

            _connectionString = String.Format("Data Source={0};Initial Catalog={1};Integrated Security=True;", SqlServerName, DatabaseName);
            using (DbConnection setupCon = new SqlConnection(_connectionString))
            {
                DataContext.Setup(setupCon, false);
            }

            _db = new SqlConnection(_connectionString);
            Context = new DataContext(_db);
        }

        private void TearDownDatabase()
        {
            string conStr = String.Format("Data Source={0};Initial Catalog={1};Integrated Security=true;", SQLServerName, "MASTER");
            using (DbConnection setupCon = new SqlConnection(conStr))
            {
                setupCon.Open();
                //try force single connection mode
                try
                {
                    using (var dbCommand = setupCon.CreateCommand())
                    {
                        dbCommand.CommandText = "ALTER DATABASE [" + DatabaseName + "] SET SINGLE_USER WITH ROLLBACK IMMEDIATE";
                        dbCommand.ExecuteNonQuery();
                    }
                }
                catch
                {
                }

                try
                {
                    using (var dbCommand = setupCon.CreateCommand())
                    {
                        dbCommand.CommandText = "USE master DROP DATABASE [" + DatabaseName + "]";
                        dbCommand.ExecuteNonQuery();
                    }
                }
                catch
                {
                }
            }
        }

        public void Dispose()
        {
            this.Context.Dispose();
            this._db.Dispose();
            SqlConnection.ClearAllPools();
        }



    }
}
