﻿using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using GradCentral.BL.Provider;
using GradCentral.BL.Provider.MasterData;
using SoftwareApproach.TestingExtensions;

namespace GradCentral.BL.Test.Providers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class MasterDataTestProvider : ProviderTestBase
    {
        [TestMethod]
        [TestCategory("Provider.MasterData")]
        public void GetCountry()
        {
            var admin = SeedData.CreateAdmin(Context);

            IMasterDataProvider countryProvider = new MasterDataProvider(Context, admin);

            var x = countryProvider.GetCountry().Count();

            x.ShouldNotBeNull();
            x.ShouldEqual(247);
        }
    }
}
