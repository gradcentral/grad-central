﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.SqlClient;
using System.Linq;
using GradCentral.BL.Entities.Logging;
using GradCentral.BL.Entities.MasterData;
using GradCentral.BL.Entities.SecurityData;
using GC.Lib.BL;
using GC.Lib.Utility;
using Configuration = GradCentral.BL.Migrations.Configuration;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.NewsLetterData;
using GradCentral.BL.Entities.AnnouncementData;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.AttachmentData;
using GradCentral.BL.Entities;
using GradCentral.BL.Entities.JobData;
using GradCentral.BL.Entities.NotificationData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.CompanyData;
using GradCentral.BL.Entities.TertiaryData;
using GradCentral.BL.Entities.TertiaryData.Faculty;
using GradCentral.BL.Entities.TertiaryData.Campus;
using GradCentral.BL.Entities.DepartmentData;


namespace GradCentral.BL.Context
{
    public class DataContext : DbContext, IAuditDBContext<PrivilegeType>
    {

        public DataContext()
            : base("DefaultConnection")
        {
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
        }

        public DataContext(DbConnection connection)
            : base(connection, true)
        {

        }

        public static void Setup()
        {
            var connection = ConfigurationManager.ConnectionStrings["DefaultConnection"];
            using (DbConnection setupCon = new SqlConnection(connection.ConnectionString))
            {
                Setup(setupCon);
            }
        }

        public static void Setup(DbConnection dbConnection, bool initDefaultData = true)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<DataContext, Configuration>());
            using (DataContext context = new DataContext(dbConnection))
            {
                try
                {
                    context.Database.Initialize(true);
                }
                catch (InvalidOperationException ex)
                {
                    throw ex;
                }
                catch (ModelValidationException ex)
                {
                    throw ex;
                }

                if (context.PrivilegeSet.Count() > 0)
                {
                    List<Privilege> privileges = context.PrivilegeSet.ToList();
                    foreach (PrivilegeType ptype in Enum.GetValues(typeof(PrivilegeType)))
                    {
                        var curr = privileges.Where(a => a.Security == ptype).SingleOrDefault();
                        if (curr == null)
                        {
                            curr = new Privilege()
                            {
                                Description = NameSplitting.SplitCamelCase(ptype),
                                Security = ptype
                            };
                            context.PrivilegeSet.Add(curr);
                        }
                    } 
                }
                //Country.LoadDefaultData(context);


                context.SaveChanges();
            }
        }

       

        public DbSet<Role> RoleSet { get; set; }

        public DbSet<Privilege> PrivilegeSet { get; set; }

        public DbSet<UserIdentity> UserIdentitySet { get; set; }

        public DbSet<SystemLog> SystemLogSet { get; set; }

        public DbSet<AuditLog> AuditLogSet { get; set; }

        public DbSet<Country> CountrySet { get; set; }


        public DbSet<Graduate> GraduateSet { get; set; }

        public DbSet<Recruiter> RecruiterSet { get; set; }

        public DbSet<NewsLetter> NewsLetterSet { get; set; }

        public DbSet<Announcement> AnnouncemnetSet { get; set; }

        public DbSet<Attachment> AttachmentSet { get; set; }

        public DbSet<Qualification> QualificationSet { get; set; }

        public DbSet<Campus> CampusSet { get; set; }

        public DbSet<Event> EventSet { get; set; }

        public DbSet<Entities.Marks.Mark> MarkSet{ get; set; }

        public DbSet<Tertiary> TertiarySet { get; set; }
        public DbSet<Faculty> FacultySet { get; set; }
        public DbSet<Entities.ModuleData.Module> ModuleSet { get; set; }

        public DbSet<Job> JobSet { get; set; }
        

        public DbSet<JobRole> JobRoleSet { get; set; }

        public DbSet<Company> CompanySet { get; set; }

        public DbSet<Notification> NotificationSet { get; set; }
        public DbSet<Department> DepartmentSet { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //       modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();


            modelBuilder.Entity<Role>()
                         .HasMany(x => x.Privileges)
                         .WithMany(a => a.Roles)
                         .Map(x =>
                         {
                             x.ToTable("RolePrivilege");
                             x.MapLeftKey("RoleId");
                             x.MapRightKey("PrivilegeId");
                         });

            modelBuilder.Entity<UserIdentity>()
                         .HasMany(x => x.Roles)
                         .WithMany(a => a.UserIdentities)
                         .Map(x =>
                         {
                             x.ToTable("UserIdentityRole");
                             x.MapLeftKey("UserIdentityId");
                             x.MapRightKey("RoleId");
                         });
            modelBuilder.Entity<Qualification>()
                       .HasMany(x => x.Graduates)
                       .WithMany(a => a.Qualifications)
                       .Map(x =>
                       {
                           x.ToTable("QualificationGraduates");
                           x.MapLeftKey("QualificationId");
                           x.MapRightKey("GraduateUserIdentityId");
                       });
            modelBuilder.Entity<Qualification>()
                      .HasMany(x => x.Modules)
                      .WithMany(a => a.Qualifications)
                      .Map(x =>
                      {
                          x.ToTable("QualificationModules");
                          x.MapLeftKey("QualificationId");
                          x.MapRightKey("ModuleId");
                      });
            modelBuilder.Entity<NewsLetter>()
                       .HasMany(x => x.Attachments)
                       .WithMany(a => a.NewsLetters)
                       .Map(x =>
                       {
                           x.ToTable("NewsLetterAttachments");
                           x.MapLeftKey("AttachmentId");
                           x.MapRightKey("NewsLetterId");
                       });
            modelBuilder.Entity<Announcement>()
                       .HasMany(x => x.Attachments)
                       .WithMany(a => a.Announcments)
                       .Map(x =>
                       {
                           x.ToTable("AnnouncementAttachments");
                           x.MapLeftKey("AttachmentId");
                           x.MapRightKey("AnnouncementId");
                       });
            modelBuilder.Entity<Qualification>()
                     .HasMany(x => x.Announcements)
                     .WithMany(a => a.Qualifications)
                     .Map(x =>
                     {
                         x.ToTable("QualificationAnnouncements");
                         x.MapLeftKey("AnnouncementId");
                         x.MapRightKey("QualificationId");
                     });
            modelBuilder.Entity<Qualification>()
                     .HasMany(x => x.NewsLetters)
                     .WithMany(a => a.Qualifications)
                     .Map(x =>
                     {
                         x.ToTable("QualificationNewsLetters");
                         x.MapLeftKey("NewsLetterId");
                         x.MapRightKey("QualificationId");
                     });
            modelBuilder.Entity<Job>()
                    .HasMany(x => x.Applicants)
                    .WithMany(a => a.PotentialJobs)
                    .Map(x =>
                    {
                        x.ToTable("JobApplicants");
                        x.MapLeftKey("GraduateId");
                        x.MapRightKey("JobId");
                    });
            
        }

        public int SaveChanges(IUserContext<PrivilegeType> currentUser)
        {
            if (currentUser != null)
                return AuditHandler.SaveChanges<AuditLog>(this, AuditLogSet, currentUser.UserName, currentUser.Id);
            return base.SaveChanges();
        }

        public void AddSystemLogEntry(object sender, Guid guid, long? currentUserId, LogEventType logEventType,
            string message, string stackTrace = null, string innerExceptionMessage = null, string innerExceptionStackTrace = null)
        {
            try
            {
                //create a fresh context because the current context is in an error state
                using (var dbContext = new DataContext())
                {
                    var syslogEntry = new SystemLog();
                    dbContext.SystemLogSet.Add(syslogEntry);

                    syslogEntry.EventTime = DateTime.Now;
                    syslogEntry.Sender = sender.ToString();
                    syslogEntry.Id = guid;
                    syslogEntry.UserIdentityId = currentUserId;
                    syslogEntry.EventType = logEventType;
                    syslogEntry.Message = message;
                    syslogEntry.StackTrace = stackTrace;
                    syslogEntry.InnerException = innerExceptionMessage;
                    syslogEntry.InnerExceptionStackTrace = innerExceptionStackTrace;

                    dbContext.SaveChanges();
                }
            }
            catch
            {
                //empty exception never allowed!
                //in this case we are unable to hit the db to log an exception
                //therefore we do not want to change the actual error that happend in the handler
            }
        }
    }
}