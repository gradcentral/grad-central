using System.Data.Entity.Migrations;

namespace GradCentral.BL.Migrations
{
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AuditLog",
                c => new
                    {
                        Id = c.Guid(nullable: false, identity: true),
                        UserId = c.Long(),
                        UserName = c.String(maxLength: 200),
                        EventDate = c.DateTime(nullable: false),
                        EventType = c.Int(nullable: false),
                        TableName = c.String(nullable: false, maxLength: 200),
                        RecordId = c.Long(nullable: false),
                        ColumnName = c.String(nullable: false, maxLength: 200),
                        OriginalValue = c.String(),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => new { t.UserName, t.EventDate, t.EventType, t.TableName }, name: "IDX_AuditLog")
                .Index(t => t.UserName, name: "IDX_AuditUser")
                .Index(t => t.EventDate, name: "IDX_AuditDate")
                .Index(t => t.EventType, name: "IDX_EventType")
                .Index(t => t.TableName, name: "IDX_AuditTable");
            
            CreateTable(
                "dbo.CheckListItem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CheckListId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        Description = c.String(),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CheckList", t => t.CheckListId)
                .Index(t => new { t.CheckListId, t.Name }, unique: true, name: "IDX_CheckItem");
            
            CreateTable(
                "dbo.CheckList",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CountryId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        Active = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.CountryId)
                .Index(t => new { t.CountryId, t.Name }, unique: true, name: "IDX_CheckList");
            
            CreateTable(
                "dbo.Country",
                c => new
                    {
                        ISOCountryId = c.Int(nullable: false),
                        ThreeCharCode = c.String(nullable: false, maxLength: 3),
                        TwoCharCode = c.String(nullable: false, maxLength: 2),
                        CountryName = c.String(nullable: false),
                        MondayIsWorkday = c.Boolean(nullable: false),
                        TuesdayIsWorkday = c.Boolean(nullable: false),
                        WednesdayIsWorkday = c.Boolean(nullable: false),
                        ThursdayIsWorkday = c.Boolean(nullable: false),
                        FridayIsWorkday = c.Boolean(nullable: false),
                        SaturdayIsWorkday = c.Boolean(nullable: false),
                        SundayIsWorkday = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ISOCountryId)
                .Index(t => t.ThreeCharCode, unique: true, name: "IDX_ISOCountryThreeChar")
                .Index(t => t.TwoCharCode, unique: true, name: "IDX_ISOCountryTwoChar");
            
            CreateTable(
                "dbo.Consumer",
                c => new
                    {
                        UserIdentityId = c.Long(nullable: false),
                        FromCountryId = c.Int(nullable: false),
                        ToCountryId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.UserIdentityId)
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId)
                .ForeignKey("dbo.Country", t => t.FromCountryId)
                .ForeignKey("dbo.Country", t => t.ToCountryId)
                .Index(t => t.UserIdentityId)
                .Index(t => t.FromCountryId)
                .Index(t => t.ToCountryId);
            
            CreateTable(
                "dbo.ConsumerCheckListItem",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ConsumerUserIdentityId = c.Long(nullable: false),
                        CheckListItemId = c.Long(nullable: false),
                        Completed = c.Boolean(nullable: false),
                        DateCompleted = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CheckListItem", t => t.CheckListItemId)
                .ForeignKey("dbo.Consumer", t => t.ConsumerUserIdentityId)
                .Index(t => new { t.ConsumerUserIdentityId, t.CheckListItemId }, unique: true, name: "IDX_ConsumerCheckListItem");
            
            CreateTable(
                "dbo.UserIdentity",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 50),
                        Active = c.Boolean(nullable: false),
                        Title = c.String(nullable: false, maxLength: 50),
                        FirstName = c.String(nullable: false, maxLength: 50),
                        Surname = c.String(nullable: false, maxLength: 50),
                        PasswordHash = c.String(nullable: false, maxLength: 200),
                        Email = c.String(nullable: false, maxLength: 200),
                        FailedLoginAttempts = c.Int(nullable: false),
                        LockedOut = c.DateTime(),
                        Deactivated = c.DateTime(),
                        LastLogin = c.DateTime(),
                        IsSystemAdmin = c.Boolean(nullable: false),
                        ConsumerId = c.Long(),
                        ServiceProviderId = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceProvider", t => t.ServiceProviderId)
                .Index(t => t.UserName, unique: true, name: "IDX_UserName")
                .Index(t => t.ServiceProviderId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        RoleName = c.String(nullable: false, maxLength: 100),
                        Description = c.String(),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Privileges",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Security = c.Int(nullable: false),
                        Description = c.String(nullable: false, maxLength: 200),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Security, unique: true, name: "IDX_PrivilegeSecurity");
            
            CreateTable(
                "dbo.ServiceProvider",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CountryId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 300),
                        Active = c.Boolean(nullable: false),
                        Email = c.String(nullable: false, maxLength: 300),
                        OfficePhone = c.String(nullable: false, maxLength: 20),
                        MobilePhone = c.String(maxLength: 20),
                        ContactPerson = c.String(nullable: false, maxLength: 300),
                        AddressLine1 = c.String(maxLength: 100),
                        AddressLine2 = c.String(maxLength: 100),
                        City = c.String(maxLength: 100),
                        PostalCode = c.String(maxLength: 50),
                        WebSiteURL = c.String(maxLength: 500),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Country", t => t.CountryId)
                .Index(t => new { t.CountryId, t.Name }, unique: true, name: "IDX_ServiceProvider");
            
            CreateTable(
                "dbo.Service",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ServiceName = c.String(nullable: false, maxLength: 200),
                        Description = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.ServiceName, unique: true, name: "IDX_Service");
            
            CreateTable(
                "dbo.SystemLogs",
                c => new
                    {
                        Id = c.Guid(nullable: false),
                        EventTime = c.DateTime(nullable: false),
                        Sender = c.String(),
                        UserIdentityId = c.Long(),
                        EventType = c.Int(nullable: false),
                        Message = c.String(),
                        StackTrace = c.String(),
                        InnerException = c.String(),
                        InnerExceptionStackTrace = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RolePrivilege",
                c => new
                    {
                        RoleId = c.Long(nullable: false),
                        PrivilegeId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.RoleId, t.PrivilegeId })
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.Privileges", t => t.PrivilegeId, cascadeDelete: true)
                .Index(t => t.RoleId)
                .Index(t => t.PrivilegeId);
            
            CreateTable(
                "dbo.UserIdentityRole",
                c => new
                    {
                        UserIdentityId = c.Long(nullable: false),
                        RoleId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserIdentityId, t.RoleId })
                .ForeignKey("dbo.UserIdentity", t => t.UserIdentityId, cascadeDelete: true)
                .ForeignKey("dbo.Role", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserIdentityId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ServiceProviderService",
                c => new
                    {
                        ServiceId = c.Long(nullable: false),
                        ServiceProviderId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => new { t.ServiceId, t.ServiceProviderId })
                .ForeignKey("dbo.Service", t => t.ServiceId, cascadeDelete: true)
                .ForeignKey("dbo.ServiceProvider", t => t.ServiceProviderId, cascadeDelete: true)
                .Index(t => t.ServiceId)
                .Index(t => t.ServiceProviderId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Consumer", "ToCountryId", "dbo.Country");
            DropForeignKey("dbo.Consumer", "FromCountryId", "dbo.Country");
            DropForeignKey("dbo.Consumer", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.UserIdentity", "ServiceProviderId", "dbo.ServiceProvider");
            DropForeignKey("dbo.ServiceProviderService", "ServiceProviderId", "dbo.ServiceProvider");
            DropForeignKey("dbo.ServiceProviderService", "ServiceId", "dbo.Service");
            DropForeignKey("dbo.ServiceProvider", "CountryId", "dbo.Country");
            DropForeignKey("dbo.UserIdentityRole", "RoleId", "dbo.Role");
            DropForeignKey("dbo.UserIdentityRole", "UserIdentityId", "dbo.UserIdentity");
            DropForeignKey("dbo.RolePrivilege", "PrivilegeId", "dbo.Privileges");
            DropForeignKey("dbo.RolePrivilege", "RoleId", "dbo.Role");
            DropForeignKey("dbo.ConsumerCheckListItem", "ConsumerUserIdentityId", "dbo.Consumer");
            DropForeignKey("dbo.ConsumerCheckListItem", "CheckListItemId", "dbo.CheckListItem");
            DropForeignKey("dbo.CheckList", "CountryId", "dbo.Country");
            DropForeignKey("dbo.CheckListItem", "CheckListId", "dbo.CheckList");
            DropIndex("dbo.ServiceProviderService", new[] { "ServiceProviderId" });
            DropIndex("dbo.ServiceProviderService", new[] { "ServiceId" });
            DropIndex("dbo.UserIdentityRole", new[] { "RoleId" });
            DropIndex("dbo.UserIdentityRole", new[] { "UserIdentityId" });
            DropIndex("dbo.RolePrivilege", new[] { "PrivilegeId" });
            DropIndex("dbo.RolePrivilege", new[] { "RoleId" });
            DropIndex("dbo.Service", "IDX_Service");
            DropIndex("dbo.ServiceProvider", "IDX_ServiceProvider");
            DropIndex("dbo.Privileges", "IDX_PrivilegeSecurity");
            DropIndex("dbo.UserIdentity", new[] { "ServiceProviderId" });
            DropIndex("dbo.UserIdentity", "IDX_UserName");
            DropIndex("dbo.ConsumerCheckListItem", "IDX_ConsumerCheckListItem");
            DropIndex("dbo.Consumer", new[] { "ToCountryId" });
            DropIndex("dbo.Consumer", new[] { "FromCountryId" });
            DropIndex("dbo.Consumer", new[] { "UserIdentityId" });
            DropIndex("dbo.Country", "IDX_ISOCountryTwoChar");
            DropIndex("dbo.Country", "IDX_ISOCountryThreeChar");
            DropIndex("dbo.CheckList", "IDX_CheckList");
            DropIndex("dbo.CheckListItem", "IDX_CheckItem");
            DropIndex("dbo.AuditLog", "IDX_AuditTable");
            DropIndex("dbo.AuditLog", "IDX_EventType");
            DropIndex("dbo.AuditLog", "IDX_AuditDate");
            DropIndex("dbo.AuditLog", "IDX_AuditUser");
            DropIndex("dbo.AuditLog", "IDX_AuditLog");
            DropTable("dbo.ServiceProviderService");
            DropTable("dbo.UserIdentityRole");
            DropTable("dbo.RolePrivilege");
            DropTable("dbo.SystemLogs");
            DropTable("dbo.Service");
            DropTable("dbo.ServiceProvider");
            DropTable("dbo.Privileges");
            DropTable("dbo.Role");
            DropTable("dbo.UserIdentity");
            DropTable("dbo.ConsumerCheckListItem");
            DropTable("dbo.Consumer");
            DropTable("dbo.Country");
            DropTable("dbo.CheckList");
            DropTable("dbo.CheckListItem");
            DropTable("dbo.AuditLog");
        }
    }
}
