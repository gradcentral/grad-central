﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.JobData;

namespace GradCentral.BL.Entities.NotificationData
{
    [Table("Notification")]
    public class Notification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }
        [Required]
        public virtual long JobId { get; set; }

        [ForeignKey("JobId")]
        public virtual Job Job { get; set; }

        [Required]
        public virtual long UserIdentityId { get; set; }

        [Required]
        public virtual bool Read { get; set; }

        public virtual DateTime Date { get; set; }

        public virtual DateTime ExpiryDate { get; set; }

        public virtual bool Confirm { get; set; }

        [ForeignKey("UserIdentityId")]
        public virtual SecurityData.UserIdentity UserIdentity { get; set; }

        [Required]
        public virtual string Message { get; set; }
        [Required]
        public virtual DateTime DateCreated { get; set; }


    }
}
