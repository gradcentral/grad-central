﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.Types;
using System;
using GradCentral.BL.Entities.QualificationData;

namespace GradCentral.BL.Entities.JobData
{
    [Table("Job")]
    public class Job
    {        
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        //[Required]
        //public virtual DateTime Date { get; set; }

        [Required]
        public virtual string Title { get; set; }

        [Required]
        public virtual long JobRoleId { get; set; }

        [ForeignKey("JobRoleId")]
        public virtual JobRole JobRole{ get; set; }
        
        [Required]
        public virtual long RecruiterId { get; set; }

        [ForeignKey("RecruiterId")]
        public virtual Recruiter Recruiter { get; set; }
        [Required]
        public virtual long QualificationId { get; set; }

        [ForeignKey("RecruiterId")]
        public virtual Qualification Qualification { get; set; }

        [Required]
        public virtual Boolean Disability { get; set; }

        [Required]
        public virtual GenderType Gender { get; set; }

        [Required]
        public virtual RaceType Race { get; set; }
        [Required]
        public virtual PositionTypes Position { get; set; }

        [Required]
        public virtual double GPA { get; set; }

        public ICollection<Graduate> Applicants { get; set; }
    }
}
