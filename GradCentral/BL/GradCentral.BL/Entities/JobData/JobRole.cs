﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.JobData
{
    [Table("JobRole")]
    public class JobRole
    {
        [Key]
        [Required]
        public virtual long Id { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
