﻿using GradCentral.BL.Entities.TertiaryData.Faculty;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.DepartmentData
{
    [Table("Department")]
    public class Department
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }
        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual long FacultyId { get; set; }
        [ForeignKey("FacultyId")]

        public virtual Faculty Faculty { get; set; }
    }
}
