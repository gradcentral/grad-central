﻿using GradCentral.BL.Entities.AttachmentData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.GraduateData;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.Types;

namespace GradCentral.BL.Entities
{
    [Table("Event")]
    public class Event
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public TimeSpan Duration { get; set; }

        public EventType EventType { get; set; }

        public ICollection<Qualification> Qualifications { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string Location { get; set; }

        public string Geolocation { get; set; }

        public int  Capacity { get; set; }

        [Required]
        public virtual long RecruiterId { get; set; }

        [ForeignKey("RecruiterId")]
        public virtual Recruiter Recruiter { get; set; }

        public ICollection<Graduate> Graduates { get; set; }

        public ICollection<Attachment> Attachments { get; set; }
    }
}
