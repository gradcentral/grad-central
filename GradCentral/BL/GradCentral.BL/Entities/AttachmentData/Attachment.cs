﻿using GradCentral.BL.Entities.AnnouncementData;
using GradCentral.BL.Entities.NewsLetterData;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Entities.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.AttachmentData
{
    [Table("Attachment")]
    public class Attachment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }
        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public virtual long RecruiterId { get; set; }
        [ForeignKey("RecruiterId")]
        public virtual Recruiter Recruiter { get; set; }

        [Required]

        public string AttachmentPath { get; set; }

        public virtual ICollection<Announcement> Announcments { get; set; }

        public virtual ICollection<NewsLetter> NewsLetters { get; set; }

        public AttachmentFormat AttachmentFormat { get; set; }
    }
}
