using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.JobData;

namespace GradCentral.BL.Entities.RecruiterData
{
    [Table("Recruiter")]
    public class Recruiter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_RecruiterUserIdentityId", IsUnique = true)]
        public virtual long UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]
        public virtual UserIdentity UserIdentity { get; set; }

        public virtual long CompanyId { get; set; }
        [ForeignKey("CompanyId")]
        public virtual CompanyData.Company Company { get; set; }

        [Required]
        [NotMapped]
        public virtual IdType IdType { get; set; }

        public ICollection<Event> Events { get; set; }

        public ICollection<GradCentral.BL.Entities.JobData.Job> Jobs { get; set; }

    }
}
