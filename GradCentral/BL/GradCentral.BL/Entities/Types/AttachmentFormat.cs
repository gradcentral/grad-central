﻿namespace GradCentral.BL.Entities.Types
{
    public enum AttachmentFormat
    {
        jpg,
        jpeg,
        xls,
        png
    }
}
