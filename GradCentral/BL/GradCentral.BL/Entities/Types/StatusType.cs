﻿namespace GradCentral.BL.Entities.Types
{
    public enum StatusType
    {
        Active,
        Inactive,
        Archive
    }
}
