﻿

namespace GradCentral.BL.Entities.Types
{
    public enum TitleType
    {
        Mr,
        Miss,
        Mrs,
        Dr,
        Prof
    }
}
