﻿namespace GradCentral.BL.Entities.Types
{
    public enum TertiaryTypes
    {
        FETCollege,
        Technicon,
        University,
    }
}
