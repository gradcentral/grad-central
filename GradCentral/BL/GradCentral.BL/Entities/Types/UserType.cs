﻿namespace GradCentral.BL.Entities.Types
{
    public enum UserType
    {
        Admin,
        Graduate,
        Recruiter
    }
}
