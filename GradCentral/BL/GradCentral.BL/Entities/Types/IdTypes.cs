﻿namespace GradCentral.BL.Entities.Types
{
    public enum IdType
    {
        RSAIDType,
        ForeignId
    }
}
