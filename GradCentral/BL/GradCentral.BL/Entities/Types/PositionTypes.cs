﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradCentral.BL.Entities.Types
{
    public enum PositionTypes
    {
        Snr,
        Intermmediate,
        Junior,
        Graduate
    }
}
