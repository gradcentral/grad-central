﻿namespace GradCentral.BL.Entities.Types
{
    public enum ProvinceType
    {
        EasternCape,
        FreeState,
        Gauteng,
        KwaZuluNatal,
        Limpopo,
        Mpumalanga,
        NorthernCape,
        NorthWest,
        WesternCape
    }
}
