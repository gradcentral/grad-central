﻿namespace GradCentral.BL.Entities.Types
{
    public enum QualificationType
    {
        Diploma,
        NationalDiploma,
        AdvanceDiploma,
        Certificate,
        HigherCertificate,
        BADegree,
        BscDegree,
        Bcom,
        HonoursDegree,
        MastersDegree,
        PHD
    }
}
