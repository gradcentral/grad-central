﻿

namespace GradCentral.BL.Entities.Types
{
    public enum EventType
    {
        Training,
        GraduateProgram,
        Seminar
    }
}
