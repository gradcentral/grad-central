﻿
namespace GradCentral.BL.Entities.Types
{
    public enum RaceType
    {
        Black,
        White,
        Coloured,
        Indian,
        Other
    }
}
