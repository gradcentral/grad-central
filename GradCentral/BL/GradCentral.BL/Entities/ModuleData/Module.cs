﻿using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.TertiaryData;
using GradCentral.BL.Entities.Types;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradCentral.BL.Entities.ModuleData
{

    [Table("Module")]
    public class Module
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public virtual string Name{ get; set; }

        [Required]
        public virtual SemesterTypes Semester { get; set; }

        [Required]
        public virtual long QualificationId { get; set; }

        [ForeignKey("QualificationId")]
        
        public virtual Qualification Qualification { get; set; }

        public virtual ICollection<Qualification> Qualifications { get; set; }

        //public ICollection<Marks.Mark>  Mark { get; set; }
    }
}
