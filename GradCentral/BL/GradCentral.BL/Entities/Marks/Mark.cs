﻿using GradCentral.BL.Entities.ModuleData;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.QualificationData;

namespace GradCentral.BL.Entities.Marks
{

    [Table("Mark")]
    public class Mark
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public virtual long QualificationId { get; set; }

        //[Required]
        //[ForeignKey("QualificationId")]
        //public virtual Qualification Qualification { get; set; }

        [Required]
        public double GPA { get; set; }

        [Required]
        public virtual long GraduateId { get; set; }
        [ForeignKey("GraduateId")]
        public virtual GraduateData.Graduate Graduate { get; set; }
    }
}
