﻿using GradCentral.BL.Entities.AttachmentData;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.RecruiterData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.AnnouncementData
{
    [Table("Announcement")]
    public class Announcement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public DateTime DateTime { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Text { get; set; }

        [Required]
        public virtual long RecruiterId { get; set; }
        [ForeignKey("RecruiterId")]
        public virtual Recruiter Recruiter { get; set; }

        public virtual ICollection<Attachment> Attachments { get; set; }

        public virtual ICollection<Qualification> Qualifications { get; set; }
    }
}
