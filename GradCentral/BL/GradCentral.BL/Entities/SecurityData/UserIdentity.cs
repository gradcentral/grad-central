﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.Types;

namespace GradCentral.BL.Entities.SecurityData
{
    [Table("UserIdentity")]
    public class UserIdentity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_UserName", IsUnique = true, Order = 1)]
        [MaxLength(50)]
        [Required]
        public virtual string UserName { get; set; }

        public bool Active { get; set; }

        [Required]
        public virtual TitleType Title { get; set; }

        [MaxLength(50)]
        [Required]
        public virtual string FirstName { get; set; }

        [MaxLength(50)]
        [Required]
        public virtual string Surname { get; set; }


        [Required]
        public virtual GenderType Gender { get; set; }

        public virtual string GenderString { get { return Gender.ToString(); } }

        [MaxLength(200)]
        //[Required]
        public virtual string PasswordHash { get; set; }

        [Required]
        [MaxLength(200)]
        public string Email { get; set; }

        public string CellNumber { get; set; }

        public int FailedLoginAttempts { get; set; }

        public DateTime? LockedOut { get; set; }

        public DateTime? Deactivated { get; set; }

        public DateTime? LastLogin { get; set; }

        public bool IsSystemAdmin { get; set; }

        public virtual ICollection<Role> Roles { get; set; }

        [Required]
        public virtual Types.UserType Type { get; set; }   

        [NotMapped]
        public List<PrivilegeType> AllowedPrivileges { get; set; }
    }
}
