﻿using System;

namespace GradCentral.BL.Entities.SecurityData
{
    [Serializable]
    public enum PrivilegeType
    {
        UserMaintenance,
        RoleMaintenance,
        GraduateMaintenance,
        StaffMaintenance,
        NewsLetterMaintenance,
        AnnouncementMaintenance,
        ApplicationMaintenance,
        CampusMaintenance,
        TertiaryMaintenance,
        QualificationMaintenance,
        ModuleMaintenance,
        FacultyMaintenance,
        JobRoleMaintenance,
        JobMaintenance,
        CompanyMaintenance,
        RecruiterMaintenance,
        DepartmentMaintenance,
        JobSearch,
        Applications,
        Notifications,
        Qualifications

    }
}
