﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.Types;

namespace GradCentral.BL.Entities.SecurityData
{
    [Table("Role")]
    public class Role
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        public virtual ICollection<Privilege> Privileges { get; set; }
        public virtual ICollection<UserIdentity> UserIdentities { get; set; }

        [Required]
        [MaxLength(100)]
        public virtual string RoleName { get; set; }

        public virtual string Description { get; set; }

        public virtual StatusType Status { get; set; }
    }
}
