﻿using GradCentral.BL.Entities.SecurityData;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.JobData;
using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.TertiaryData;
using GradCentral.BL.Entities.Types;

namespace GradCentral.BL.Entities.GraduateData
{

    [Table("Graduate")]
    public class Graduate
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Index("IDX_GraduateUserIdentityId", IsUnique = true)]
        public virtual long UserIdentityId { get; set; }
        [ForeignKey("UserIdentityId")]
        public virtual UserIdentity UserIdentity { get; set; }

        [Required]
        public virtual bool IsEmployed { get; set; }

        [Required]
        public virtual bool IsNasfas { get; set; }

        [Required]
        public virtual long TertiaryId { get; set; }

        public virtual long? JobId { get; set; }

        public virtual RaceType Race { get; set; }

        public virtual string RaceString { get {return Race.ToString();} }


        public virtual bool IsDisable { get; set; }

        public virtual string DisabilityDescription { get; set; }

        public string CurrentEmployer { get; set; }

        [ForeignKey("TertiaryId")]
        public Tertiary Tertiary { get; set; }

        public ICollection<Qualification> Qualifications { get; set; }

        [ForeignKey("JobId")]

        public Job Job { get; set; }


        public ICollection<Job> PotentialJobs { get; set; } 
    }

}
