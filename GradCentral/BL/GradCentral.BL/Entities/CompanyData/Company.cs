﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GradCentral.BL.Entities.RecruiterData;

namespace GradCentral.BL.Entities.CompanyData
{
    [Table("Company")]
    public class Company
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public virtual string Number { get; set; }

        [Required]
        public virtual string Street { get; set; }

        [Required]
        public virtual string Town { get; set; }

        [Required]
        public virtual string City { get; set; }

        [Required]
        public virtual string Province { get; set; }

        [Required]
        public virtual int Zip { get; set; }

        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual string Email { get; set; }

        public ICollection<Company> Companies { get; set; }

        public ICollection<Recruiter> Recruiters { get; set; }
    }
}
