﻿using GradCentral.BL.Entities.AnnouncementData;
using GradCentral.BL.Entities.NewsLetterData;
using GradCentral.BL.Entities.GraduateData;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using GradCentral.BL.Entities.Types;
using GradCentral.BL.Entities.TertiaryData.Faculty;
using GradCentral.BL.Entities.Marks;

namespace GradCentral.BL.Entities.QualificationData
{
    [Table("Qualification")]
    public class Qualification
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public DateTime Date { get; set; }

        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual long FacultyId { get; set; }
        [ForeignKey("FacultyId")]

        public virtual Faculty Faculty { get; set; }

        [Required]
        public QualificationType QualificationType { get; set; }

        public virtual ICollection<Graduate> Graduates { get; set; }

        public virtual ICollection<Announcement> Announcements { get; set; }

        public virtual ICollection<NewsLetter> NewsLetters { get; set; }

        public virtual ICollection<ModuleData.Module> Modules { get; set; }

        public virtual ICollection<Mark> Marks { get; set; }


    }
}
