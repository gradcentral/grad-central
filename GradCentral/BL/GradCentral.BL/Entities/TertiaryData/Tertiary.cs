﻿using GradCentral.BL.Entities.QualificationData;
using GradCentral.BL.Entities.Types;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.TertiaryData
{
    [Table("Tertiary")]
    public class Tertiary
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public virtual string Name { get; set; }

        [Required]
        public TertiaryTypes TertiaryType { get; set; }

        [Required]
        public ProvinceType ProvinceType { get; set; }

        public ICollection<GraduateData.Graduate> Graduates { get;  set; }


        //public ICollection<Qualification> Qualifications { get; set; }
    }
}
