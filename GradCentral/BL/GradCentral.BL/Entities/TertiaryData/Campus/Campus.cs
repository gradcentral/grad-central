﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.TertiaryData.Campus
{
    [Table("Campus")]
    public class Campus
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public virtual string Number { get; set; }

        [Required]
        public virtual string Street { get; set; }

        [Required]
        public virtual string Town { get; set; }

        [Required]
        public virtual string City { get; set; }

        [Required]
        public virtual string Province{ get; set; }

        [Required]
        public virtual int Zip { get; set; }

        [Required]
        public virtual string Name { get; set; }
        [Required]
        public virtual long TertiaryId { get; set; }
        [ForeignKey(" TertiaryId")]
        public virtual Tertiary Tertiary { get; set; }

    }
}
