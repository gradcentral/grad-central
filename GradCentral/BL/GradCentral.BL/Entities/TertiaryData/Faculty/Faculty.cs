﻿using GradCentral.BL.Entities.DepartmentData;
using GradCentral.BL.Entities.QualificationData;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace GradCentral.BL.Entities.TertiaryData.Faculty
{
    [Table("Faculty")]
    public class Faculty
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public virtual long Id { get; set; }

        [Required]
        public virtual string Name { get; set; }
        
        public virtual long CampusId { get; set; }
        [ForeignKey("CampusId")]
        public virtual Campus.Campus Campus { get; set; }

        public ICollection<Qualification> Qualifications { get; set; }
    }
}
