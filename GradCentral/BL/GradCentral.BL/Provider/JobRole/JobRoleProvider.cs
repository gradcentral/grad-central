﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System.Security;

namespace GradCentral.BL.Provider.JobRole
{
    public class JobRoleProvider : GradCentralProvider,IJobRoleProvider
    {
        public JobRoleProvider(DataContext context, ICurrentUser currentUser) : base(context,currentUser){}

        public Entities.JobData.JobRole GetJobRole(long JobRoleId)
        {
            try
            {
                return DataContext.JobRoleSet.SingleOrDefault(s => s.Id == JobRoleId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get JobRole.");
            }
        }

        public List<Entities.JobData.JobRole> GetJobRoles()
        {
            try
            {
                return DataContext.JobRoleSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get JobRole.");
            }
        }

        public List<Entities.QualificationData.Qualification> GetQualifications()
        {
            try
            {
                return DataContext.QualificationSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get qualifications.");
            }
        }
        public Entities.JobData.JobRole SaveJobRole(Entities.JobData.JobRole JobRole)
        {
            try
            {
                Entities.JobData.JobRole newJobRole = new Entities.JobData.JobRole();
                if (JobRole.Id > 0)
                {
                    newJobRole = DataContext.JobRoleSet.SingleOrDefault(s => s.Id == JobRole.Id);
                    if(newJobRole != null)
                    {
                        newJobRole.Id = JobRole.Id;
                        newJobRole.Name = JobRole.Name;

                        DataContextSaveChanges();

                    }
                }
                else
                {
                    newJobRole = new Entities.JobData.JobRole();
                    newJobRole = JobRole;
                    DataContext.JobRoleSet.Add(newJobRole);
                    DataContextSaveChanges();
                    return newJobRole;
                }
                return null;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to save JobRole.");
            }
        }

        public Entities.JobData.JobRole DeactivateJobRole(long JobRoleId)
        {
            try
            {
                Entities.JobData.JobRole newJobRole = new Entities.JobData.JobRole();
                if (JobRoleId > 0)
                {
                    newJobRole = DataContext.JobRoleSet.SingleOrDefault(s => s.Id == JobRoleId);

                    if (newJobRole != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newJobRole;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
