﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.JobRole
{
    public interface IJobRoleProvider : IGradCentralProvider
    {
        Entities.JobData.JobRole GetJobRole(long JobRoleId);
        List<Entities.JobData.JobRole> GetJobRoles();
        Entities.JobData.JobRole SaveJobRole(Entities.JobData.JobRole JobRole);

        Entities.JobData.JobRole DeactivateJobRole(long JobRoleId);
    }
}
