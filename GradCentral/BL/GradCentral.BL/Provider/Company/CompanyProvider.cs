﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System.Security;

namespace GradCentral.BL.Provider.Company
{
    public class CompanyProvider : GradCentralProvider,ICompanyProvider
    {
        public CompanyProvider(DataContext context, ICurrentUser currentUser) : base(context,currentUser){}

        public Entities.CompanyData.Company GetCompany(long CompanyId)
        {
            try
            {
                return DataContext.CompanySet.SingleOrDefault(s => s.Id == CompanyId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Company.");
            }
        }

        public List<Entities.CompanyData.Company> GetCompanys()
        {
            try
            {
                return DataContext.CompanySet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Company.");
            }
        }

        public List<Entities.QualificationData.Qualification> GetQualifications()
        {
            try
            {
                return DataContext.QualificationSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get qualifications.");
            }
        }
        public Entities.CompanyData.Company SaveCompany(Entities.CompanyData.Company Company)
        {
            try
            {
                Entities.CompanyData.Company newCompany = new Entities.CompanyData.Company();
                if (Company.Id > 0)
                {
                    newCompany = DataContext.CompanySet.SingleOrDefault(s => s.Id == Company.Id);
                    if(newCompany != null)
                    {
                        newCompany.Id = Company.Id;
                        newCompany.Name = Company.Name;
                        newCompany.Street = Company.Street;
                        newCompany.City = Company.City;
                        newCompany.Number = Company.Number;
                        newCompany.Town = Company.Town;
                        newCompany.Province = Company.Province;
                        newCompany.Zip = Company.Zip;
                        newCompany.Companies = Company.Companies;
                        newCompany.Recruiters = Company.Recruiters;
                        newCompany.Email = Company.Email;

                        DataContextSaveChanges();
                    }
                }
                else
                {
                    newCompany = new Entities.CompanyData.Company();
                    newCompany = Company;
                    DataContext.CompanySet.Add(newCompany);
                    DataContextSaveChanges();
                    return newCompany;
                }
                return null;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to save Company.");
            }
        }

        public Entities.CompanyData.Company DeactivateCompany(long CompanyId)
        {
            try
            {
                Entities.CompanyData.Company newCompany = new Entities.CompanyData.Company();
                if (CompanyId > 0)
                {
                    newCompany = DataContext.CompanySet.SingleOrDefault(s => s.Id == CompanyId);

                    if (newCompany != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newCompany;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
