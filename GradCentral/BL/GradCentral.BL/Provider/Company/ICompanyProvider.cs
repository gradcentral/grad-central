﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Company
{
    public interface ICompanyProvider : IGradCentralProvider
    {
        Entities.CompanyData.Company GetCompany(long CompanyId);
        List<Entities.CompanyData.Company> GetCompanys();
        Entities.CompanyData.Company SaveCompany(Entities.CompanyData.Company Company);

        Entities.CompanyData.Company DeactivateCompany(long CompanyId);
    }
}
