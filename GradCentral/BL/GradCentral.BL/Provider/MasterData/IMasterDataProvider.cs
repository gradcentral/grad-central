﻿using System.Linq;
using GradCentral.BL.Entities.MasterData;

namespace GradCentral.BL.Provider
{
    public interface IMasterDataProvider:IGradCentralProvider
    {
        Country SaveCountry(int? id,string threeCharCode,string twoCharCode,string country,bool mon,
            bool tue ,bool wed,bool thurs,bool fri,bool sat,bool sun);

        IQueryable<Country> GetCountry();
    }
}
