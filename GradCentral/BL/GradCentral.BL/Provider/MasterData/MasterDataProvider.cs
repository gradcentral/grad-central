﻿using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.MasterData;
using GradCentral.BL.Provider.Security;

namespace GradCentral.BL.Provider.MasterData
{
    public class MasterDataProvider : GradCentralProvider,IMasterDataProvider
    {
        #region Constructor
        public MasterDataProvider(DataContext context,ICurrentUser currentUser)
            :base(context,currentUser)
        {

        }
        #endregion
        public Country SaveCountry(int ?id, string threeCharCode, string twoCharCode, string country, bool mon, bool tue, bool wed, bool thurs, bool fri, bool sat, bool sun)
        {
            Country saveCountry = new Country();

            saveCountry = DataContext.CountrySet.Where(a=>a.ISOCountryId != id && a.CountryName==country).SingleOrDefault();

            if (saveCountry != null)
            {
                throw new MasterDataException("Country with the name " +country+ " already exists.");
            }

            if (id !=null)
            {
                saveCountry = DataContext.CountrySet.Where(a => a.ISOCountryId == id).SingleOrDefault();
            }
            else
            {
                saveCountry = new Country();
                //saveCountry.ISOCountryId = (int)id;
                saveCountry.CountryName = country;
                saveCountry.TwoCharCode = twoCharCode;
                saveCountry.ThreeCharCode = threeCharCode;
                DataContext.CountrySet.Add(saveCountry);
            }

            saveCountry = new Country();
            saveCountry.CountryName = country;
            saveCountry.ISOCountryId = (int)id;
            saveCountry.TwoCharCode = twoCharCode;
            saveCountry.ThreeCharCode = threeCharCode;
            saveCountry.MondayIsWorkday = mon;
            saveCountry.TuesdayIsWorkday = tue;
            saveCountry.WednesdayIsWorkday = wed;
            saveCountry.ThursdayIsWorkday = thurs;
            saveCountry.FridayIsWorkday = fri;
            saveCountry.SaturdayIsWorkday = sat;
            saveCountry.SundayIsWorkday = sun;

    
            DataContext.CountrySet.Add(saveCountry);
            DataContextSaveChanges();

            return saveCountry;
        }

        public IQueryable<Country> GetCountry()
        {
            var objQuery = from c in DataContext.CountrySet
                           orderby c.CountryName
                           select c;

            return objQuery;
        }
    }
}
