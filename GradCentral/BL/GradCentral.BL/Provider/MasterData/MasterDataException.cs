﻿using System;

namespace GradCentral.BL.Provider.MasterData
{
    public class MasterDataException : Exception
    {
        public MasterDataException(string message)
            :base(message)
        {

        }
    }
}
