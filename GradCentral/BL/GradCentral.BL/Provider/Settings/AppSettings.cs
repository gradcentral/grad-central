﻿using System;
using System.Configuration;
using System.IO;
using System.Reflection;

namespace GradCentral.BL.Provider.Settings
{
    public class AppSettings : IAppSettings
    {
        public AppSettings(string rootReportFolder)
        {
            //if (!rootReportFolder.EndsWith("\\"))
            //    rootReportFolder = rootReportFolder + "\\";

            //RDLCFolder = rootReportFolder + "Report\\";
        }

        public AppSettings()
            : this(AssemblyDirectory)
        {

        }

        private static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        public string EmailFromName
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailFromName"] as string;
            }
        }

        public string EmailFromAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["EmailFromAddress"] as string;
            }
        }

        public string RootURL
        {
            get
            {
                return ConfigurationManager.AppSettings["RootURL"] as string;
            }
        }
    }
}
