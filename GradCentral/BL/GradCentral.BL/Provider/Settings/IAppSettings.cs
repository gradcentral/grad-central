﻿namespace GradCentral.BL.Provider.Settings
{
    public interface IAppSettings
    {
        string EmailFromName { get; }

        string EmailFromAddress { get; }

        string RootURL { get; }
    }
}
