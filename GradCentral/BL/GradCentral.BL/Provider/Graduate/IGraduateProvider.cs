﻿using GradCentral.BL.Entities.Types;
using System.Collections.Generic;

namespace GradCentral.BL.Provider.Graduate
{
    public interface IGraduateProvider : IGradCentralProvider
    {
        Entities.GraduateData.Graduate GetGraduate(long GraduateId);
        List<Entities.GraduateData.Graduate> GetGraduates();

        List<Entities.Marks.Mark> GetMarks();

        Entities.GraduateData.Graduate SaveGraduate(Entities.GraduateData.Graduate Graduate);
        List<Entities.GraduateData.Graduate> GetJobCandidates(int jobId);

        List<Entities.GraduateData.Graduate> GetPossibleCandidates(bool IsDisable, RaceType race, GenderType gender, int qualificationId, int gpa);

        Entities.GraduateData.Graduate DeactivateGraduate(long GraduateId);

    }
}
