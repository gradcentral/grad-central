﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using GradCentral.BL.Provider.Tertiary;
using System.Collections;
using GradCentral.BL.Entities.Types;
using GradCentral.BL.Entities.Marks;
using System.Collections.ObjectModel;

namespace GradCentral.BL.Provider.Graduate
{
    public class GraduateProvider : GradCentralProvider, IGraduateProvider
    {
        #region Constructor
        public GraduateProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion
        public Entities.GraduateData.Graduate GetGraduate(long GraduateId)
        {
            try
            {
                Entities.GraduateData.Graduate grad = DataContext.GraduateSet.SingleOrDefault(s => s.Id == GraduateId);


                return grad;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<Entities.GraduateData.Graduate> GetJobCandidates(int jobId)
        {
            try
            {
                List<Entities.GraduateData.Graduate> grads = new List<Entities.GraduateData.Graduate>();
                List<Entities.NotificationData.Notification> notifications = DataContext.NotificationSet.Where(n => n.JobId == jobId).ToList();

                foreach (Entities.NotificationData.Notification notification in notifications)
                {
                    grads.Add(DataContext.GraduateSet.FirstOrDefault(g => g.UserIdentityId == notification.UserIdentityId));
                }
                return grads;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Entities.GraduateData.Graduate> GetPossibleCandidates(bool IsDisable, RaceType race, GenderType gender, int qualificationId, int gpa)
        {
            try
            {
                var grads = DataContext.GraduateSet.Where(g => g.IsDisable == IsDisable && g.Race == race && g.UserIdentity.Gender == gender).ToList();
                List<Entities.GraduateData.Graduate> graduates = new List<Entities.GraduateData.Graduate>();


                foreach (Entities.GraduateData.Graduate graduate in grads)
                {
                    List<Mark> marks = new List<Mark>();
                    marks = DataContext.MarkSet.Where(q => q.Graduate.Id == graduate.Id && q.GPA > gpa && q.QualificationId == qualificationId).ToList();
                    if (marks.Count == 0)
                        continue;
                    graduate.UserIdentity.Roles.Clear();

                    List<Entities.QualificationData.Qualification> qualifications = new List<Entities.QualificationData.Qualification>();
                    foreach (Mark mark in marks)
                    {
                        qualifications = DataContext.QualificationSet.Where(q => q.Id == mark.QualificationId).ToList();
                        foreach (var item in qualifications)
                        {
                            item.Faculty = null;
                            item.Marks = item.Marks.Where(m => m.Id == mark.Id).ToList();
                            if (graduate.Qualifications == null)
                                graduate.Qualifications = new Collection<Entities.QualificationData.Qualification>();
                            graduate.Qualifications.Add(item);
                        }
                    }
                    if (graduate.Qualifications.Count > 0)
                        graduates.Add(graduate);
                }

                return graduates;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Entities.GraduateData.Graduate> GetGraduates()
        {
            try
            {
                var grads = DataContext.GraduateSet.ToList();
                List<Entities.GraduateData.Graduate> graduates = new List<Entities.GraduateData.Graduate>();


                foreach (Entities.GraduateData.Graduate graduate in grads)
                {
                    List<Mark> marks = new List<Mark>();
                    marks = DataContext.MarkSet.Where(q => q.Graduate.Id == graduate.Id).ToList();
                    if (marks.Count == 0)
                        continue;
                    graduate.UserIdentity.Roles.Clear();

                    List<Entities.QualificationData.Qualification> qualifications = new List<Entities.QualificationData.Qualification>();
                    foreach (Mark mark in marks)
                    {
                        qualifications = DataContext.QualificationSet.Where(q => q.Id == mark.QualificationId).ToList();
                        foreach (var item in qualifications)
                        {
                            item.Faculty = null;
                            item.Marks = item.Marks.Where(m => m.Id == mark.Id).ToList();
                            if (graduate.Qualifications == null)
                                graduate.Qualifications = new Collection<Entities.QualificationData.Qualification>();
                            graduate.Qualifications.Add(item);
                        }
                    }
                    if (graduate.Qualifications.Count > 0)
                    {
                        graduates.Add(graduate);
                    }
                }
                return graduates;
            }
            catch (Exception e)
            {
                return null;
            }
        }


        public List<Entities.Marks.Mark> GetMarks()
        {

            try
            {
                return DataContext.MarkSet.ToList();
            }
            catch (Exception e)
            {
                return null;
            }
        }
        public Entities.GraduateData.Graduate SaveGraduate(Entities.GraduateData.Graduate Graduate)
        {
            try
            {
                Entities.GraduateData.Graduate newGraduate = new Entities.GraduateData.Graduate();
                if (Graduate.Id > 0)
                {
                    newGraduate = DataContext.GraduateSet.SingleOrDefault(s => s.Id == Graduate.Id);

                    if (newGraduate != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                else
                {
                    newGraduate = Graduate;
                    DataContext.GraduateSet.Add(newGraduate);
                    DataContext.SaveChangesAsync();
                }
                return newGraduate;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Entities.GraduateData.Graduate DeactivateGraduate(long GraduateId)
        {
            try
            {
                Entities.GraduateData.Graduate newGraduate = new Entities.GraduateData.Graduate();
                if (GraduateId > 0)
                {
                    newGraduate = DataContext.GraduateSet.SingleOrDefault(s => s.Id == GraduateId);

                    if (newGraduate != null)
                    {
                        newGraduate.UserIdentity.Active = false;
                        newGraduate.UserIdentity.Deactivated = DateTime.Now;
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newGraduate;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
