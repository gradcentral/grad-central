﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;

namespace GradCentral.BL.Provider.Recruiter
{
    public class RecruiterProvider : GradCentralProvider, IRecruiterProvider
    {
          #region Constructor
        public RecruiterProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion
        public Entities.RecruiterData.Recruiter GetRecruiter(long RecruiterId)
        {
            try
            {
                return DataContext.RecruiterSet.SingleOrDefault(s => s.Id == RecruiterId);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public List<Entities.RecruiterData.Recruiter> GetRecruiters()
        {
            try
            {
                List<Entities.RecruiterData.Recruiter> model = DataContext.RecruiterSet.ToList();
                foreach (var recruiter in model)
                {
                    recruiter.Company =
                        DataContext.CompanySet.SingleOrDefault(ra => ra.Id == recruiter.CompanyId);
                    recruiter.Company.Recruiters = null;
                }

                return model;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public Entities.RecruiterData.Recruiter SaveRecruiter(Entities.RecruiterData.Recruiter Recruiter)
        {
            try
            {
                Entities.RecruiterData.Recruiter newRecruiter = new Entities.RecruiterData.Recruiter();
                if ( Recruiter.Id > 0)
                {
                    newRecruiter = DataContext.RecruiterSet.SingleOrDefault(s => s.Id == Recruiter.Id);

                    if (newRecruiter != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                else
                {
                    newRecruiter = Recruiter;
                    DataContext.RecruiterSet.Add(newRecruiter);
                    DataContext.SaveChangesAsync();
                }
                return newRecruiter;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Entities.RecruiterData.Recruiter DeactivateRecruiter(long RecruiterId)
        {
            try
            {
                Entities.RecruiterData.Recruiter newRecruiter = new Entities.RecruiterData.Recruiter();
                if (RecruiterId > 0)
                {
                    newRecruiter = DataContext.RecruiterSet.SingleOrDefault(s => s.Id == RecruiterId);

                    if (newRecruiter != null)
                    {
                        newRecruiter.UserIdentity.Active = false;
                        newRecruiter.UserIdentity.Deactivated = DateTime.Now;
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newRecruiter;
            }
            catch (Exception)
            {
                return null;
            }
        }

    }
}
