﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Recruiter
{
    public interface IRecruiterProvider : IGradCentralProvider
    {
        Entities.RecruiterData.Recruiter GetRecruiter(long RecruiterId);
        List<Entities.RecruiterData.Recruiter> GetRecruiters();

        Entities.RecruiterData.Recruiter SaveRecruiter(Entities.RecruiterData.Recruiter Recruiter);

        Entities.RecruiterData.Recruiter DeactivateRecruiter(long RecruiterId);

    }
}
