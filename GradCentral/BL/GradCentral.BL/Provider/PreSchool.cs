﻿using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Security;
using GC.Lib.BL;

namespace GradCentral.BL.Provider
{
    public class GradCentralProvider : ProviderBase<PrivilegeType>, IGradCentralProvider
    {
        public GradCentralProvider(DataContext context)
            : this(context, null)
        {

        }

        public GradCentralProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }

        public ICurrentUser CurrentUser
        {
            get
            {
                return LoggedInUser as ICurrentUser;
            }
            set
            {
                LoggedInUser = value as IUserContext<PrivilegeType>;
            }
        }

        public DataContext DataContext
        {
            get { return this.AuditDbContext as DataContext; }
        }
    }
}
