﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Tertiary.Campus
{
    public interface ICampusProvider : IGradCentralProvider
    {
        Entities.TertiaryData.Campus.Campus GetCampus(long CampusId);
        List<Entities.TertiaryData.Campus.Campus> GetCampuses();

        Entities.TertiaryData.Campus.Campus SaveCampus(Entities.TertiaryData.Campus.Campus Campus);

        Entities.TertiaryData.Campus.Campus DeactivateCampus(long CampusId);

    }
}
