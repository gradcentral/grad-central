﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;

namespace GradCentral.BL.Provider.Tertiary.Campus
{
    public class CampusProvider : GradCentralProvider, ICampusProvider
    {
          #region Constructor
        public CampusProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion
        public Entities.TertiaryData.Campus.Campus GetCampus(long CampusId)
        {
            try
            {
                return DataContext.CampusSet.SingleOrDefault(s => s.Id == CampusId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get campus.");
            }
        }

        public List<Entities.TertiaryData.Campus.Campus> GetCampuses()
        {
            try
            {
                return DataContext.CampusSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get campuses.");
            }
        }

        public Entities.TertiaryData.Campus.Campus SaveCampus(Entities.TertiaryData.Campus.Campus Campus)
        {
            try
            {
                Entities.TertiaryData.Campus.Campus newCampus = new Entities.TertiaryData.Campus.Campus();
                if ( Campus.Id > 0)
                {
                    newCampus = DataContext.CampusSet.SingleOrDefault(s => s.Id == Campus.Id);

                    if (newCampus != null)
                    {
                        newCampus.Name = Campus.Name;
                        newCampus.City = Campus.City;
                        newCampus.Number = Campus.Number;
                        newCampus.Street = Campus.Street;
                        newCampus.Town = Campus.Town;
                        newCampus.Zip = Campus.Zip;
                        if (Campus.TertiaryId > 0)
                            newCampus.TertiaryId = Campus.TertiaryId;
                        if(Campus.Tertiary != null)
                        newCampus.Tertiary = Campus.Tertiary;
                        newCampus.Province = Campus.Province;

                        DataContextSaveChanges();
                    }
                }
                else
                {
                    newCampus = new Entities.TertiaryData.Campus.Campus();
                    newCampus = Campus;
                    DataContext.CampusSet.Add(newCampus);
                    DataContextSaveChanges();
                }
                return newCampus;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get save campus.");
            }
        }

        public Entities.TertiaryData.Campus.Campus DeactivateCampus(long CampusId)
        {
            try
            {
                Entities.TertiaryData.Campus.Campus campus = new Entities.TertiaryData.Campus.Campus();
                if (CampusId > 0)
                {
                    campus = DataContext.CampusSet.SingleOrDefault(s => s.Id == CampusId);

                    if (campus != null)
                    {
                        DataContextSaveChanges();
                    }
                }
                return campus;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
