﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;

namespace GradCentral.BL.Provider.Tertiary
{
    public class TertiaryProvider : GradCentralProvider, ITertiaryProvider
    {
          #region Constructor
        public TertiaryProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion
        public Entities.TertiaryData.Tertiary GetTertiary(long TertiaryId)
        {
            try
            {
                return DataContext.TertiarySet.SingleOrDefault(s => s.Id == TertiaryId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get tertiary.");
            }
        }

        public List<Entities.TertiaryData.Tertiary> GetTertiarys()
        {
            try
            {
                return DataContext.TertiarySet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get terties.");
            }
        }

        public Entities.TertiaryData.Tertiary SaveTertiary(Entities.TertiaryData.Tertiary Tertiary)
        {
            try
            {
                Entities.TertiaryData.Tertiary newTertiary;
                if ( Tertiary.Id > 0)
                {
                    Entities.TertiaryData.Tertiary updateTeriary = DataContext.TertiarySet.SingleOrDefault(s => s.Id == Tertiary.Id);

                    if (updateTeriary != null)
                    {
                        updateTeriary.Name = Tertiary.Name;
                        updateTeriary.TertiaryType = Tertiary.TertiaryType;
                        updateTeriary.ProvinceType = Tertiary.ProvinceType;
                        DataContextSaveChanges();
                        return updateTeriary;
                    }
                }
                else
                {
                    newTertiary = new Entities.TertiaryData.Tertiary();
                    newTertiary = Tertiary;
                    DataContext.TertiarySet.Add(newTertiary);
                    DataContextSaveChanges();
                    return newTertiary;
                }
                return null;

            }
            catch (Exception)
            {
                throw new SecurityException("Unable to save tertiary.");
            }
        }

        public Entities.TertiaryData.Tertiary DeactivateTertiary(long TertiaryId)
        {
            try
            {
                Entities.TertiaryData.Tertiary newTertiary = new Entities.TertiaryData.Tertiary();
                if (TertiaryId > 0)
                {
                    newTertiary = DataContext.TertiarySet.SingleOrDefault(s => s.Id == TertiaryId);

                    if (newTertiary != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newTertiary;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }

    }
}
