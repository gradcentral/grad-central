﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Tertiary
{
    public interface ITertiaryProvider : IGradCentralProvider
    {
        Entities.TertiaryData.Tertiary GetTertiary(long TertiaryId);
        List<Entities.TertiaryData.Tertiary> GetTertiarys();

        Entities.TertiaryData.Tertiary SaveTertiary(Entities.TertiaryData.Tertiary Tertiary);

        Entities.TertiaryData.Tertiary DeactivateTertiary(long TertiaryId);

    }
}
