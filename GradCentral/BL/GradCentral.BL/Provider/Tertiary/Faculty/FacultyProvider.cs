using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;

namespace GradCentral.BL.Provider.Tertiary.Faculty
{
    public class FacultyProvider : GradCentralProvider, IFacultyProvider
    {
          #region Constructor
        public FacultyProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion
        public Entities.TertiaryData.Faculty.Faculty GetFaculty(long FacultyId)
        {
            try
            {
                return DataContext.FacultySet.SingleOrDefault(s => s.Id == FacultyId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Faculty.");
            }
        }

        public List<Entities.TertiaryData.Faculty.Faculty> GetFaculties()
        {
            try
            {
                return DataContext.FacultySet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Facultyes.");
            }
        }

        public Entities.TertiaryData.Faculty.Faculty SaveFaculty(Entities.TertiaryData.Faculty.Faculty Faculty)
        {
            try
            {
                Entities.TertiaryData.Faculty.Faculty newFaculty = new Entities.TertiaryData.Faculty.Faculty();
                if ( Faculty.Id > 0)
                {
                    newFaculty = DataContext.FacultySet.SingleOrDefault(s => s.Id == Faculty.Id);

                    if (newFaculty != null)
                    {
                        newFaculty.Name = Faculty.Name;
                        newFaculty.CampusId = Faculty.CampusId;
                        newFaculty.Qualifications = Faculty.Qualifications;

                        DataContextSaveChanges();
                    }
                }
                else
                {
                    newFaculty = new Entities.TertiaryData.Faculty.Faculty();
                    newFaculty = Faculty;
                    DataContext.FacultySet.Add(newFaculty);
                    DataContextSaveChanges();
                }
                return newFaculty;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to save Faculty.");
            }
        }

        public Entities.TertiaryData.Faculty.Faculty DeactivateFaculty(long FacultyId)
        {
            try
            {
                Entities.TertiaryData.Faculty.Faculty Faculty = new Entities.TertiaryData.Faculty.Faculty();
                if (FacultyId > 0)
                {
                    Faculty = DataContext.FacultySet.SingleOrDefault(s => s.Id == FacultyId);

                    if (Faculty != null)
                    {
                        DataContextSaveChanges();
                    }
                }
                return Faculty;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
