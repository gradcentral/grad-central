using System.Collections.Generic;

namespace GradCentral.BL.Provider.Tertiary.Faculty
{
    public interface IFacultyProvider : IGradCentralProvider
    {
        Entities.TertiaryData.Faculty.Faculty GetFaculty(long FacultyId);
        List<Entities.TertiaryData.Faculty.Faculty> GetFaculties();

        Entities.TertiaryData.Faculty.Faculty SaveFaculty(Entities.TertiaryData.Faculty.Faculty Faculty);

        Entities.TertiaryData.Faculty.Faculty DeactivateFaculty(long FacultyId);

    }
}
