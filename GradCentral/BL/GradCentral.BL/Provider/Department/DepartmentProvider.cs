﻿using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;

namespace GradCentral.BL.Provider.Department
{
    public class DepartmentProvider : GradCentralProvider,IDepartmentProvider
    {
        public DepartmentProvider(DataContext context, ICurrentUser currentUser) : base(context,currentUser){ }

        public Entities.DepartmentData.Department GetDepartment(long DepartmentId)
        {
            try
            {
                return DataContext.DepartmentSet.SingleOrDefault(s => s.Id == DepartmentId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Department.");
            }
        }

        public List<Entities.DepartmentData.Department> GetDepartments()
        {
            try
            {
                return DataContext.DepartmentSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Department.");
            }
        }

        public Entities.DepartmentData.Department SaveDepartment(Entities.DepartmentData.Department Department)
        {
            try
            {
                Entities.DepartmentData.Department newDepartment = new Entities.DepartmentData.Department();
                if (Department.Id > 0)
                {
                    newDepartment = DataContext.DepartmentSet.SingleOrDefault(s => s.Id == Department.Id);
                    if (newDepartment != null)
                    {
                        newDepartment.Id = Department.Id;
                        newDepartment.Name = Department.Name;
                        newDepartment.FacultyId = newDepartment.FacultyId;
                        newDepartment.Faculty = newDepartment.Faculty;
                        DataContextSaveChanges();
                    }
                }
                else
                {
                    newDepartment = new Entities.DepartmentData.Department();
                    newDepartment = Department;
                    DataContext.DepartmentSet.Add(newDepartment);
                    DataContextSaveChanges();
                    return newDepartment;
                }
                return null;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to save Department.");
            }
        }

        public Entities.DepartmentData.Department DeactivateDepartment(long DepartmentId)
        {
            try
            {
                Entities.DepartmentData.Department newDepartment = new Entities.DepartmentData.Department();
                if (DepartmentId > 0)
                {
                    newDepartment = DataContext.DepartmentSet.SingleOrDefault(s => s.Id == DepartmentId);

                    if (newDepartment != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newDepartment;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
