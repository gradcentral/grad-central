﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Department
{
    public interface IDepartmentProvider : IGradCentralProvider
    {
        Entities.DepartmentData.Department GetDepartment(long DepartmentId);
        List<Entities.DepartmentData.Department> GetDepartments();
        Entities.DepartmentData.Department SaveDepartment(Entities.DepartmentData.Department Department);

        Entities.DepartmentData.Department DeactivateDepartment(long DepartmentId);
    }
}
