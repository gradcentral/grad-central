﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System.Security;

namespace GradCentral.BL.Provider.Qualification
{
    public class QualificationProvider : GradCentralProvider, IQualificationProvider
    {
        #region Constructor
        public QualificationProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion

        public Entities.QualificationData.Qualification GetQualification(long QualificationId)
        {
            try
            {
                return DataContext.QualificationSet.SingleOrDefault(s => s.Id == QualificationId);

            }
            catch (Exception e)
            {
                return null;
                throw;
            }
        }

        public List<Entities.QualificationData.Qualification> GetQualifications()
        {
            try
            {
                return DataContext.QualificationSet.ToList();
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public Entities.QualificationData.Qualification saveQualification(Entities.QualificationData.Qualification Qualification)
        {
            try
            {
                Entities.QualificationData.Qualification newQualification;

                if (Qualification.Id > 0)
                {
                    newQualification = DataContext.QualificationSet.SingleOrDefault(s => s.Id == Qualification.Id);

                    if (newQualification != null)
                    {


                        newQualification.Date = Qualification.Date;
                        newQualification.Name = Qualification.Name;
                        newQualification.QualificationType = Qualification.QualificationType;
                        if (Qualification.FacultyId > 0)
                            newQualification.FacultyId = Qualification.FacultyId;
                        if (Qualification.Faculty != null)
                            newQualification.Faculty = Qualification.Faculty;



                        DataContext.QualificationSet.Attach(newQualification);
                        DataContextSaveChanges();
                    }
                    
                }
                else
                {
                    newQualification = new Entities.QualificationData.Qualification();
                    newQualification = Qualification;
                    DataContext.QualificationSet.Add(Qualification);
                    DataContextSaveChanges();
                    return newQualification;
                }

                return null;


            }
            catch (Exception e)
            {

                throw new SecurityException("Unable to save Qualification.");
            }

        }

        public Entities.QualificationData.Qualification DeactivateQualification(long QualificationId)
        {
            try
            {
                Entities.QualificationData.Qualification newQualification = new Entities.QualificationData.Qualification();
                if (QualificationId > 0)
                {
                    newQualification = DataContext.QualificationSet.SingleOrDefault(s => s.Id == QualificationId);
                    if (newQualification != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newQualification;
            }
            catch (Exception)
            {

                return null;
            }
            
        }

    }
}
