﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Qualification
{
    public interface IQualificationProvider : IGradCentralProvider
    {
        Entities.QualificationData.Qualification GetQualification(long QualificationId); //load One Record by ID
        List<Entities.QualificationData.Qualification> GetQualifications(); //Get List of Records
        Entities.QualificationData.Qualification saveQualification(Entities.QualificationData.Qualification Qualification); //Add / Save Record
        Entities.QualificationData.Qualification DeactivateQualification(long QualificationId);

    }
}
