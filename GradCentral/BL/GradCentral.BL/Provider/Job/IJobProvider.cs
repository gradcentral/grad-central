﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GradCentral.BL.Provider.Job
{
    public interface IJobProvider : IGradCentralProvider
    {
        Entities.JobData.Job GetJob(long JobId);
        List<Entities.JobData.Job> GetJobs();
        Entities.JobData.Job SaveJob(Entities.JobData.Job Job);

        Entities.JobData.Job DeactivateJob(long JobId);
    }
}
