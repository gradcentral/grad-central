﻿using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;

namespace GradCentral.BL.Provider.Job
{
    public class JobProvider : GradCentralProvider, IJobProvider
    {
        public JobProvider(DataContext context, ICurrentUser currentUser) : base(context,currentUser){ }

        public Entities.JobData.Job GetJob(long JobId)
        {
            try
            {
                return DataContext.JobSet.SingleOrDefault(s => s.Id == JobId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Job.");
            }
        }

        public List<Entities.JobData.Job> GetJobs()
        {
            try
            {
                if (CurrentUser.UserType == Entities.Types.UserType.Recruiter)
                {
                    Entities.RecruiterData.Recruiter r = DataContext.RecruiterSet.SingleOrDefault(rec => rec.UserIdentityId == CurrentUser.Id);
                    return DataContext.JobSet.Where(j => j.RecruiterId == r.Id).ToList();
                }
                else
                return DataContext.JobSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get Job.");
            }
        }

        public List<Entities.QualificationData.Qualification> GetQualifications()
        {
            try
            {
                return DataContext.QualificationSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get qualifications.");
            }
        }
        public Entities.JobData.Job SaveJob(Entities.JobData.Job Job)
        {
            try
            {
                Entities.JobData.Job newJob = new Entities.JobData.Job();
                if (Job.Id > 0)
                {
                    newJob = DataContext.JobSet.SingleOrDefault(s => s.Id == Job.Id);
                    if (newJob != null)
                    {
                        newJob.Id = Job.Id;
                        newJob.Title = Job.Title;
                        //newJob.Date = Job.Date;
                        //newJob.ClosingDate = Job.ClosingDate;
                        newJob.Disability = Job.Disability;
                        newJob.Position = Job.Position;
                        newJob.GPA = Job.GPA;
                        newJob.Gender = Job.Gender;
                        newJob.Race = Job.Race;

                        if (Job.JobRoleId > 0)
                            newJob.JobRoleId = Job.JobRoleId;
                        if (Job.JobRole != null)
                            newJob.JobRole = Job.JobRole;

                        if (Job.RecruiterId > 0)
                        {
                            newJob.RecruiterId = Job.RecruiterId;
                            
                        }
                        if (Job.Recruiter != null)
                        {
                            newJob.Recruiter = Job.Recruiter;
                        }
                        
                        if (Job.QualificationId > 0)
                            newJob.QualificationId = Job.QualificationId;
                        if (Job.Qualification != null)
                            newJob.Qualification = Job.Qualification;

                        DataContextSaveChanges();
                    }
                }
                else
                {
                    newJob = new Entities.JobData.Job();
                    newJob = Job;
                    //if (CurrentUser.UserType == Entities.Types.UserType.Recruiter)
                    //{
                    //    newJob.RecruiterId = DataContext.RecruiterSet.FirstOrDefault(r => r.UserIdentityId == CurrentUser.Id).Id;

                    //}
                    //else
                    //{
                    //    newJob.RecruiterId = DataContext.RecruiterSet.FirstOrDefault().Id;

                    //}
                    DataContext.JobSet.Add(newJob);
                    DataContextSaveChanges();

                }
                return newJob;
            }
            catch (Exception e)
            {
                var thing = e.Message;
                throw new SecurityException("Unable to save Job.");
            }
        }

        public Entities.JobData.Job DeactivateJob(long JobId)
        {
            try
            {
                Entities.JobData.Job newJob = new Entities.JobData.Job();
                if (JobId > 0)
                {
                    newJob = DataContext.JobSet.SingleOrDefault(s => s.Id == JobId);

                    if (newJob != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newJob;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
