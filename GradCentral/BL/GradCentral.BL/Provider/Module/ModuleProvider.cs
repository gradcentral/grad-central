﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System.Security;

namespace GradCentral.BL.Provider.Module
{
    public class ModuleProvider : GradCentralProvider,IModuleProvider
    {
        public ModuleProvider(DataContext context, ICurrentUser currentUser) : base(context,currentUser){}

        public Entities.ModuleData.Module GetModule(long ModuleId)
        {
            try
            {
                return DataContext.ModuleSet.SingleOrDefault(s => s.Id == ModuleId);
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get module.");
            }
        }

        public List<Entities.ModuleData.Module> GetModules()
        {
            try
            {
                return DataContext.ModuleSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get module.");
            }
        }

        public List<Entities.QualificationData.Qualification> GetQualifications()
        {
            try
            {
                return DataContext.QualificationSet.ToList();
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to get qualifications.");
            }
        }
        public Entities.ModuleData.Module SaveModule(Entities.ModuleData.Module Module)
        {
            try
            {
                Entities.ModuleData.Module newModule = new Entities.ModuleData.Module();
                if (Module.Id > 0)
                {
                    newModule = DataContext.ModuleSet.SingleOrDefault(s => s.Id == Module.Id);
                    if(newModule != null)
                    {
                        newModule.Id = Module.Id;
                        newModule.Name = Module.Name;
                        newModule.Semester = Module.Semester;
                        
                        if (Module.QualificationId > 0)
                        {
                            newModule.QualificationId = Module.QualificationId;
                        }
                        if (Module.Qualification != null)
                            newModule.Qualification = Module.Qualification;

                        DataContextSaveChanges();

                    }
                }
                else
                {
                    newModule = new Entities.ModuleData.Module();
                    newModule = Module;
                    DataContext.ModuleSet.Add(newModule);
                    DataContextSaveChanges();
                    return newModule;
                }
                return null;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to save module.");
            }
        }

        public Entities.ModuleData.Module DeactivateModule(long ModuleId)
        {
            try
            {
                Entities.ModuleData.Module newModule = new Entities.ModuleData.Module();
                if (ModuleId > 0)
                {
                    newModule = DataContext.ModuleSet.SingleOrDefault(s => s.Id == ModuleId);

                    if (newModule != null)
                    {
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newModule;
            }
            catch (Exception)
            {
                throw new SecurityException("Unable to change activeation status.");
            }
        }
    }
}
