﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Module
{
    public interface IModuleProvider : IGradCentralProvider
    {
        Entities.ModuleData.Module GetModule(long ModuleId);
        List<Entities.ModuleData.Module> GetModules();
        Entities.ModuleData.Module SaveModule(Entities.ModuleData.Module Module);

        Entities.ModuleData.Module DeactivateModule(long ModuleId);
    }
}
