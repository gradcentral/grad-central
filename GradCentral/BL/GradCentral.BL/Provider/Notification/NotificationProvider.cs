﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Context;
using GradCentral.BL.Provider.Security;
using System.Security;
using GradCentral.BL.Provider.Settings;
using GradCentral.BL.Provider.Email;

namespace GradCentral.BL.Provider.Notification
{
    public class NotificationProvider : GradCentralProvider, INotificationProvider
    {
        #region Constructor
        public NotificationProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion

        public Entities.NotificationData.Notification GetNotification(long NotificationId)
        {
            try
            {
                return DataContext.NotificationSet.SingleOrDefault(s => s.Id == NotificationId);

            }
            catch (Exception e)
            {
                return null;
                throw;
            }
        }

        public List<Entities.NotificationData.Notification> GetNotifications()
        {
            try
            {
                return DataContext.NotificationSet.ToList();
            }
            catch (Exception e)
            {

                return null;
            }
        }

        public bool SendGradsNotification(Entities.JobData.Job Job, List<Entities.GraduateData.Graduate> Graduates)
        {
            try
            {
                Entities.NotificationData.Notification Notification;
                List<Entities.NotificationData.Notification> Notifications = new List<Entities.NotificationData.Notification>();
                EmailProvider emailProvider = new EmailProvider(DataContext, new AppSettings());
                string emailList = "";

                foreach (Entities.GraduateData.Graduate graduate in Graduates)
                {
                    if (emailList != "")
                        emailList = emailList + ";" + graduate.UserIdentity.Email;
                    else
                        emailList = graduate.UserIdentity.Email;

                    Notification = new Entities.NotificationData.Notification
                    {
                        Date = DateTime.Now,
                        ExpiryDate = DateTime.Now.AddMonths(1),
                        JobId = Job.Id,
                        Message = Job.Title,
                        UserIdentityId = graduate.UserIdentityId,
                    };
                    SaveNotification(Notification);
                }

                string msg = "Good day <br>" + Job.Title + "<br /><br />  Regards <br /> Grad Central Website";

                emailProvider.SendHTMLEmail(emailList, "You have are a potential Candidate for " + DataContext.JobRoleSet.FirstOrDefault(r => r.Id == Job.JobRoleId).Name, Job.Title);

                return true;
            }
            catch (Exception e)
            {

                throw new SecurityException(e.Message.ToString());
            }
        }

        public Entities.NotificationData.Notification SaveNotification(Entities.NotificationData.Notification Notification)
        {
            try
            {
                Entities.NotificationData.Notification newNotification;

                if (Notification.Id > 0)
                {
                    newNotification = DataContext.NotificationSet.SingleOrDefault(s => s.Id == Notification.Id);

                    if (newNotification != null)
                    {

                        newNotification.Read = Notification.Read;
                        newNotification.ExpiryDate = Notification.ExpiryDate;
                        newNotification.Confirm = Notification.Confirm;
                        newNotification.DateCreated = Notification.DateCreated;
                        newNotification.Message = Notification.Message;
                        if (Notification.JobId > 0)
                            newNotification.JobId = Notification.JobId;
                        if (Notification.UserIdentityId > 0)
                            newNotification.UserIdentityId = Notification.UserIdentityId;
                        DataContext.NotificationSet.Attach(newNotification);
                        DataContextSaveChanges();
                    }

                }
                else
                {
                    newNotification = new Entities.NotificationData.Notification();
                    newNotification = Notification;
                    newNotification.DateCreated = DateTime.Now;
                    DataContext.NotificationSet.Add(Notification);
                    DataContextSaveChanges();
                    return newNotification;
                }

                return null;


            }
            catch (Exception e)
            {

                throw new SecurityException("Unable to save Notification.");
            }

        }

        public Entities.NotificationData.Notification DeleteNotification(long NotificationId)
        {
            try
            {
                Entities.NotificationData.Notification newNotification = new Entities.NotificationData.Notification();
                if (NotificationId > 0)
                {
                    newNotification = DataContext.NotificationSet.SingleOrDefault(s => s.Id == NotificationId);
                    if (newNotification != null)
                    {
                        DataContext.NotificationSet.Remove(newNotification);
                        DataContext.SaveChanges(CurrentUser);
                    }
                }
                return newNotification;
            }
            catch (Exception)
            {

                return null;
            }

        }

        public List<Entities.NotificationData.Notification> GetNotificationsByUser()
        {

            try
            {

                //var NotificationList = DataContext.NotificationSet.ToList();

                var Nots = DataContext.NotificationSet.Where(t => t.UserIdentityId == CurrentUser.Id);
                return Nots.ToList();

            }
            catch (Exception e)
            {

                return null;
            }


        }

    }
}
