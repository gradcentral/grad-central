﻿using System.Collections.Generic;

namespace GradCentral.BL.Provider.Notification
{
    public interface INotificationProvider : IGradCentralProvider
    {
        Entities.NotificationData.Notification GetNotification(long NotificationId);
        List<Entities.NotificationData.Notification> GetNotifications();

        bool SendGradsNotification(Entities.JobData.Job Job, List<Entities.GraduateData.Graduate> Graduate);
        List<Entities.NotificationData.Notification> GetNotificationsByUser();

        Entities.NotificationData.Notification SaveNotification(Entities.NotificationData.Notification Notification);

        Entities.NotificationData.Notification DeleteNotification(long NotificationId);

    }
}
