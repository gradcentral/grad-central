﻿using GradCentral.BL.Entities.SecurityData;

namespace GradCentral.BL.Provider.Email
{
    public interface IEmailProvider : IGradCentralProvider
    {
        void SendPasswordEmailToUser(UserIdentity user, string password);
    }
}
