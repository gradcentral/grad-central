﻿using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Security;
using GradCentral.BL.Provider.Settings;
using GC.Lib.Email;

namespace GradCentral.BL.Provider.Email
{
    public class EmailProvider : GradCentralProvider, IEmailProvider
    {
        #region CTor

        private IAppSettings AppSettings { get; set; }

        public EmailProvider(DataContext context, IAppSettings appSettings)
            :base(context,null)
        {
            AppSettings = appSettings;
        }

        public EmailProvider(DataContext context, ICurrentUser currentUser, IAppSettings appSettings)
            :base(context,currentUser)
        {
            AppSettings = appSettings;
        }

        #endregion

        public void SendPasswordEmailToUser(UserIdentity user, string password)
        {
            string sendMailTo = user.Email;
            string emailBody = "Good day " + user.FirstName + ",<br><br>" +
                               "Your user name is : " + user.UserName + "<br>" +
                               "and new password is : " + password + "<br>" +
                               "Please go to "+ AppSettings.RootURL + " to log in." + "<br> <br>" +
                               "Kind Regards" + "<br>" +
                               "GradCentral Team.";

            SendHTMLEmail(sendMailTo, "GradCentral Password", emailBody);

        }

        public void SendHTMLEmail(string sendMailTo, string subject, string emailBody)
        {
            EmailSender.SimpleHtmlSendMail(AppSettings.EmailFromAddress, AppSettings.EmailFromName, sendMailTo, subject, emailBody);
        }
    }
}
