﻿using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Provider.Security;
using GC.Lib.BL;

namespace GradCentral.BL.Provider
{
    public interface IGradCentralProvider : IProviderBase<PrivilegeType>
    {
        DataContext DataContext { get; }

        ICurrentUser CurrentUser { get; set; }
    }
}
