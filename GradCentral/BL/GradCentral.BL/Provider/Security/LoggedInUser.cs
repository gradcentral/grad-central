﻿using System.Collections.Generic;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;

namespace GradCentral.BL.Provider.Security
{
    public class LoggedInUser : ICurrentUser
    {
        public long Id { get; set; }

        public string UserName { get; set; }

        public string DisplayName { get; set; }

        public List<PrivilegeType> AllowedPrivileges { get; set; }

        public bool IsSystemAdmin { get; set; }

        public UserType UserType { get; set; }

        public string UserTypeString
        {
            get { return UserType.ToString(); }
        }
    }
}
