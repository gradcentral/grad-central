﻿using System;
using System.Collections.Generic;
using System.Linq;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using GradCentral.BL.Provider.Email;

namespace GradCentral.BL.Provider.Security
{
    public interface ISecurityProvider : IGradCentralProvider
    {
        ICurrentUser UserLogin(string userName, string password);
        ICurrentUser UserIdentityToCurrentUser(UserIdentity user);
        ICurrentUser GetCurrentUser(string userName);

        #region UserMaintenance
        UserIdentity SignUp(string userName, TitleType title, string firstName, string surname, string password,string email,  int fromCountry, int toCountry, GenderType genderType);
        UserIdentity SaveUser(TitleType title,UserType type,long? id, string userName, string firstName, string surname, 
            string email, List<long> allowedRoles,
            bool? isEmployed, long? jobId, long? teriaryId, long? companyId, DateTime? lockOut, bool isNasfas,
            string cellNumber, GenderType gender, bool? IsDisable, RaceType? race, string disablityInfo, string currentEmploye, ICollection<Entities.Marks.Mark> marks, ICollection<Entities.QualificationData.Qualification> qualfications);
        IQueryable<UserIdentity> GetUserList();
        UserIdentity LockAccount(long userId);
        UserIdentity  EditProfile(string firstName, string surname, string email, TitleType title);
        UserIdentity GetMyProfile();
        UserIdentity UnlockAccount(long userId);
        UserIdentity ChangeMyPassword(string oldPassword, string newPassword);
        void ResetUserPassword(string userName, IEmailProvider emailProvider);
        #endregion

        #region Roles
        Role SaveRole(long? id, string name, string description, StatusType status, List<PrivilegeType> privileges);
        Role GetRole(long id);
        IQueryable<Role> GetRoles();
        #endregion
    }
}
