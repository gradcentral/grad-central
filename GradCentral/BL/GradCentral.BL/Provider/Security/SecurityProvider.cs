﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using GradCentral.BL.Context;
using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using GradCentral.BL.Provider.Email;
using GradCentral.BL.Util;
using GC.Lib.Utility;
using GradCentral.BL.Entities.RecruiterData;
using GradCentral.BL.Provider.Settings;
using GradCentral.BL.Entities.Marks;

namespace GradCentral.BL.Provider.Security
{
    public class SecurityProvider : GradCentralProvider, ISecurityProvider
    {
        #region Constructor
        public SecurityProvider(DataContext context, ICurrentUser currentUser)
            : base(context, currentUser)
        {

        }
        #endregion

        public ICurrentUser UserLogin(string userName, string password)
        {
            var result = DataContext.UserIdentitySet.Where(x => x.UserName == userName).SingleOrDefault();

            if (result == null || result.PasswordHash != Cipher.Encrypt(password))
            {
                if (result != null)
                {
                    result.FailedLoginAttempts = result.FailedLoginAttempts + 1;
                    if (result.FailedLoginAttempts >= 3)
                        result.LockedOut = DateTime.Now;
                    DataContextSaveChanges();
                }

                throw new SecurityException("Invalid user name and password combination.");
            }

            if (result.Deactivated != null)
                throw new SecurityException("Account Deactivated!");

            if (result.LockedOut != null)
                throw new SecurityException("Account locked out!");

            if (result.Active == false)
                throw new SecurityException("Account Deactivated!");

            result.LastLogin = DateTime.Now;

            if (result.FailedLoginAttempts > 0)
                result.FailedLoginAttempts = 0;


            DataContextSaveChanges();

            var qPrivs = from userP in DataContext.UserIdentitySet
                         from role in userP.Roles
                         from privileges in role.Privileges
                         where userP.Id == result.Id
                         && role.Status == StatusType.Active
                         select privileges.Security;

            result.AllowedPrivileges = qPrivs.Distinct().ToList();

            if (result.IsSystemAdmin)
                result.AllowedPrivileges = DataContext.PrivilegeSet.Select(a => a.Security).ToList();

            CurrentUser = UserIdentityToCurrentUser(result);

            return CurrentUser;
        }

        public ICurrentUser UserIdentityToCurrentUser(UserIdentity user)
        {
            UserType type = new UserType();// UserType.Consumer;

            if (user.IsSystemAdmin)
            {
                type = UserType.Admin;
            }
            else if (user.Type == UserType.Graduate)
            {
                type = UserType.Graduate;
            }
            else if (user.Type == UserType.Recruiter)
            {
                type = UserType.Recruiter;
            }


            return new LoggedInUser()
            {
                Id = user.Id,
                UserName = user.UserName,
                DisplayName = user.FirstName + " " + user.Surname,
                AllowedPrivileges = user.AllowedPrivileges,
                IsSystemAdmin = user.IsSystemAdmin,
                UserType = type
            };
        }

        public ICurrentUser GetCurrentUser(string userName)
        {
            var result = DataContext.UserIdentitySet.Where(x => x.UserName == userName).SingleOrDefault();

            if (result == null)
                throw new SecurityException("User not logged in!");

            if (result.Deactivated != null)
                throw new SecurityException("Account Deactivated!");

            if (result.LockedOut != null)
                throw new SecurityException("Account locked out!");

            if (result.Active == false)
                throw new SecurityException("Account Deactivated!");

            result.LastLogin = DateTime.Now;

            if (result.FailedLoginAttempts > 0)
                result.FailedLoginAttempts = 0;

            DataContextSaveChanges();

            var qPrivs = from userP in DataContext.UserIdentitySet
                         from role in userP.Roles
                         from privileges in role.Privileges
                         where userP.Id == result.Id
                         select privileges.Security;

            result.AllowedPrivileges = qPrivs.Distinct().ToList();

            if (result.IsSystemAdmin)
                result.AllowedPrivileges = DataContext.PrivilegeSet.Select(a => a.Security).ToList();

            //CurrentUser = UserIdentityToCurrentUser(result);

            return CurrentUser;
        }

        #region UserMaintenance

        public UserIdentity SignUp(string userName, TitleType title, string firstName, string surname,
            string password, string email, int fromCountry, int toCountry, GenderType gender)
        {
            var current = DataContext.UserIdentitySet
                            .Where(a => a.UserName == userName).SingleOrDefault();

            if (current != null)
            {
                throw new SecurityException("User already exists");
            }

            var user = new UserIdentity()
            {
                UserName = userName,
                Title = title,
                FirstName = firstName,
                Surname = surname,
                PasswordHash = Cipher.Encrypt(password),
                FailedLoginAttempts = 0,
                IsSystemAdmin = false,
                Active = true,
                Email = email,
                Type = UserType.Graduate,
                Gender = gender
            };

            DataContext.UserIdentitySet.Add(user);

            DataContextSaveChanges();


            return user;
        }

        public UserIdentity SaveUser(TitleType title, UserType type, long? id, string userName, string firstName, string surname, string email,
            List<long> allowedRoles, bool? isEmployed, long? jobId, long? teriaryId, long? recruiterId, DateTime? lockOut, bool isNasfas,
            string cellNumber, GenderType gender, bool? IsDisable, RaceType? race, string disablityInfo, string currentEmployer, ICollection<Entities.Marks.Mark> marks, ICollection<Entities.QualificationData.Qualification> qualifications)
        {
            Authenticate(PrivilegeType.UserMaintenance);
            bool isEdit = false;

            if (id == 0)
                id = null;
            var newUser = DataContext.UserIdentitySet.Where(u => u.UserName == userName).SingleOrDefault();

            if (newUser != null)
            {
                isEdit = true;
                if (newUser.Id == id)
                {
                    newUser = DataContext.UserIdentitySet.Where(u => u.Id == id).Single();
                }
                else
                {
                    throw new SecurityException("User already exists");
                }
            }
            else
            {
                if (id == null)
                {
                    newUser = new UserIdentity();
                    if (type == UserType.Admin)
                        newUser.IsSystemAdmin = true;
                    else
                        newUser.IsSystemAdmin = false;
                    newUser.Active = true;
                    newUser.Type = type;
                    newUser.PasswordHash = Cipher.Encrypt(GenerateNewPassword());
                    DataContext.UserIdentitySet.Add(newUser);
                }
                else
                    throw new SecurityException("User not found!");
            }

            newUser.UserName = userName;
            newUser.FirstName = firstName;
            newUser.Surname = surname;
            newUser.Email = email;
            newUser.Title = title;
            newUser.LockedOut = lockOut;
            newUser.Gender = gender;
            newUser.CellNumber = cellNumber;


            //if(active != null)

            var roles = DataContext.RoleSet.Where(a => allowedRoles.Contains(a.Id)).ToList();

            if (newUser.Roles != null)
            {
                newUser.Roles.Clear();
                foreach (var r in roles)
                {
                    newUser.Roles.Add(r);
                }
            }
            else
            {
                newUser.Roles = roles;
            }

            DataContextSaveChanges();
            Entities.GraduateData.Graduate newGraduate;
            Entities.RecruiterData.Recruiter newRecruiter;

            if (type == UserType.Graduate)
            {

                if (jobId == 0)
                    jobId = null;
                if (!isEdit)
                {
                    foreach (BL.Entities.QualificationData.Qualification qualification in qualifications)
                    {
                        qualification.Faculty = null;
                        qualification.Graduates = null;
                    }
                    newGraduate = new Entities.GraduateData.Graduate()
                    {
                        TertiaryId = (long)teriaryId,
                        IsEmployed = (bool)isEmployed,
                        IsNasfas = (bool)isNasfas,
                        UserIdentityId = newUser.Id,
                        IsDisable = (bool)IsDisable,
                        DisabilityDescription = disablityInfo,
                        CurrentEmployer= currentEmployer,
                        Race = (RaceType)race,
                        //Qualifications = qualifications
                    };
                    DataContext.GraduateSet.Add(newGraduate);
                    DataContextSaveChanges();

                    foreach (Mark mark in marks)
                    {
                        mark.GraduateId = newGraduate.Id;
                        mark.Graduate = newGraduate;
                        //mark.Qualification = null;
                    }

                    DataContext.MarkSet.AddRange(marks);

                }
                else
                {
                    newGraduate = DataContext.GraduateSet.SingleOrDefault(g => g.UserIdentityId == newUser.Id);

                    if (newGraduate != null)
                    {
                        newGraduate.IsEmployed = (bool)isEmployed;
                        newGraduate.IsNasfas = (bool)isNasfas;
                        newGraduate.IsDisable = (bool)IsDisable;
                        newGraduate.Race = (RaceType)race;
                        newGraduate.DisabilityDescription = disablityInfo;
                        newGraduate.CurrentEmployer = currentEmployer;
                            }
                }
            }
            else if (type == UserType.Recruiter && !isEdit)
            {
                newRecruiter = new Entities.RecruiterData.Recruiter()
                {
                    CompanyId = (long)recruiterId,
                    UserIdentityId = newUser.Id
                };
                DataContext.RecruiterSet.Add(newRecruiter);
            }
            DataContextSaveChanges();

            if (!isEdit)
            {
                EmailProvider emailProvider = new EmailProvider(DataContext, new AppSettings());
                emailProvider.SendPasswordEmailToUser(newUser, Cipher.Decrypt(newUser.PasswordHash));
            }

            return newUser;
        }

        public IQueryable<UserIdentity> GetUserList()
        {
            var userList = from u in DataContext.UserIdentitySet
                           orderby u.Active, u.UserName, u.IsSystemAdmin ascending
                           select u;
            return userList;
        }

        public UserIdentity LockAccount(long userId)
        {
            //Authenticate(PrivilegeType.UserMaintenance);

            var thisUser = DataContext.UserIdentitySet.Where(u => u.Id == userId).Single();

            if (thisUser.LockedOut != null)
                throw new SecurityException("Account already locked.");
            else
                thisUser.LockedOut = DateTime.Today;

            DataContextSaveChanges();

            return thisUser;
        }

        public UserIdentity EditProfile(string firstName, string surname, string email, TitleType title)
        {
            //Authenticate(PrivilegeType.Consumer);
            UserIdentity myProfile = DataContext.UserIdentitySet.Where(u => u.Id == CurrentUser.Id).Single();

            myProfile.FirstName = firstName;
            myProfile.Email = email;
            myProfile.Surname = surname;
            myProfile.Title = title;

            DataContext.SaveChanges();

            return myProfile;

        }

        public UserIdentity GetMyProfile()
        {
            //Authenticate(PrivilegeType.GraduateMaintenance);
            var myProfile = DataContext.UserIdentitySet.Where(u => u.Id == CurrentUser.Id).Single();

            return myProfile;
        }

        public UserIdentity ChangeMyPassword(string oldPassword, string newPassword)
        {

            var me = DataContext.UserIdentitySet.Where(m => m.Id == CurrentUser.Id).Single();

            if (me.PasswordHash != Cipher.Encrypt(oldPassword))
                throw new SecurityException("Old password is incorrect!");

            me.PasswordHash = Cipher.Encrypt(newPassword);

            DataContextSaveChanges();

            return me;
        }

        public UserIdentity UnlockAccount(long userId)
        {
            Authenticate(PrivilegeType.UserMaintenance);

            var thisUser = DataContext.UserIdentitySet.Where(u => u.Id == userId).Single();

            thisUser.LockedOut = null;

            DataContextSaveChanges();

            return thisUser;
        }

        public void ResetUserPassword(string userName, IEmailProvider emailProvider)
        {
            var q = from u in DataContext.UserIdentitySet
                    where u.UserName == userName
                    select u;

            var user = q.SingleOrDefault();
            if (user == null)
                throw new SecurityException("User not found");

            string password = GenerateNewPassword();
            user.PasswordHash = Cipher.Encrypt(password);

            if (user.LockedOut != null)
            {
                user.LockedOut = null;
            }

            DataContextSaveChanges();

            emailProvider.SendPasswordEmailToUser(user, password);
        }

        private string GenerateNewPassword()
        {
            return RandomFactory.GeneratePassword(3, 4, 2, 1);
        }
        #endregion

        #region Roles
        public Role SaveRole(long? roleId, string name, string description, StatusType status, List<PrivilegeType> privileges)
        {
            var roleList = privileges.ToArray();

            var role = DataContext.RoleSet.Where(r => r.RoleName == name).SingleOrDefault();

            if (role != null && role.Id != roleId)
            {
                throw new SecurityException(name + " role already exists.");
            }
            else
            {
                if (role != null)
                {
                    role = DataContext.RoleSet.Where(r => r.Id == roleId).Single();
                }
                else
                {
                    role = new Role();
                    DataContext.RoleSet.Add(role);
                }
            }

            role.Status = status;
            role.RoleName = name;
            role.Description = description;

            var security = DataContext.PrivilegeSet.Where(s => roleList.Contains(s.Security)).ToList();
            if (role.Privileges != null)
            {
                role.Privileges.Clear();
                foreach (var i in security)
                {
                    role.Privileges.Add(i);
                }
            }
            else
            {
                role.Privileges = security;
            }
            DataContextSaveChanges();

            return role;
        }

        public Role GetRole(long id)
        {

            var role = DataContext.RoleSet.Where(r => r.Id == id).Single();


            return role;
        }

        public IQueryable<Role> GetRoles()
        {
            var objQuery = from r in DataContext.RoleSet
                           orderby r.RoleName
                           select r;

            return objQuery;
        }
        #endregion
    }
}
