﻿using GradCentral.BL.Entities.SecurityData;
using GradCentral.BL.Entities.Types;
using GC.Lib.BL;

namespace GradCentral.BL.Provider.Security
{
    public interface ICurrentUser : IUserContext<PrivilegeType>
    {
        UserType UserType { get; }

        string UserTypeString { get; }
    }
}
